﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToeV2.BLL;
using TicTacToeV2.View;

namespace TicTacToeV2
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu menu = new Menu();
            Game game = new Game();
            game.Start(menu);
        }
    }
}
