﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe.Models
{
    class Board
    {
        public string[] boardLocations = new string[] { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

        public void ExecuteBoard()
        {
            DisplayBoard(boardLocations);
        }

        public void DisplayBoard(string[] boardLocations)
        {
            Console.WriteLine("\n\n\n\t\t\t\t|\t|");
            Console.WriteLine("\n\t\t           {0}    |    {1}  |    {2}", boardLocations[0], boardLocations[1], boardLocations[2]);
            Console.WriteLine("\n\t\t\t\t|\t|");
            Console.WriteLine("\t\t\t--------------------------");
            Console.WriteLine("\n\t\t\t\t|\t|");
            Console.WriteLine("\n\t\t           {0}    |    {1}  |    {2}", boardLocations[3], boardLocations[4], boardLocations[5]);
            Console.WriteLine("\n\t\t\t\t|\t|");
            Console.WriteLine("\t\t\t--------------------------");
            Console.WriteLine("\n\t\t\t\t|\t|");
            Console.WriteLine("\n\t\t           {0}    |    {1}  |    {2}", boardLocations[6], boardLocations[7], boardLocations[8]);
            Console.WriteLine("\n\t\t\t\t|\t|");
        }

    }
}
