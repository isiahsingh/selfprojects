﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.WorkFlow;

namespace TicTacToe
{
    class Program
    {
        static void Main(string[] args)
        {
            Game TicTacToe = new Game();
            TicTacToe.Execute();
        }
    }
}
