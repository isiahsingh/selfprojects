﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TicTacToe.Models;
using TicTacToe.Operations;

namespace TicTacToe.WorkFlow
{
    class Game
    {
        BoardOperations gameBoard = new BoardOperations();
        string[] names = new string[2];
        private string p1;
        private string p2;
        private int turnCounter = 0;
        private bool isWinner = false;

        public void Execute()
        {
            DisplayHeader();
            NameGetter getNames = new NameGetter();
            names = getNames.getPlayerNames();
            p1 = names[0];
            p2 = names[1];
            DisplayHeader(p1, p2);
            gameBoard.Execute();
            startGameExecution(p1, p2);
        }

        public void startGameExecution(string player1, string player2)
        {
            do
            {
                int location;
                string response;
                bool validInput = false;
                string[] currentLocations = new string[9];
                currentLocations = gameBoard.getCurrentBoard();

                if (turnCounter % 2 == 0)
                {
                    do
                    {  
                        Console.WriteLine("\n\t\t{0}, select a location to place your mark.", player1);
                        response = Console.ReadLine();
                        validInput = int.TryParse(response, out location);
                        if (validInput == false || (location <= 1 && location >= 9) || ((currentLocations[location-1] == "X") || (currentLocations[location-1] == "O")))
                        {
                            ConsoleColor regularColor = Console.ForegroundColor;
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("That was an incorrect input... Press Enter to try again.");
                            Console.ForegroundColor = regularColor;
                            Console.ReadLine();
                        }
                    } while (validInput == false || (location <= 1 && location >= 9) || ((currentLocations[location-1] == "X") || (currentLocations[location-1] == "O")));
                    turnCounter++;
                    UpdateBoard(response, player1);
                    checkWinner();
                }
                else
                {
                    do
                    {
                        Console.WriteLine("\n\t\t{0}, select a location to place your mark.", player2);
                        response = Console.ReadLine();
                        validInput = int.TryParse(response, out location);
                        if (validInput == false || (location <= 1 && location >= 9) || ((currentLocations[location-1] == "X") || (currentLocations[location-1] == "O")))
                        {
                            ConsoleColor regularColor = Console.ForegroundColor;
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("That was an incorrect input... Press Enter to try again.");
                            Console.ForegroundColor = regularColor;
                            Console.ReadLine();
                        }
                    } while (validInput == false || (location <= 1 && location >= 9) || ((currentLocations[location-1] == "X") || (currentLocations[location-1] == "O")));
                    turnCounter++;
                    UpdateBoard(response, player2);
                    checkWinner();
                }
            } while (isWinner == false && turnCounter != 9);
            DisplayWinnerMessage();
        }

        public void UpdateBoard(string location, string player)
        {
            string[] boardLocations = gameBoard.getCurrentBoard();

            if (player == names[0])
            {
                for (int i = 0; i < boardLocations.Length; i++)
                {
                    if (boardLocations[i].Equals(location))
                    {
                        boardLocations[i] = "X";
                    }
                }
                gameBoard.UpdateBoard(boardLocations);
                Console.Clear();
                DisplayHeader(p1, p2);
                gameBoard.Execute();
            }
            else
            {
                for (int i = 0; i < boardLocations.Length; i++)
                {
                    if (boardLocations[i].Equals(location))
                    {
                        boardLocations[i] = "O";
                    }
                }
                gameBoard.UpdateBoard(boardLocations);
                Console.Clear();
                DisplayHeader(p1, p2);
                gameBoard.Execute();
            }
            
        }

        public void checkWinner()
        {
            string[] gameBoardLocations = gameBoard.getCurrentBoard();
            if ((gameBoardLocations[0] == gameBoardLocations[1] && gameBoardLocations[1] == gameBoardLocations[2]) ||
                (gameBoardLocations[3] == gameBoardLocations[4] && gameBoardLocations[4] == gameBoardLocations[5]) ||
                (gameBoardLocations[6] == gameBoardLocations[7] && gameBoardLocations[7] == gameBoardLocations[8]) ||
                (gameBoardLocations[0] == gameBoardLocations[3] && gameBoardLocations[3] == gameBoardLocations[6]) ||
                (gameBoardLocations[1] == gameBoardLocations[4] && gameBoardLocations[4] == gameBoardLocations[7]) ||
                (gameBoardLocations[2] == gameBoardLocations[5] && gameBoardLocations[5] == gameBoardLocations[8]) ||
                (gameBoardLocations[0] == gameBoardLocations[4] && gameBoardLocations[4] == gameBoardLocations[8]) ||
                (gameBoardLocations[2] == gameBoardLocations[4] && gameBoardLocations[4] == gameBoardLocations[6]))
            {
                isWinner = true;
            }              
        }

        public void DisplayWinnerMessage()
        {
           
            if (turnCounter == 9)
            {
                ConsoleColor regularColor = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("Its a draw.\n");
                Console.ForegroundColor = regularColor;
                playAgain();

            }
            else if ((turnCounter - 1)%2 == 0)
            {
                ConsoleColor regularColor = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("Player {0} wins!!!!", p1);
                Console.ForegroundColor = regularColor;
                playAgain();
            }
            else
            {
                ConsoleColor regularColor = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("Player {0} wins!!!!", p2);
                Console.ForegroundColor = regularColor;
            }
        }

        public void playAgain()
        {
            bool validPlayAgain = false;
            do
            {
                Console.WriteLine("\nWant to play again? (y/n)");
                string playAgain = Console.ReadLine();
                if (playAgain.ToUpper() == "Y")
                {
                    gameBoard.clearBoard();
                    turnCounter = 0;
                    isWinner = false;
                    Execute();
                    validPlayAgain = true;
                }
                else if (playAgain.ToUpper() == "N")
                {
                    Console.WriteLine("Thanks for playing.");
                    Thread.Sleep(700);
                    Environment.Exit(0);
                    validPlayAgain = true;
                }
                else
                {
                    Console.WriteLine("Invalid Input");
                }
            } while (validPlayAgain == false);
        }

        public void DisplayHeader()
        {
            Console.Clear();
            ConsoleColor regularColor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("===============================================================================");
            Console.WriteLine("*********************************Tic Tac Toe***********************************");
            Console.WriteLine("===============================================================================");
            Console.ForegroundColor = regularColor;
        }

        public void DisplayHeader(string player1, string player2)
        {
            Console.Clear();
            Console.WriteLine("===============================================================================");
            Console.WriteLine("*********************************Tic Tac Toe***********************************");
            Console.WriteLine("===============================================================================");
            ConsoleColor regularColor = Console.ForegroundColor;
            Console.Write("\n\n\t\t\tPlayer X - ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("{0}", player1);
            Console.ForegroundColor = regularColor;
            Console.Write(" Versus Player O - ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("{0}", player2);
            Console.ForegroundColor = regularColor;
        }
    }
}
