﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.Models;

namespace TicTacToe.Operations
{
    class BoardOperations
    {
        Board gameBoard = new Board();

        public void Execute()
        {
            gameBoard.ExecuteBoard();
        }

        public void UpdateBoard(string[] newBoard)
        {
            gameBoard.boardLocations = newBoard;
        }

        public string[] getCurrentBoard()
        {
            return gameBoard.boardLocations;
        }

        public void clearBoard()
        {
            gameBoard.boardLocations = new string[] {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
        }
    }
}
