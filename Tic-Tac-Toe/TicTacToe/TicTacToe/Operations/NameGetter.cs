﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.Models;

namespace TicTacToe.Operations
{
    class NameGetter
    {
        Player player1 = new Player();
        Player player2 = new Player();
        private string[] playerNames = new string[2];

        public string[] getPlayerNames()
        {
            Console.Write("\n\t\tPlayer 1, enter your name : ");
            player1.playerName = Console.ReadLine();
            playerNames[0] = player1.playerName;
            Console.Write("\n\t\tPlayer 2, enter your name : ");
            player2.playerName = Console.ReadLine();
            playerNames[1] = player2.playerName;
            return playerNames;
        }
    }
}
