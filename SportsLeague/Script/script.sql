USE [master]
GO
/****** Object:  Database [BasketballLeague]    Script Date: 2/28/2016 1:44:18 PM ******/
CREATE DATABASE [BasketballLeague]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BasketballLeague', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQL2014\MSSQL\DATA\BasketballLeague.mdf' , SIZE = 5056KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'BasketballLeague_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQL2014\MSSQL\DATA\BasketballLeague_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [BasketballLeague] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BasketballLeague].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BasketballLeague] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BasketballLeague] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BasketballLeague] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BasketballLeague] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BasketballLeague] SET ARITHABORT OFF 
GO
ALTER DATABASE [BasketballLeague] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BasketballLeague] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BasketballLeague] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BasketballLeague] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BasketballLeague] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BasketballLeague] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BasketballLeague] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BasketballLeague] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BasketballLeague] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BasketballLeague] SET  DISABLE_BROKER 
GO
ALTER DATABASE [BasketballLeague] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BasketballLeague] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BasketballLeague] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BasketballLeague] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BasketballLeague] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BasketballLeague] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BasketballLeague] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BasketballLeague] SET RECOVERY FULL 
GO
ALTER DATABASE [BasketballLeague] SET  MULTI_USER 
GO
ALTER DATABASE [BasketballLeague] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BasketballLeague] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BasketballLeague] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BasketballLeague] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [BasketballLeague] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'BasketballLeague', N'ON'
GO
USE [BasketballLeague]
GO
/****** Object:  Table [dbo].[Games]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Games](
	[GameID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [date] NOT NULL,
	[HomeTeamID] [int] NOT NULL,
	[AwayTeamID] [int] NOT NULL,
	[WinnerID] [int] NULL,
 CONSTRAINT [PK_Games] PRIMARY KEY CLUSTERED 
(
	[GameID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Leagues]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Leagues](
	[LeagueID] [int] IDENTITY(1,1) NOT NULL,
	[LeagueName] [nvarchar](25) NOT NULL,
 CONSTRAINT [PK_Leagues] PRIMARY KEY CLUSTERED 
(
	[LeagueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Manager]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Manager](
	[ManagerID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](25) NOT NULL,
	[LastName] [nvarchar](25) NOT NULL,
	[TeamID] [int] NOT NULL,
 CONSTRAINT [PK_Manager] PRIMARY KEY CLUSTERED 
(
	[ManagerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PlayerGameStats]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlayerGameStats](
	[StatsID] [int] IDENTITY(1,1) NOT NULL,
	[GameID] [int] NOT NULL,
	[PlayerID] [int] NOT NULL,
	[FGA] [int] NOT NULL,
	[FGM] [int] NOT NULL,
	[FTA] [int] NOT NULL,
	[FTM] [int] NOT NULL,
	[Steals] [int] NOT NULL,
	[Blocks] [int] NOT NULL,
	[Assist] [int] NOT NULL,
	[Rebounds] [int] NOT NULL,
	[Points] [int] NOT NULL,
	[Minutes] [int] NOT NULL,
 CONSTRAINT [PK_GameStats] PRIMARY KEY CLUSTERED 
(
	[StatsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PlayerPositions]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlayerPositions](
	[PlayerID] [int] NOT NULL,
	[PositionID] [int] NOT NULL,
 CONSTRAINT [PK_PlayerPositions] PRIMARY KEY CLUSTERED 
(
	[PlayerID] ASC,
	[PositionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Players]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Players](
	[PlayerID] [int] IDENTITY(1,1) NOT NULL,
	[TeamID] [int] NOT NULL,
	[FirstName] [nvarchar](20) NOT NULL,
	[LastName] [nvarchar](20) NOT NULL,
	[JerseyNumber] [int] NOT NULL,
 CONSTRAINT [PK_Players] PRIMARY KEY CLUSTERED 
(
	[PlayerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Positions]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Positions](
	[PositionID] [int] IDENTITY(1,1) NOT NULL,
	[PositionName] [nvarchar](15) NOT NULL,
 CONSTRAINT [PK_Positions] PRIMARY KEY CLUSTERED 
(
	[PositionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Teams]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Teams](
	[TeamID] [int] IDENTITY(1,1) NOT NULL,
	[LeagueID] [int] NOT NULL,
	[TeamName] [nvarchar](25) NOT NULL,
 CONSTRAINT [PK_Teams] PRIMARY KEY CLUSTERED 
(
	[TeamID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Games] ON 

INSERT [dbo].[Games] ([GameID], [Date], [HomeTeamID], [AwayTeamID], [WinnerID]) VALUES (2, CAST(N'2016-02-24' AS Date), 2, 1, NULL)
INSERT [dbo].[Games] ([GameID], [Date], [HomeTeamID], [AwayTeamID], [WinnerID]) VALUES (3, CAST(N'2016-01-11' AS Date), 1, 2, 1)
SET IDENTITY_INSERT [dbo].[Games] OFF
SET IDENTITY_INSERT [dbo].[Leagues] ON 

INSERT [dbo].[Leagues] ([LeagueID], [LeagueName]) VALUES (1, N'Basketball')
SET IDENTITY_INSERT [dbo].[Leagues] OFF
SET IDENTITY_INSERT [dbo].[Manager] ON 

INSERT [dbo].[Manager] ([ManagerID], [FirstName], [LastName], [TeamID]) VALUES (1, N'Minh', N'Chuong', 1)
INSERT [dbo].[Manager] ([ManagerID], [FirstName], [LastName], [TeamID]) VALUES (2, N'Isiah', N'Singh', 2)
SET IDENTITY_INSERT [dbo].[Manager] OFF
SET IDENTITY_INSERT [dbo].[PlayerGameStats] ON 

INSERT [dbo].[PlayerGameStats] ([StatsID], [GameID], [PlayerID], [FGA], [FGM], [FTA], [FTM], [Steals], [Blocks], [Assist], [Rebounds], [Points], [Minutes]) VALUES (1, 3, 9, 14, 8, 11, 6, 1, 0, 6, 12, 22, 36)
INSERT [dbo].[PlayerGameStats] ([StatsID], [GameID], [PlayerID], [FGA], [FGM], [FTA], [FTM], [Steals], [Blocks], [Assist], [Rebounds], [Points], [Minutes]) VALUES (2, 3, 14, 1, 0, 2, 1, 0, 0, 0, 1, 1, 16)
INSERT [dbo].[PlayerGameStats] ([StatsID], [GameID], [PlayerID], [FGA], [FGM], [FTA], [FTM], [Steals], [Blocks], [Assist], [Rebounds], [Points], [Minutes]) VALUES (3, 3, 5, 5, 4, 0, 0, 0, 1, 1, 9, 8, 21)
INSERT [dbo].[PlayerGameStats] ([StatsID], [GameID], [PlayerID], [FGA], [FGM], [FTA], [FTM], [Steals], [Blocks], [Assist], [Rebounds], [Points], [Minutes]) VALUES (4, 3, 7, 27, 11, 6, 5, 1, 0, 6, 2, 31, 38)
INSERT [dbo].[PlayerGameStats] ([StatsID], [GameID], [PlayerID], [FGA], [FGM], [FTA], [FTM], [Steals], [Blocks], [Assist], [Rebounds], [Points], [Minutes]) VALUES (5, 3, 15, 15, 6, 3, 3, 2, 0, 2, 7, 17, 35)
INSERT [dbo].[PlayerGameStats] ([StatsID], [GameID], [PlayerID], [FGA], [FGM], [FTA], [FTM], [Steals], [Blocks], [Assist], [Rebounds], [Points], [Minutes]) VALUES (6, 3, 10, 4, 3, 2, 2, 0, 0, 1, 5, 9, 30)
INSERT [dbo].[PlayerGameStats] ([StatsID], [GameID], [PlayerID], [FGA], [FGM], [FTA], [FTM], [Steals], [Blocks], [Assist], [Rebounds], [Points], [Minutes]) VALUES (7, 3, 4, 10, 4, 4, 4, 1, 0, 1, 6, 12, 27)
INSERT [dbo].[PlayerGameStats] ([StatsID], [GameID], [PlayerID], [FGA], [FGM], [FTA], [FTM], [Steals], [Blocks], [Assist], [Rebounds], [Points], [Minutes]) VALUES (8, 3, 8, 0, 0, 2, 1, 0, 0, 0, 2, 1, 5)
INSERT [dbo].[PlayerGameStats] ([StatsID], [GameID], [PlayerID], [FGA], [FGM], [FTA], [FTM], [Steals], [Blocks], [Assist], [Rebounds], [Points], [Minutes]) VALUES (9, 3, 17, 4, 3, 0, 0, 0, 0, 0, 1, 6, 6)
INSERT [dbo].[PlayerGameStats] ([StatsID], [GameID], [PlayerID], [FGA], [FGM], [FTA], [FTM], [Steals], [Blocks], [Assist], [Rebounds], [Points], [Minutes]) VALUES (10, 3, 11, 3, 1, 0, 0, 0, 0, 6, 4, 2, 17)
INSERT [dbo].[PlayerGameStats] ([StatsID], [GameID], [PlayerID], [FGA], [FGM], [FTA], [FTM], [Steals], [Blocks], [Assist], [Rebounds], [Points], [Minutes]) VALUES (11, 3, 6, 3, 1, 0, 0, 0, 0, 2, 0, 2, 10)
INSERT [dbo].[PlayerGameStats] ([StatsID], [GameID], [PlayerID], [FGA], [FGM], [FTA], [FTM], [Steals], [Blocks], [Assist], [Rebounds], [Points], [Minutes]) VALUES (12, 3, 19, 15, 6, 4, 3, 0, 2, 4, 12, 15, 36)
INSERT [dbo].[PlayerGameStats] ([StatsID], [GameID], [PlayerID], [FGA], [FGM], [FTA], [FTM], [Steals], [Blocks], [Assist], [Rebounds], [Points], [Minutes]) VALUES (13, 3, 20, 12, 6, 0, 0, 0, 0, 2, 10, 14, 33)
INSERT [dbo].[PlayerGameStats] ([StatsID], [GameID], [PlayerID], [FGA], [FGM], [FTA], [FTM], [Steals], [Blocks], [Assist], [Rebounds], [Points], [Minutes]) VALUES (14, 3, 31, 8, 3, 0, 0, 1, 1, 1, 3, 7, 26)
INSERT [dbo].[PlayerGameStats] ([StatsID], [GameID], [PlayerID], [FGA], [FGM], [FTA], [FTM], [Steals], [Blocks], [Assist], [Rebounds], [Points], [Minutes]) VALUES (15, 3, 21, 10, 5, 2, 1, 0, 0, 5, 5, 13, 36)
INSERT [dbo].[PlayerGameStats] ([StatsID], [GameID], [PlayerID], [FGA], [FGM], [FTA], [FTM], [Steals], [Blocks], [Assist], [Rebounds], [Points], [Minutes]) VALUES (16, 3, 29, 20, 6, 8, 8, 0, 1, 11, 7, 20, 33)
INSERT [dbo].[PlayerGameStats] ([StatsID], [GameID], [PlayerID], [FGA], [FGM], [FTA], [FTM], [Steals], [Blocks], [Assist], [Rebounds], [Points], [Minutes]) VALUES (17, 3, 23, 1, 0, 0, 0, 0, 0, 1, 3, 0, 7)
INSERT [dbo].[PlayerGameStats] ([StatsID], [GameID], [PlayerID], [FGA], [FGM], [FTA], [FTM], [Steals], [Blocks], [Assist], [Rebounds], [Points], [Minutes]) VALUES (18, 3, 27, 4, 3, 0, 0, 0, 0, 0, 2, 6, 5)
INSERT [dbo].[PlayerGameStats] ([StatsID], [GameID], [PlayerID], [FGA], [FGM], [FTA], [FTM], [Steals], [Blocks], [Assist], [Rebounds], [Points], [Minutes]) VALUES (19, 3, 22, 13, 6, 1, 1, 1, 0, 2, 2, 15, 34)
INSERT [dbo].[PlayerGameStats] ([StatsID], [GameID], [PlayerID], [FGA], [FGM], [FTA], [FTM], [Steals], [Blocks], [Assist], [Rebounds], [Points], [Minutes]) VALUES (20, 3, 24, 7, 6, 1, 1, 0, 0, 5, 3, 13, 28)
INSERT [dbo].[PlayerGameStats] ([StatsID], [GameID], [PlayerID], [FGA], [FGM], [FTA], [FTM], [Steals], [Blocks], [Assist], [Rebounds], [Points], [Minutes]) VALUES (21, 3, 26, 1, 0, 0, 0, 0, 0, 0, 0, 0, 4)
SET IDENTITY_INSERT [dbo].[PlayerGameStats] OFF
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (3, 1)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (3, 2)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (4, 3)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (4, 4)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (5, 5)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (6, 2)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (7, 1)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (7, 2)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (8, 5)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (9, 3)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (9, 4)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (10, 2)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (10, 3)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (11, 1)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (11, 2)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (12, 4)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (13, 3)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (14, 2)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (14, 3)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (15, 2)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (16, 4)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (16, 5)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (17, 4)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (17, 5)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (19, 4)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (19, 5)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (20, 3)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (21, 2)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (22, 3)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (23, 4)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (24, 2)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (25, 4)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (26, 2)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (27, 4)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (27, 5)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (28, 2)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (29, 1)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (30, 5)
INSERT [dbo].[PlayerPositions] ([PlayerID], [PositionID]) VALUES (31, 4)
SET IDENTITY_INSERT [dbo].[Players] ON 

INSERT [dbo].[Players] ([PlayerID], [TeamID], [FirstName], [LastName], [JerseyNumber]) VALUES (3, 1, N'Leandro', N'Barbosa', 19)
INSERT [dbo].[Players] ([PlayerID], [TeamID], [FirstName], [LastName], [JerseyNumber]) VALUES (4, 1, N'Harrison', N'Barnes', 40)
INSERT [dbo].[Players] ([PlayerID], [TeamID], [FirstName], [LastName], [JerseyNumber]) VALUES (5, 1, N'Andrew', N'Bogut', 12)
INSERT [dbo].[Players] ([PlayerID], [TeamID], [FirstName], [LastName], [JerseyNumber]) VALUES (6, 1, N'Ian', N'Clark', 21)
INSERT [dbo].[Players] ([PlayerID], [TeamID], [FirstName], [LastName], [JerseyNumber]) VALUES (7, 1, N'Stephen', N'Curry', 30)
INSERT [dbo].[Players] ([PlayerID], [TeamID], [FirstName], [LastName], [JerseyNumber]) VALUES (8, 1, N'Festus', N'Ezeli', 31)
INSERT [dbo].[Players] ([PlayerID], [TeamID], [FirstName], [LastName], [JerseyNumber]) VALUES (9, 1, N'Draymond', N'Green', 23)
INSERT [dbo].[Players] ([PlayerID], [TeamID], [FirstName], [LastName], [JerseyNumber]) VALUES (10, 1, N'Andre', N'Igoudala', 9)
INSERT [dbo].[Players] ([PlayerID], [TeamID], [FirstName], [LastName], [JerseyNumber]) VALUES (11, 1, N'Shaun', N'Livingston', 34)
INSERT [dbo].[Players] ([PlayerID], [TeamID], [FirstName], [LastName], [JerseyNumber]) VALUES (12, 1, N'Kevon', N'Looney', 36)
INSERT [dbo].[Players] ([PlayerID], [TeamID], [FirstName], [LastName], [JerseyNumber]) VALUES (13, 1, N'James Michael', N'McAdoo', 20)
INSERT [dbo].[Players] ([PlayerID], [TeamID], [FirstName], [LastName], [JerseyNumber]) VALUES (14, 1, N'Brandon', N'Rush', 4)
INSERT [dbo].[Players] ([PlayerID], [TeamID], [FirstName], [LastName], [JerseyNumber]) VALUES (15, 1, N'Klay', N'Thompson', 11)
INSERT [dbo].[Players] ([PlayerID], [TeamID], [FirstName], [LastName], [JerseyNumber]) VALUES (16, 1, N'Jason', N'Thompson', 1)
INSERT [dbo].[Players] ([PlayerID], [TeamID], [FirstName], [LastName], [JerseyNumber]) VALUES (17, 1, N'Marreese', N'Speights', 5)
INSERT [dbo].[Players] ([PlayerID], [TeamID], [FirstName], [LastName], [JerseyNumber]) VALUES (19, 2, N'Chris', N'Bosh', 1)
INSERT [dbo].[Players] ([PlayerID], [TeamID], [FirstName], [LastName], [JerseyNumber]) VALUES (20, 2, N'Luol', N'Deng', 9)
INSERT [dbo].[Players] ([PlayerID], [TeamID], [FirstName], [LastName], [JerseyNumber]) VALUES (21, 2, N'Goran', N'Dragic', 7)
INSERT [dbo].[Players] ([PlayerID], [TeamID], [FirstName], [LastName], [JerseyNumber]) VALUES (22, 2, N'Gerald', N'Green', 14)
INSERT [dbo].[Players] ([PlayerID], [TeamID], [FirstName], [LastName], [JerseyNumber]) VALUES (23, 2, N'Udonis', N'Haslem', 40)
INSERT [dbo].[Players] ([PlayerID], [TeamID], [FirstName], [LastName], [JerseyNumber]) VALUES (24, 2, N'Tyler', N'Johnson', 8)
INSERT [dbo].[Players] ([PlayerID], [TeamID], [FirstName], [LastName], [JerseyNumber]) VALUES (25, 2, N'Josh', N'McRoberts', 4)
INSERT [dbo].[Players] ([PlayerID], [TeamID], [FirstName], [LastName], [JerseyNumber]) VALUES (26, 2, N'Josh', N'Richardson', 0)
INSERT [dbo].[Players] ([PlayerID], [TeamID], [FirstName], [LastName], [JerseyNumber]) VALUES (27, 2, N'Amar''e', N'Stoudemire', 5)
INSERT [dbo].[Players] ([PlayerID], [TeamID], [FirstName], [LastName], [JerseyNumber]) VALUES (28, 2, N'Beno', N'Udrih', 19)
INSERT [dbo].[Players] ([PlayerID], [TeamID], [FirstName], [LastName], [JerseyNumber]) VALUES (29, 2, N'Dwayne', N'Wade', 3)
INSERT [dbo].[Players] ([PlayerID], [TeamID], [FirstName], [LastName], [JerseyNumber]) VALUES (30, 2, N'Hassan', N'Whiteside', 21)
INSERT [dbo].[Players] ([PlayerID], [TeamID], [FirstName], [LastName], [JerseyNumber]) VALUES (31, 2, N'Justise', N'Winslow', 20)
SET IDENTITY_INSERT [dbo].[Players] OFF
SET IDENTITY_INSERT [dbo].[Positions] ON 

INSERT [dbo].[Positions] ([PositionID], [PositionName]) VALUES (1, N'Point Guard')
INSERT [dbo].[Positions] ([PositionID], [PositionName]) VALUES (2, N'Shooting Guard')
INSERT [dbo].[Positions] ([PositionID], [PositionName]) VALUES (3, N'Small Forward')
INSERT [dbo].[Positions] ([PositionID], [PositionName]) VALUES (4, N'Power Forward')
INSERT [dbo].[Positions] ([PositionID], [PositionName]) VALUES (5, N'Center')
SET IDENTITY_INSERT [dbo].[Positions] OFF
SET IDENTITY_INSERT [dbo].[Teams] ON 

INSERT [dbo].[Teams] ([TeamID], [LeagueID], [TeamName]) VALUES (1, 1, N'Warriors')
INSERT [dbo].[Teams] ([TeamID], [LeagueID], [TeamName]) VALUES (2, 1, N'Heat')
SET IDENTITY_INSERT [dbo].[Teams] OFF
ALTER TABLE [dbo].[Games]  WITH CHECK ADD  CONSTRAINT [FK_Games_Teams] FOREIGN KEY([HomeTeamID])
REFERENCES [dbo].[Teams] ([TeamID])
GO
ALTER TABLE [dbo].[Games] CHECK CONSTRAINT [FK_Games_Teams]
GO
ALTER TABLE [dbo].[Games]  WITH CHECK ADD  CONSTRAINT [FK_Games_Teams1] FOREIGN KEY([AwayTeamID])
REFERENCES [dbo].[Teams] ([TeamID])
GO
ALTER TABLE [dbo].[Games] CHECK CONSTRAINT [FK_Games_Teams1]
GO
ALTER TABLE [dbo].[Games]  WITH CHECK ADD  CONSTRAINT [FK_Games_Teams2] FOREIGN KEY([WinnerID])
REFERENCES [dbo].[Teams] ([TeamID])
GO
ALTER TABLE [dbo].[Games] CHECK CONSTRAINT [FK_Games_Teams2]
GO
ALTER TABLE [dbo].[Manager]  WITH CHECK ADD  CONSTRAINT [FK_Manager_Teams] FOREIGN KEY([TeamID])
REFERENCES [dbo].[Teams] ([TeamID])
GO
ALTER TABLE [dbo].[Manager] CHECK CONSTRAINT [FK_Manager_Teams]
GO
ALTER TABLE [dbo].[PlayerGameStats]  WITH CHECK ADD  CONSTRAINT [FK_GameStats_Games] FOREIGN KEY([GameID])
REFERENCES [dbo].[Games] ([GameID])
GO
ALTER TABLE [dbo].[PlayerGameStats] CHECK CONSTRAINT [FK_GameStats_Games]
GO
ALTER TABLE [dbo].[PlayerGameStats]  WITH CHECK ADD  CONSTRAINT [FK_GameStats_Players] FOREIGN KEY([PlayerID])
REFERENCES [dbo].[Players] ([PlayerID])
GO
ALTER TABLE [dbo].[PlayerGameStats] CHECK CONSTRAINT [FK_GameStats_Players]
GO
ALTER TABLE [dbo].[PlayerPositions]  WITH CHECK ADD  CONSTRAINT [FK_PlayerPositions_Players] FOREIGN KEY([PlayerID])
REFERENCES [dbo].[Players] ([PlayerID])
GO
ALTER TABLE [dbo].[PlayerPositions] CHECK CONSTRAINT [FK_PlayerPositions_Players]
GO
ALTER TABLE [dbo].[PlayerPositions]  WITH CHECK ADD  CONSTRAINT [FK_PlayerPositions_Positions] FOREIGN KEY([PositionID])
REFERENCES [dbo].[Positions] ([PositionID])
GO
ALTER TABLE [dbo].[PlayerPositions] CHECK CONSTRAINT [FK_PlayerPositions_Positions]
GO
ALTER TABLE [dbo].[Players]  WITH CHECK ADD  CONSTRAINT [FK_Players_Teams] FOREIGN KEY([TeamID])
REFERENCES [dbo].[Teams] ([TeamID])
GO
ALTER TABLE [dbo].[Players] CHECK CONSTRAINT [FK_Players_Teams]
GO
ALTER TABLE [dbo].[Teams]  WITH CHECK ADD  CONSTRAINT [FK_Teams_Leagues] FOREIGN KEY([LeagueID])
REFERENCES [dbo].[Leagues] ([LeagueID])
GO
ALTER TABLE [dbo].[Teams] CHECK CONSTRAINT [FK_Teams_Leagues]
GO
/****** Object:  StoredProcedure [dbo].[AddPlayerPositionsForNewPlayer]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddPlayerPositionsForNewPlayer]
(
	@PlayerID int,
	@PositionID int 
)
AS

INSERT INTO PlayerPositions(PlayerID, PositionID)
VALUES(@PlayerID, @PositionID)



GO
/****** Object:  StoredProcedure [dbo].[CreateAGame]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CreateAGame]
(
	@GameID int OUTPUT,
	@GameDate date,
	@HomeTeamID int,
	@AwayTeamID	int,
	@WinnerID int
) 
AS

INSERT INTO Games
([Date], HomeTeamID, AwayTeamID, WinnerID)
VALUES (@GameDate, @HomeTeamID, @AwayTeamID, @WinnerID)
SET @GameID = SCOPE_IDENTITY()




GO
/****** Object:  StoredProcedure [dbo].[CreateAManager]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CreateAManager]
(
	@ManagerID int OUTPUT,
	@FirstName nvarchar(25),
	@LastName nvarchar(25),
	@TeamID int
)

AS

INSERT INTO Manager(FirstName, LastName, TeamID)
VALUES (@FirstName, @LastName, @TeamID)
SET @ManagerID = SCOPE_IDENTITY()







GO
/****** Object:  StoredProcedure [dbo].[CreateAPlayer]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CreateAPlayer]
(
	@PlayerID int OUTPUT,
	@TeamID int,
	@FirstName nvarchar(25),
	@LastName nvarchar(25),
	@JerseyNumber int
) 
AS

INSERT INTO Players
(TeamID, FirstName, LastName, JerseyNumber)
VALUES (@TeamID,@FirstName, @LastName, @JerseyNumber)
SET @PlayerID = SCOPE_IDENTITY()




GO
/****** Object:  StoredProcedure [dbo].[CreateLeague]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CreateLeague]
(
	@LeagueID int OUTPUT,
	@LeagueName nvarchar(25)
) 
AS

INSERT INTO Leagues
(LeagueName)
VALUES (@LeagueName)
SET @LeagueID = SCOPE_IDENTITY()





GO
/****** Object:  StoredProcedure [dbo].[CreateTeam]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CreateTeam]
(
	@TeamID int OUTPUT,
	@TeamName nvarchar(25),
	@LeagueId int
) 
AS

INSERT INTO Teams
(TeamName, LeagueID)
VALUES (@TeamName, @LeagueId)
SET @TeamID = SCOPE_IDENTITY()




GO
/****** Object:  StoredProcedure [dbo].[DeleteAGame]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DeleteAGame]
(
	@GameID int
) 
AS

DELETE FROM Games
WHERE GameID = @GameID




GO
/****** Object:  StoredProcedure [dbo].[DeleteLeague]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DeleteLeague]
(
	@LeagueID int
)AS

DELETE FROM Leagues
WHERE LeagueID = @LeagueID




GO
/****** Object:  StoredProcedure [dbo].[DeleteManager]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DeleteManager]
(
	@ManagerID int
)

AS

DELETE FROM Manager 
WHERE ManagerID = @ManagerID





GO
/****** Object:  StoredProcedure [dbo].[DeletePlayer]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DeletePlayer]	
(
	@PlayerID int
)

AS

DELETE FROM Players
WHERE PlayerID = @PlayerID





GO
/****** Object:  StoredProcedure [dbo].[DeleteTeam]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DeleteTeam]
(
	@TeamID int
) 
AS

DELETE FROM Teams
WHERE TeamID = @TeamID




GO
/****** Object:  StoredProcedure [dbo].[GetAllGames]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetAllGames]

AS

SELECT GameID, [Date] AS GameDate, HomeTeamID, AwayTeamID, WinnerID
FROM Games




GO
/****** Object:  StoredProcedure [dbo].[GetAllLeagues]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetAllLeagues] AS

SELECT *
FROM Leagues



GO
/****** Object:  StoredProcedure [dbo].[GetAllManagers]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetAllManagers]

AS

SELECT * 
FROM Manager





GO
/****** Object:  StoredProcedure [dbo].[GetAllPlayerGameStats]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAllPlayerGameStats]

AS

SELECT *
FROM PlayerGameStats
GO
/****** Object:  StoredProcedure [dbo].[GetAllPlayers]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetAllPlayers] AS

SELECT *
FROM Players



GO
/****** Object:  StoredProcedure [dbo].[GetAllPositions]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetAllPositions]

AS

SELECT * 
FROM Positions



GO
/****** Object:  StoredProcedure [dbo].[GetAllTeams]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetAllTeams]
 
AS

SELECT *
FROM Teams


GO
/****** Object:  StoredProcedure [dbo].[GetManagerByID]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetManagerByID]
(
	@ManagerID int
)

AS

SELECT * 
FROM Manager
WHERE ManagerID = @ManagerID





GO
/****** Object:  StoredProcedure [dbo].[GetPlayerByID]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetPlayerByID]	
(
	@PlayerID int
)

AS

SELECT *
FROM Players
WHERE PlayerID = @PlayerID





GO
/****** Object:  StoredProcedure [dbo].[GetPlayersForAPosition]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPlayersForAPosition]
(
	@PositionID int
)
AS

SELECT * 
FROM PlayerPositions
INNER JOIN Players
ON PlayerPositions.PlayerID = Players.PlayerID
WHERE PlayerPositions.PositionID = @PositionID



GO
/****** Object:  StoredProcedure [dbo].[GetPositionsForPlayer]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPositionsForPlayer]
(
	@PlayerID int
)
AS

SELECT * 
FROM PlayerPositions
INNER JOIN Positions
ON PlayerPositions.PositionID = Positions.PositionID
WHERE PlayerPositions.PlayerID = @PlayerID



GO
/****** Object:  StoredProcedure [dbo].[GetSpecificGame]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetSpecificGame]
(
@GameID int 
)AS

SELECT *
FROM Games
WHERE GameID = @GameID


GO
/****** Object:  StoredProcedure [dbo].[GetSpecificLeague]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetSpecificLeague]
(
	@LeagueID int
)AS

SELECT *
FROM Leagues
WHERE LeagueID = @LeagueID




GO
/****** Object:  StoredProcedure [dbo].[GetTeamById]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetTeamById]
(
	@TeamID int	
)AS

SELECT *
FROM Teams
WHERE TeamID = @TeamID




GO
/****** Object:  StoredProcedure [dbo].[RemoveAllPlayerPositionsForPlayer]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RemoveAllPlayerPositionsForPlayer]
(
	@PlayerID int 
)
AS

DELETE FROM PlayerPositions
WHERE PlayerID = @PlayerID



GO
/****** Object:  StoredProcedure [dbo].[UpdateLeague]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UpdateLeague]
(
	@LeagueID int,
	@LeagueName nvarchar(25)
)AS

UPDATE Leagues
SET LeagueName = @LeagueName
WHERE LeagueID = @LeagueID




GO
/****** Object:  StoredProcedure [dbo].[UpdateManager]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UpdateManager]
(
	@ManagerID int,
	@FirstName nvarchar(25),
	@LastName nvarchar(25),
	@TeamID int
)

AS

UPDATE Manager
SET FirstName = @FirstName,
LastName = @LastName,
TeamID = @TeamID
WHERE ManagerID = @ManagerID






GO
/****** Object:  StoredProcedure [dbo].[UpdatePlayer]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UpdatePlayer]	
(
	@PlayerID int,
	@TeamID int,
	@FirstName nvarchar(25), 
	@LastName nvarchar(25),
	@JerseyNumber int
)

AS

UPDATE Players
SET TeamID = @TeamID,
FirstName = @FirstName,
LastName = @LastName,
JerseyNumber = @JerseyNumber
WHERE PlayerID = @PlayerID





GO
/****** Object:  StoredProcedure [dbo].[UpdateTeam]    Script Date: 2/28/2016 1:44:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UpdateTeam]
(
	@TeamID int,
	@TeamName nvarchar(25),
	@LeagueId int
) 
AS

UPDATE Teams
SET TeamName = @TeamName,
LeagueID = @LeagueId
WHERE TeamID = @TeamID




GO
USE [master]
GO
ALTER DATABASE [BasketballLeague] SET  READ_WRITE 
GO
