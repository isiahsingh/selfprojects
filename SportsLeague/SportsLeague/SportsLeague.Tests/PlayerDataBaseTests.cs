﻿using System.Collections.Generic;
using NUnit.Framework;
using SportsLeague.Data.Repositories;
using SportsLeague.Model.Models;

namespace SportsLeague.Tests
{

    [TestFixture]
    class PlayerDataBaseTests
    {
        [Test]
        public void TestGetPlayerList()
        {
            List<Player> players = new List<Player>();


            PlayerRepo repo = new PlayerRepo();
            players = repo.GetAll();

            var actual = players.Count;

            Assert.AreEqual(28, actual);
        }

        [Test]
        public void TestGetSinglePlayerByID()
        {
            Player player = new Player();
            int id = 4;

            PlayerRepo repo = new PlayerRepo();

            player = repo.GetBySpecificId(id);
            Assert.AreEqual("Harrison", player.FirstName);
        }

        //[Test]
        //public void TestRemovePlayer()
        //{
            
        //}

        //[Test]
        //public void UpdatePlayer()
        //{
            
        //}


    }
}
