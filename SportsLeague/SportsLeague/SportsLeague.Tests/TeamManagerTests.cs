﻿using System.Linq;
using NUnit.Framework;
using SportsLeague.BLL.Managers;
using SportsLeague.Model.Models;

namespace SportsLeague.Tests
{
    [TestFixture]
    public class TeamManagerTests
    {
        //Get all the players that is in a specific team
        [Test]
        public void GetAllPlayersForSpecificTeamTest()
        {
            TeamManager manager = new TeamManager();
            Response<Team> teamPlayers = manager.GetATeam(1);

            Assert.AreEqual(15, teamPlayers.Data.Players.Count);
        }

        //Get all the games that a single team is in
        [Test]
        public void GetAllGamesForSpecificTeamTest()
        {
            TeamManager manager = new TeamManager();
            Response<Team> teamPlayers = manager.GetATeam(1);

            Assert.AreEqual(2, teamPlayers.Data.Games.Count);
        }

        //Test to get a single team and check name
        [Test]
        public void GetNameForSpecificTeamTest()
        {
            TeamManager manager = new TeamManager();
            Response<Team> teamPlayers = manager.GetATeam(1);

            Assert.AreEqual("Warriors", teamPlayers.Data.TeamName);
        }

        //Test to see if we get all the team
        [Test]
        public void GetAllTeamForALeagueCountTest()
        {
            TeamManager manager = new TeamManager();
            var teams = manager.GetAllTeamByLeague(1);

            Assert.AreEqual(2, teams.Data.Count);
        }

        //Check to see if all the team players are filled
        [Test]
        public void GetPlayersCountForALeagueTeamTest()
        {
            TeamManager manager = new TeamManager();
            var teams = manager.GetAllTeamByLeague(1);

            var teamOne = teams.Data.FirstOrDefault(g => g.TeamId == 1);

            Assert.AreEqual(15, teamOne.Players.Count);
        }

        //Check to see if all the team games are filled
        [Test]
        public void GetTeamCountForALeagueTeamCountTest()
        {
            TeamManager manager = new TeamManager();
            var teams = manager.GetAllTeamByLeague(1);

            var teamOne = teams.Data.FirstOrDefault(g => g.TeamId == 1);

            Assert.AreEqual(2, teamOne.Games.Count);
        }
    }
}
