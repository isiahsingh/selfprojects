﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using NUnit.Framework;
using SportsLeague.Data.Repositories;
using SportsLeague.Data.Settings;
using SportsLeague.Model.Models;

namespace SportsLeague.Tests
{
    [TestFixture]
    class GameTests
    {
        [Test]
        public void TestGetAllGame()
        {
            var repo = new GameRepo();
            Assert.AreEqual(2, repo.GetAll().Count);
        }

        [Test]
        public void TestCreateGame()
        {
            Game gameToAdd = new Game
            {
                GameDate = DateTime.Now,
                HomeTeamId = 1,
                AwayTeamId = 2
            };
            var repo = new GameRepo();
            repo.Create(gameToAdd);
            var repo2 = new GameRepo();
            Assert.AreEqual(3, repo2.GetAll().Count);
        }

        [Test]
        public void TestRemoveGame()
        {
            var repo = new GameRepo();
            repo.Remove(6);
            var repo2 = new GameRepo();
            Assert.AreEqual(2, repo2.GetAll().Count);
        }

        [Test]
        public void TestGetBySpecificId()
        {
            var repo = new GameRepo();
            Game game = repo.GetBySpecificId(3);
            Assert.AreEqual(1, game.WinnerId);
        }

    }
}
