﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SportsLeague.Data.Repositories;
using SportsLeague.Data.Settings;

namespace SportsLeague.Tests
{
    [TestFixture]
    class DatabaseTests
    {
        [Test]
        public void GetPlayersTest()
        {
            var response = new List<string>();

            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand
                {
                    CommandText = "SELECT FirstName, LastName, Points FROM Players INNER JOIN GameStats " +
                                  "ON Players.PlayerID = GameStats.PlayerID " +
                                  "WHERE Players.LastName = 'Curry'",
                    Connection = cn
                };

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        response.Add(dr["FirstName"].ToString());
                        response.Add(dr["LastName"].ToString());
                        response.Add(dr["Points"].ToString());
                    }
                }
            }

            Assert.AreEqual("Stephen", response[0]);
            Assert.AreEqual("Curry", response[1]);
            Assert.AreEqual("31", response[2]);
        }
    }
}
