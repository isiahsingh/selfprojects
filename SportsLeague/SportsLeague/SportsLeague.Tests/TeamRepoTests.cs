﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SportsLeague.Data.Repositories;
using SportsLeague.Model.Models;

namespace SportsLeague.Tests
{
    [TestFixture]
    class TeamRepoTests
    {
        [Test]
        public void TestCreateTeamReturnsTeamID()
        {
            var repo = new TeamRepo();
            var team = new Team()
            {
                TeamName = "Angels",
                LeagueId = 1
            };

            var actual = repo.Create(team);
            Assert.IsNotNull(actual);
        }
    }
}
