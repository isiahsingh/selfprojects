﻿using System.Collections.Generic;

namespace SportsLeague.Model.Models
{
    public class Position
    {
        public int PositionId { get; set; }
        public string PositionName { get; set; }
        public List<Player> Players { get; set; }

        public Position()
        {
            Players = new List<Player>();
        }
    }
}
