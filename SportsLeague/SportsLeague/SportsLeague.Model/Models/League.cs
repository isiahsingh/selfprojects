﻿using System.Collections.Generic;

namespace SportsLeague.Model.Models
{
    public class League
    {
        public int LeagueId { get; set; }
        public string LeagueName { get; set; }
        public List<Team> Teams { get; set; }

        public League()
        {
            Teams = new List<Team>();
        }
    }
}
