﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SportsLeague.Model.Models
{
    public class Game : IValidatableObject
    {
        public int GameId { get; set; }
        [DataType(DataType.Date)]
        public DateTime GameDate { get; set; }
        public int HomeTeamId { get; set; }
        public int AwayTeamId { get; set; }
        public int? WinnerId { get; set; }
        public List<PlayerGameStat> PlayerGameStats { get; set; }

        public Game()
        {
            PlayerGameStats = new List<PlayerGameStat>();
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext context)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (GameDate < DateTime.Today)
            {
                errors.Add(new ValidationResult("Can only create a game in the future.",
                    new[] { "GameDate" }));
            }

            if (HomeTeamId == AwayTeamId)
            {
                errors.Add(new ValidationResult("A team can not play against themselves.",
                    new[] { "HomeTeamId", "AwayTeamId" }));
            }

            return errors;
        }
    }
}
