﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SportsLeague.Model.Models
{
    public class Team
    {
        public int TeamId { get; set; }
        public int LeagueId { get; set; }

        [Required]
        public string TeamName { get; set; }

        public List<Player> Players { get; set; }
        public List<Game> Games { get; set; }

        public Team()
        {
            Players = new List<Player>();
            Games = new List<Game>();
        }

    }
}
