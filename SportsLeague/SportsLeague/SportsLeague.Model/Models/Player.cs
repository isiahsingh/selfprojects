﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SportsLeague.Model.Models
{
    public class Player
    {
        public int PlayerId { get; set; }
        public int TeamId { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public int JerseyNumber { get; set; }
        public List<Position> Positions { get; set; }
        public List<PlayerGameStat> PlayerGameStats { get; set; }

        public Player()
        {
            Positions = new List<Position>();
            PlayerGameStats = new List<PlayerGameStat>();
        }
    }
}
