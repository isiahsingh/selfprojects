﻿namespace SportsLeague.Model.Models
{
    public class Manager
    {
        public int? ManagerId { get; set; }
        public int TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
