﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Modules;
using SportsLeague.BLL.Interfaces;
using SportsLeague.BLL.Managers;

namespace SportsLeague.BLL.Factory
{
    public class ManagerBindings : NinjectModule
    {
        public override void Load()
        {
            Bind<IGameManager>().To<GameManager>();
            Bind<ILeagueManager>().To<LeagueManager>();
            Bind<IPlayerManager>().To<PlayerManager>();
            Bind<ITeamManager>().To<TeamManager>();
        }
    }
}
