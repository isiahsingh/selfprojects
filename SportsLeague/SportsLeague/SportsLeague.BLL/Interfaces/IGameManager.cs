﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SportsLeague.Model.Models;

namespace SportsLeague.BLL.Interfaces
{
    public interface IGameManager
    {
        Response<Game> GetGameWithGameStats(int gameId);
    }
}
