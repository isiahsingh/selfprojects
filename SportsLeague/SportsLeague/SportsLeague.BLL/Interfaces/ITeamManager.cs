﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SportsLeague.Model.Models;

namespace SportsLeague.BLL.Interfaces
{
    public interface ITeamManager
    {
        Response<List<Team>> GetAllTeamByLeague(int id);
        Response<Team> GetATeam(int id);
    }
}
