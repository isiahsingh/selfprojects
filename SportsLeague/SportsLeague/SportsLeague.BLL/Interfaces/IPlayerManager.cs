﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SportsLeague.Model.Models;

namespace SportsLeague.BLL.Interfaces
{
    public interface IPlayerManager
    {
        Response<Player> GetPlayer(int id);
        Response<List<Player>> GetAllPlayers();
    }
}
