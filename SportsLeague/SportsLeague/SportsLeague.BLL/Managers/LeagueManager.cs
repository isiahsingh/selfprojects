﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Ninject;
using SportsLeague.BLL.Interfaces;
using SportsLeague.Data.Factory;
using SportsLeague.Data.Interfaces;
using SportsLeague.Data.Repositories;
using SportsLeague.Model.Models;

namespace SportsLeague.BLL.Managers
{
    public class LeagueManager : ILeagueManager
    {
        private readonly ILeagueRepository _leagueRepo;
        private readonly ITeamRepository _teamRepo;
        private readonly IPlayerRepository _playerRepo;
        private readonly IPositionRepository _positionRepo;

        public LeagueManager()
        {
            var kernel = new StandardKernel(new DataBindings());
            kernel.Load(Assembly.GetExecutingAssembly());
            _leagueRepo = kernel.Get<ILeagueRepository>();
            _teamRepo = kernel.Get<ITeamRepository>();
            _playerRepo = kernel.Get<IPlayerRepository>();
            _positionRepo = kernel.Get<IPositionRepository>();
        }

        public Response<List<League>> GetLeagues()
        {
            var response = new Response<List<League>>();
            try
            {
                List<League> leagueList = _leagueRepo.GetAll();
                foreach (var league in leagueList)
                {
                    league.Teams = _teamRepo.GetAll().Where(t => t.LeagueId == league.LeagueId).ToList();
                    foreach (var team in league.Teams)
                    {
                        team.Players = _playerRepo.GetAll().Where(p => p.TeamId == team.TeamId).ToList();
                        foreach (var player in team.Players)
                        {
                            player.Positions = _positionRepo.GetAllPositionsForPlayer(player.PlayerId);
                        }
                    }
                }
                response.Success = true;
                response.Data = leagueList;
            }
            catch (Exception)
            {
                response.Success = false;
            }

            return response;
        }
    }
}
