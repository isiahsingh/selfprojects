﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Ninject;
using SportsLeague.BLL.Interfaces;
using SportsLeague.Data.Factory;
using SportsLeague.Data.Interfaces;
using SportsLeague.Data.Repositories;
using SportsLeague.Model.Models;

namespace SportsLeague.BLL.Managers
{
    public class GameManager : IGameManager
    {
        private readonly IGameRepository _gameRepo;
        private readonly IPlayerGameStatRepository _statRepo;
        
        public GameManager()
        {
            var kernel = new StandardKernel(new DataBindings());
            kernel.Load(Assembly.GetExecutingAssembly());
            _gameRepo = kernel.Get<IGameRepository>();
            _statRepo = kernel.Get<IPlayerGameStatRepository>();
        }

        public Response<Game> GetGameWithGameStats(int gameId)
        {
            var response = new Response<Game>();
            try
            {
                Game game = _gameRepo.GetBySpecificId(gameId);
                List<PlayerGameStat> statList = _statRepo.GetAll().Where(s => s.GameId == gameId).ToList();
                game.PlayerGameStats = statList;
                response.Success = true;
                response.Data = game;
            }
            catch
            {
                response.Success = false;
            }
            return response;
        }

       
    }
}
