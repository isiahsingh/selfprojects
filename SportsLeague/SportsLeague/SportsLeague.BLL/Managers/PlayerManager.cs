﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Ninject;
using SportsLeague.BLL.Interfaces;
using SportsLeague.Data.Factory;
using SportsLeague.Data.Interfaces;
using SportsLeague.Data.Repositories;
using SportsLeague.Model.Models;

namespace SportsLeague.BLL.Managers
{
    public class PlayerManager : IPlayerManager
    {
        private readonly IPlayerRepository _playerRepo;
        private readonly IPositionRepository _positionRepo;
        private readonly IPlayerGameStatRepository _statRepo;

        public PlayerManager()
        {
            var kernel = new StandardKernel(new DataBindings());
            kernel.Load(Assembly.GetExecutingAssembly());
            _statRepo = kernel.Get<IPlayerGameStatRepository>();
            _playerRepo = kernel.Get<IPlayerRepository>();
            _positionRepo = kernel.Get<IPositionRepository>();
        }

        public Response<Player> GetPlayer(int id)
        {
            var response = new Response<Player>();
            try
            {
                List<Position> positions = _positionRepo.GetAllPositionsForPlayer(id);
                List<PlayerGameStat> stats = _statRepo.GetAll().Where(s => s.PlayerId == id).ToList();
                Player player = _playerRepo.GetBySpecificId(id);
                player.Positions = positions;
                player.PlayerGameStats = stats;
                response.Success = true;
                response.Data = player;
            }
            catch
            {
                response.Success = false;
            }
            return response;
        }

        public Response<List<Player>> GetAllPlayers()
        {
            var response = new Response<List<Player>>();
            try
            {
                List<Player> playerList = _playerRepo.GetAll();
                foreach (var player in playerList)
                {
                    player.PlayerGameStats = _statRepo.GetAll().Where(s => s.PlayerId == player.PlayerId).ToList();
                    player.Positions = _positionRepo.GetAllPositionsForPlayer(player.PlayerId);
                }
                response.Success = true;
                response.Data = playerList;
            }
            catch
            {
                response.Success = false;
            }
            return response;
        }

    }
}
