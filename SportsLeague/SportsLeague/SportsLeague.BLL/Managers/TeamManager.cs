﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Ninject;
using SportsLeague.BLL.Interfaces;
using SportsLeague.Data.Factory;
using SportsLeague.Data.Interfaces;
using SportsLeague.Data.Repositories;
using SportsLeague.Model.Models;

namespace SportsLeague.BLL.Managers
{
    public class TeamManager : ITeamManager
    {
        private readonly IPlayerRepository _playerRepo;
        private readonly ITeamRepository _teamRepo;
        private readonly IGameRepository _gameRepo;
        private readonly IPlayerGameStatRepository _statRepo;
        private readonly IPositionRepository _positionRepo;

        public TeamManager()
        {
            var kernel = new StandardKernel(new DataBindings());
            kernel.Load(Assembly.GetExecutingAssembly());
            _teamRepo = kernel.Get<ITeamRepository>();
            _playerRepo = kernel.Get<IPlayerRepository>();
            _gameRepo = kernel.Get<IGameRepository>();
            _statRepo = kernel.Get<IPlayerGameStatRepository>();
            _positionRepo = kernel.Get<IPositionRepository>();
        }

        public Response<List<Team>> GetAllTeamByLeague(int id)
        {
            var response = new Response<List<Team>>();
            try
            {
                List<Team> teams = _teamRepo.GetAll().Where(t => t.LeagueId == id).ToList();

                foreach (var team in teams)
                {
                    List<Player> players = _playerRepo.GetAll().Where(p => p.TeamId == team.TeamId).ToList();
                    List<Game> games = _gameRepo.GetAll().Where(g => g.AwayTeamId == team.TeamId || g.HomeTeamId == team.TeamId).ToList();

                    team.Players = players;
                    team.Games = games;
                }

                response.Success = true;
                response.Data = teams;
            }
            catch (Exception)
            {
                response.Success = false;
            }

            return response;
        }

        public Response<Team> GetATeam(int id)
        {
            var response = new Response<Team>();
            try
            {       
                Team team = _teamRepo.GetBySpecificId(id);

                List<Player> playerList = _playerRepo.GetAll().Where(p => p.TeamId == id).ToList();
                List<Game> gameList = _gameRepo.GetAll().Where(g => g.AwayTeamId == id || g.HomeTeamId == id).ToList();
                team.Players = playerList;
                team.Games = gameList;

                foreach (var player in team.Players)
                {
                    List<PlayerGameStat> gameStats = _statRepo.GetAll().Where(s => s.PlayerId == player.PlayerId).ToList();
                    player.PlayerGameStats = gameStats;
                    List<Position> playerPositions = _positionRepo.GetAllPositionsForPlayer(player.PlayerId);
                    player.Positions = playerPositions;
                }
             
                response.Success = true;
                response.Data = team;
            }
            catch (Exception)
            {
                response.Success = false;
            }

            return response;
        }
    }
}
