﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Ninject;
using Ninject.Modules;
using SportsLeague.BLL.Factory;
using SportsLeague.BLL.Interfaces;
using SportsLeague.BLL.Managers;
using SportsLeague.Data.Factory;
using SportsLeague.Data.Interfaces;
using SportsLeague.Data.Repositories;
using SportsLeague.Model.Models;
using SportsLeague.UI.Models;

namespace SportsLeague.UI.Controllers
{
    public class TeamController : Controller
    {
        private readonly ITeamRepository _teamRepo;
        private readonly IPlayerRepository _playerRepo;
        private readonly ILeagueManager _leagueManager;
        private readonly ITeamManager _teamManager;

        public TeamController()
        {
            var kernel = new StandardKernel(new ManagerBindings(), new DataBindings());
            kernel.Load(Assembly.GetExecutingAssembly());
            _teamRepo = kernel.Get<ITeamRepository>();
            _playerRepo = kernel.Get<IPlayerRepository>();
            _leagueManager = kernel.Get<ILeagueManager>();
            _teamManager = kernel.Get<ITeamManager>();
        }

        public ActionResult CreateTeam()
        {
            return View(_leagueManager.GetLeagues().Data.FirstOrDefault());
        }

        [HttpPost]
        public ActionResult CreateTeamPost(Team team)
        {
            int teamID = _teamRepo.Create(team);
            Team newTeam = _teamManager.GetATeam(teamID).Data;
            var TeamAndPlayer = new TeamPlayersVm()
            {
                NewTeam = newTeam,
                NewPlayer = new Player()
            };
            return View("GetPlayers", TeamAndPlayer);
        }

        [HttpPost]
        public ActionResult GetPlayers(TeamPlayersVm viewModel, int positionID)
        {
            viewModel.NewPlayer.TeamId = viewModel.NewTeam.TeamId;
            viewModel.NewPlayer.Positions.Add(new Position {PositionId = positionID});
            _playerRepo.Create(viewModel.NewPlayer);

            ModelState?.Clear();
            viewModel.NewTeam = _teamManager.GetATeam(viewModel.NewTeam.TeamId).Data;

            viewModel.NewPlayer = new Player();
            
            return View(viewModel);
        }

        public ActionResult TeamHomepage(int id)
        {
            var result = _teamManager.GetATeam(id).Data;
            return View(result);
        }

        [HttpPost]
        public ActionResult DeletePlayer(int playerID, int teamID)
        {
            _playerRepo.Remove(playerID);
            return RedirectToAction("TeamHomepage", new {id = teamID});
        }

        public ActionResult AddPlayer(int teamId)
        {
            Team team = _teamManager.GetATeam(teamId).Data;
            TeamPlayersVm TeamAndPlayer = new TeamPlayersVm()
            {
                NewTeam = team,
                NewPlayer = new Player()
            };
            return View("GetPlayers", TeamAndPlayer);
        }

        public ActionResult TradePlayer()
        {
            return View(_leagueManager.GetLeagues().Data.FirstOrDefault().Teams);
        }

        public ActionResult SelectPlayerTrade(int team1, int team2)
        {
            var vm = new TradePlayerVm();
            vm.Team1 = _teamManager.GetATeam(team1).Data;
            vm.Team2 = _teamManager.GetATeam(team2).Data;

            return View(vm);
        }

        [HttpPost]
        public ActionResult CompletePlayerTrade(int player1, int player2, TradePlayerVm teams)
        {
            var manager = new PlayerManager();
            var firstPlayer = manager.GetPlayer(player1);
            var secondPlayer = manager.GetPlayer(player2);

            firstPlayer.Data.TeamId = teams.Team2.TeamId;
            secondPlayer.Data.TeamId = teams.Team1.TeamId;

            var repo = new PlayerRepo();
            repo.Update(firstPlayer.Data);
            repo.Update(secondPlayer.Data);

            return RedirectToAction("ViewTeams", "League");
        }
     
    }
}