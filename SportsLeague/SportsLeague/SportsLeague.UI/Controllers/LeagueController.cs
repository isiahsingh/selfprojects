﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Ninject;
using SportsLeague.BLL.Factory;
using SportsLeague.BLL.Interfaces;
using SportsLeague.BLL.Managers;
using SportsLeague.Data.Factory;
using SportsLeague.Data.Interfaces;

namespace SportsLeague.UI.Controllers
{
    public class LeagueController : Controller
    {
        private readonly ILeagueManager _leagueManager;

        public LeagueController()
        {
            var kernel = new StandardKernel(new ManagerBindings(), new DataBindings());
            kernel.Load(Assembly.GetExecutingAssembly());
            _leagueManager = kernel.Get<ILeagueManager>();
        }

        public ActionResult ViewTeams()
        {
            return View(_leagueManager.GetLeagues().Data.First());
        }
    }
}