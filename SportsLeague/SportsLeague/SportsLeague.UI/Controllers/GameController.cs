﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using Ninject;
using SportsLeague.BLL.Factory;
using SportsLeague.BLL.Managers;
using SportsLeague.Data.Factory;
using SportsLeague.Data.Interfaces;
using SportsLeague.Data.Repositories;
using SportsLeague.Model.Models;
using SportsLeague.UI.Models;

namespace SportsLeague.UI.Controllers
{
    public class GameController : Controller
    {
        private readonly IGameRepository _gameRepo;
        
        public GameController()
        {
            var kernel = new StandardKernel(new ManagerBindings(), new DataBindings());
            kernel.Load(Assembly.GetExecutingAssembly());
            _gameRepo = kernel.Get<IGameRepository>();
        }

        public ActionResult Index()
        {
            var viewModel = new TeamGameVm();
            return View(viewModel);
        }
        
        [HttpPost]
        public ActionResult AddGame(TeamGameVm viewModel)
        {
            if (ModelState.IsValid)
            {
                int gameId = _gameRepo.Create(viewModel.NewGame);
            }
            return RedirectToAction("Index");
        }

        public ActionResult GameResults(int id)
        {
            GamePlayerStatsVM viewModel = new GamePlayerStatsVM(id);
            return View(viewModel);
        }
    }
}