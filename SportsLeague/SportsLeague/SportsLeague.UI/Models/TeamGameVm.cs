﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using SportsLeague.BLL.Managers;
using SportsLeague.Data.Repositories;
using SportsLeague.Model.Models;

namespace SportsLeague.UI.Models
{
    public class TeamGameVm
    {
        public List<Game> GameList { get; set; }
        public List<Team> TeamList { get; set; }
        public Game NewGame { get; set; }
        public List<SelectListItem> TeamListItems { get; set; }

        public TeamGameVm()
        {
            var gameRepo = new GameRepo();
            var teamRepo = new TeamRepo();
            GameList = gameRepo.GetAll().OrderByDescending(d => d.GameDate).ToList();
            TeamList = teamRepo.GetAll();
            TeamListItems = ConvertTeamsToItems(TeamList);
        }

        private List<SelectListItem> ConvertTeamsToItems(List<Team> teamList)
        {
            var itemList = new List<SelectListItem>();
            itemList = teamList.Select(t => new SelectListItem
            {
                Text = t.TeamName,
                Value = t.TeamId.ToString()
            }).ToList();
            return itemList;
        } 

        public string GetGameHomeTeam(Game game)
        {
            Team homeTeam = TeamList.FirstOrDefault(t => t.TeamId == game.HomeTeamId);
            if (homeTeam != null)
                return homeTeam.TeamName;
            else
                return "";
        }

        public string GetGameAwayTeam(Game game)
        {
            Team awayTeam = TeamList.FirstOrDefault(t => t.TeamId == game.AwayTeamId);
            if (awayTeam != null)
                return awayTeam.TeamName;
            else
                return "";
        }
    }
}