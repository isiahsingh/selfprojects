﻿using System.Collections.Generic;
using System.Web.Mvc;
using SportsLeague.Data.Repositories;
using SportsLeague.Model.Models;

namespace SportsLeague.UI.Models
{
    public class TeamPlayersVm
    {
        public Team NewTeam { get; set; }
        public Player NewPlayer { get; set; }
        public List<Position> PositionList { get; set; }
        public TeamPlayersVm()
        {
            var positionRepo = new PositionRepo();
            PositionList = positionRepo.GetAll();
        }
    }

}