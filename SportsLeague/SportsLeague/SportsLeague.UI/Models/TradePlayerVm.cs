﻿using System.Collections.Generic;
using System.Linq;
using SportsLeague.Model.Models;

namespace SportsLeague.UI.Models
{
    public class TradePlayerVm
    {
        public Team Team1 { get; set; }
        public Team Team2 { get; set; }
    }
}