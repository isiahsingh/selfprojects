﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SportsLeague.BLL.Managers;
using SportsLeague.Data.Repositories;
using SportsLeague.Model.Models;

namespace SportsLeague.UI.Models
{
    public class GamePlayerStatsVM
    {
        public Game Game { get; set; }
        public List<Player> HomePlayers { get; set; }
        public List<Player> AwayPlayers { get; set; }

        public GamePlayerStatsVM(int gameId)
        {
            var gameManager = new GameManager();
            var playerManager = new PlayerManager();
            Game = gameManager.GetGameWithGameStats(gameId).Data;
            HomePlayers = playerManager.GetAllPlayers().Data.Where(p => p.TeamId == Game.HomeTeamId).ToList();
            AwayPlayers = playerManager.GetAllPlayers().Data.Where(p => p.TeamId == Game.AwayTeamId).ToList();
        }

        public PlayerGameStat GetPlayerStat(Player player)
        {
            return player.PlayerGameStats.FirstOrDefault(s => s.GameId == Game.GameId);
        }
    }
}