﻿using System.Configuration;

namespace SportsLeague.Data.Settings
{
    public static class Config
    {
        private static string _connectionString;

        public static string ConnectionString
        {
            get
            {
                if (string.IsNullOrEmpty(_connectionString))
                {
                    _connectionString = ConfigurationManager.ConnectionStrings["BasketballLeague"].ConnectionString;
                }
                return _connectionString;
            }
        }
    }
}
