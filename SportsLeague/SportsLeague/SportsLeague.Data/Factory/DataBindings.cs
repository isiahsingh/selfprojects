﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Modules;
using SportsLeague.Data.Interfaces;
using SportsLeague.Data.Repositories;

namespace SportsLeague.Data.Factory
{
    public class DataBindings : NinjectModule
    {
        public override void Load()
        {
            Bind<IGameRepository>().To<GameRepo>();
            Bind<ILeagueRepository>().To<LeagueRepo>();
            Bind<IManagerRepository>().To<ManagerRepo>();
            Bind<IPlayerGameStatRepository>().To<PlayerGameStatRepo>();
            Bind<IPlayerRepository>().To<PlayerRepo>();
            Bind<IPositionRepository>().To<PositionRepo>();
            Bind<ITeamRepository>().To<TeamRepo>();

        }
    }
}
