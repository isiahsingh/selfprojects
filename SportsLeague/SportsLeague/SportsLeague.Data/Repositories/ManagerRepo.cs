﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using SportsLeague.Data.Interfaces;
using SportsLeague.Data.Settings;
using SportsLeague.Model.Models;

namespace SportsLeague.Data.Repositories
{
    public class ManagerRepo : IManagerRepository
    {
        public List<Manager> GetAllPlayers()
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                return cn.Query<Manager>("GetAllManagers", commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public Player GetBySpecificId(int id)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("ManagerID", id);
                return cn.Query<Player>("GetManagerByID", p, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
        }

        public void Remove(int id)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("ManagerID", id);
                cn.Execute("DeleteManager", p, commandType: CommandType.StoredProcedure);
            }
        }

        public void Update(Manager model)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("FirstName", model.FirstName);
                p.Add("LastName", model.LastName);
                p.Add("TeamID", model.TeamId);

                cn.Execute("UpdateManager", p, commandType: CommandType.StoredProcedure);
            }
        }

        public int Create(Manager model)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("ManagerID", DbType.Int32, direction: ParameterDirection.Output);
                p.Add("TeamID", model.TeamId);
                p.Add("FirstName", model.FirstName);
                p.Add("LastName", model.LastName);

                cn.Execute("CreateAManager", p, commandType: CommandType.StoredProcedure);

                return p.Get<int>("ManagerID");
            }
        }
    }
}
