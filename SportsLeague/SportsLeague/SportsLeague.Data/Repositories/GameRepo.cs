﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using SportsLeague.Data.Interfaces;
using SportsLeague.Data.Settings;
using SportsLeague.Model.Models;

namespace SportsLeague.Data.Repositories
{
    public class GameRepo : IGameRepository
    {
      
        public List<Game> GetAll()
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                return cn.Query<Game>("GetAllGames", commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public Game GetBySpecificId(int id)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("GameID", id);
                Game game = cn.Query<Game>("GetSpecificGame", p, commandType: CommandType.StoredProcedure).FirstOrDefault();
                return game;
            }
        }

        public void Remove(int id)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("GameID", id);
                cn.Execute("DeleteAGame", p, commandType: CommandType.StoredProcedure);
            }
        }

        public int Create(Game model)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("GameID", DbType.Int32, direction: ParameterDirection.Output);
                p.Add("GameDate", model.GameDate);
                p.Add("HomeTeamID", model.HomeTeamId);
                p.Add("AwayTeamID", model.AwayTeamId);
                p.Add("WinnerID", model.WinnerId);

                cn.Execute("CreateAGame", p, commandType: CommandType.StoredProcedure);

                return p.Get<int>("GameID");
            }
        }
    }
}
