﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using SportsLeague.Data.Interfaces;
using SportsLeague.Data.Settings;
using SportsLeague.Model.Models;

namespace SportsLeague.Data.Repositories
{
    public class PlayerGameStatRepo : IPlayerGameStatRepository
    {
        public List<PlayerGameStat> GetAll()
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                return cn.Query<PlayerGameStat>("GetAllPlayerGameStats", commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public PlayerGameStat GetBySpecificId(int id)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("StatID", id);
                return cn.Query<PlayerGameStat>("GetSpecificStat", p).FirstOrDefault();
            }
        }

        public void Remove(int id)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("PlayerGameStatID", id);
                cn.Execute("DeleteAPlayerGameStat", p, commandType: CommandType.StoredProcedure);
            }
        }

        public int Create(PlayerGameStat model)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("StatID", DbType.Int32, direction: ParameterDirection.Output);
                p.Add("GameID", model.GameId);
                p.Add("PlayerID", model.PlayerId);
                p.Add("FGA", model.FGA);
                p.Add("FGM", model.FGM);
                p.Add("FTA", model.FTA);
                p.Add("FTM", model.FTM);
                p.Add("Steals", model.Steals);
                p.Add("Blocks", model.Blocks);
                p.Add("Assist", model.Assist);
                p.Add("Rebounds", model.Rebounds);
                p.Add("Points", model.Points);
                p.Add("Minutes", model.Minutes);

                cn.Execute("CreatePlayerGameStat", p, commandType: CommandType.StoredProcedure);

                return p.Get<int>("StatID");
            }
        }

        public void Update(PlayerGameStat model)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("FGA", model.FGA);
                p.Add("FGM", model.FGM);
                p.Add("FTA", model.FTA);
                p.Add("FTM", model.FTM);
                p.Add("Steals", model.Steals);
                p.Add("Blocks", model.Blocks);
                p.Add("Assist", model.Assist);
                p.Add("Rebounds", model.Rebounds);
                p.Add("Points", model.Points);
                p.Add("Minutes", model.Minutes);

                cn.Execute("UpdatePlayerGameStat", p, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
