﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using SportsLeague.Data.Interfaces;
using SportsLeague.Data.Settings;
using SportsLeague.Model.Models;

namespace SportsLeague.Data.Repositories
{
    public class LeagueRepo : ILeagueRepository
    {
        public List<League> GetAll()
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                return cn.Query<League>("GetAllLeagues", commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public League GetBySpecificId(int id)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("LeagueID", id);
                return cn.Query<League>("GetSpecificLeague", p).FirstOrDefault();
            }
        }

        public void Remove(int id)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("LeagueToDeleteId", id);
                cn.Execute("DeleteLeague", p, commandType: CommandType.StoredProcedure);
            }
        }

        public void Update(League model)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("LeagueName", model.LeagueName);
                cn.Execute("UpdateLeague", p, commandType: CommandType.StoredProcedure);
            }
        }

        public int Create(League model)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("LeagueID", DbType.Int32, direction: ParameterDirection.Output);
                p.Add("LeagueName", model.LeagueName);
                cn.Execute("CreateLeague", p, commandType: CommandType.StoredProcedure);

                return p.Get<int>("GameID");
            }
        }
    }
}
