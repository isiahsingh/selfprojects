﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using SportsLeague.Data.Interfaces;
using SportsLeague.Data.Settings;
using SportsLeague.Model.Models;

namespace SportsLeague.Data.Repositories
{
    public class PlayerRepo : IPlayerRepository
    {

        public List<Player> GetAll()
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                return cn.Query<Player>("GetAllPlayers", commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public Player GetBySpecificId(int id)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("PlayerID", id);
                return cn.Query<Player>("GetPlayerByID", p, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
        } 

        public void Remove(int id)
        {
            Player player = GetBySpecificId(id);
            RemoveAllPlayerPositionsForPlayer(player);
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("PlayerID", id);
                cn.Execute("DeletePlayer", p, commandType: CommandType.StoredProcedure);
            }
        }

        public void Update(Player model)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("PlayerID", model.PlayerId);
                p.Add("TeamID", model.TeamId);
                p.Add("FirstName", model.FirstName);
                p.Add("LastName", model.LastName);
                p.Add("JerseyNumber", model.JerseyNumber);
                cn.Execute("UpdatePlayer", p, commandType: CommandType.StoredProcedure);

                RemoveAllPlayerPositionsForPlayer(model);
                AddPlayerPositions(model);
            }
        }

        public int Create(Player model)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("PlayerID", DbType.Int32, direction: ParameterDirection.Output);
                p.Add("TeamID", model.TeamId);
                p.Add("FirstName", model.FirstName);
                p.Add("LastName", model.LastName);
                p.Add("JerseyNumber", model.JerseyNumber);
                cn.Execute("CreateAPlayer", p, commandType: CommandType.StoredProcedure);
                
                int playerID = p.Get<int>("PlayerID");
                model.PlayerId = playerID;
                //Add List of positions to PlayerPositions
                AddPlayerPositions(model);
                return playerID;
            }
        }

        private void AddPlayerPositions(Player model)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                foreach (var position in model.Positions)
                {
                    var pp = new DynamicParameters();
                    pp.Add("PlayerID", model.PlayerId);
                    pp.Add("PositionID", position.PositionId);
                    cn.Execute("AddPlayerPositionsForNewPlayer", pp, commandType: CommandType.StoredProcedure);
                }
            }
        }

        private void RemoveAllPlayerPositionsForPlayer(Player model)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var pp = new DynamicParameters();
                pp.Add("PlayerID", model.PlayerId);
                cn.Execute("RemoveAllPlayerPositionsForPlayer", pp, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
