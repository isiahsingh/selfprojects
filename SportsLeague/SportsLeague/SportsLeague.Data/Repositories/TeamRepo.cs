﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using SportsLeague.Data.Interfaces;
using SportsLeague.Data.Settings;
using SportsLeague.Model.Models;

namespace SportsLeague.Data.Repositories
{
    public class TeamRepo : ITeamRepository
    {
        public List<Team> GetAll()
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                return cn.Query<Team>("GetAllTeams", commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public Team GetBySpecificId(int id)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("TeamID", id);
                return cn.Query<Team>("GetTeamById", p, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
        }

        public int Create(Team model)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("TeamID", DbType.Int32, direction: ParameterDirection.Output);
                p.Add("TeamName", model.TeamName);
                p.Add("LeagueID", model.LeagueId);
                cn.Execute("CreateTeam", p, commandType: CommandType.StoredProcedure);

                return p.Get<int>("TeamID");
            }
        }

        public void Update(Team model)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("TeamName", model.TeamName);
                p.Add("LeagueId", model.LeagueId);
                cn.Execute("UpdateTeam", p, commandType: CommandType.StoredProcedure);
            }
        }

        public void Remove(int id)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("TeamID", id);
                cn.Execute("DeleteTeam", p, commandType: CommandType.StoredProcedure);
            }
        }


    }
}
