﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using SportsLeague.Data.Interfaces;
using SportsLeague.Data.Settings;
using SportsLeague.Model.Models;

namespace SportsLeague.Data.Repositories
{
    public class PositionRepo : IPositionRepository
    {
        public List<Position> GetAll()
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                return cn.Query<Position>("GetAllPositions", commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public List<Position> GetAllPositionsForPlayer(int id)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("PlayerID", id);
                return cn.Query<Position>("GetPositionsForPlayer", p, commandType: CommandType.StoredProcedure).ToList();
            }
        } 
    }
}
