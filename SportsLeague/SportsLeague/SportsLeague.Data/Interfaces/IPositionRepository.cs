﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SportsLeague.Model.Models;

namespace SportsLeague.Data.Interfaces
{
    public interface IPositionRepository
    {
        List<Position> GetAll();
        List<Position> GetAllPositionsForPlayer(int id);
    }
}
