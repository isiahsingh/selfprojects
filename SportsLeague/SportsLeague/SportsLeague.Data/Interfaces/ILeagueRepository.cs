﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SportsLeague.Model.Models;

namespace SportsLeague.Data.Interfaces
{
    public interface ILeagueRepository
    {
        List<League> GetAll();
        League GetBySpecificId(int id);
        void Remove(int id);
        void Update(League model);
        int Create(League model);
    }
}
