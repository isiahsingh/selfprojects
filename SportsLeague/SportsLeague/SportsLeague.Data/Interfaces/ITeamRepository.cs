﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SportsLeague.Model.Models;

namespace SportsLeague.Data.Interfaces
{
    public interface ITeamRepository
    {
        List<Team> GetAll();
        Team GetBySpecificId(int id);
        int Create(Team model);
        void Update(Team model);
        void Remove(int id);
    }
}
