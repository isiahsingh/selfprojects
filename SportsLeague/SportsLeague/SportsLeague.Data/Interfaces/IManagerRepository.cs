﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SportsLeague.Model.Models;

namespace SportsLeague.Data.Interfaces
{
    public interface IManagerRepository
    {
        List<Manager> GetAllPlayers();
        Player GetBySpecificId(int id);
        void Remove(int id);
        void Update(Manager model);
        int Create(Manager model);
    }
}
