﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SportsLeague.Model.Models;

namespace SportsLeague.Data.Interfaces
{
    public interface IPlayerGameStatRepository
    {
        List<PlayerGameStat> GetAll();
        PlayerGameStat GetBySpecificId(int id);
        void Remove(int id);
        void Update(PlayerGameStat model);
        int Create(PlayerGameStat model);
    }
}
