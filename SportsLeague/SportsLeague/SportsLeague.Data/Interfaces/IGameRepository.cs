﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SportsLeague.Model.Models;

namespace SportsLeague.Data.Interfaces
{
    public interface IGameRepository
    {
        List<Game> GetAll();
        Game GetBySpecificId(int id);
        void Remove(int id);
        int Create(Game model);
    }
}
