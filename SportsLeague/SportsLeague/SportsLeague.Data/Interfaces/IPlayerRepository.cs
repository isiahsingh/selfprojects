﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SportsLeague.Model.Models;

namespace SportsLeague.Data.Interfaces
{
    public interface IPlayerRepository
    {
        List<Player> GetAll();
        Player GetBySpecificId(int id);
        void Remove(int id);
        void Update(Player model);
        int Create(Player model);
    }
}
