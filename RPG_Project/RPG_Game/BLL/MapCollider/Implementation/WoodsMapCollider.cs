﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Characters.Humans;
using UI.Enums;

namespace BLL.MapCollider.Implementation
{
    public class WoodsMapCollider : ICollider
    {
        public MapTypes[,] enumMap { get; set; }

        public WoodsMapCollider()
        {
            enumMap = new MapTypes[20, 16];
            using (StreamReader sr = File.OpenText(@"..\..\..\Data\EnumMaps\WoodsMap.txt"))
            {
                string input = null;
                int x = 0;
                while ((input = sr.ReadLine()) != null)
                {
                    for (int y = 0; y < input.Length; y++)
                    {
                        int num = int.Parse(input[y].ToString());
                        enumMap[x, y] = (MapTypes)num;
                    }
                    x++;
                }
            }






            //enumMap = new MapTypes[20, 16];

            //for (int i = 0; i < 20; i++)
            //{
            //    for (int j = 0; j < 16; j++)
            //    {
            //        if (i == player.characterPosition[0] && j == player.characterPosition[1])
            //        {
            //            enumMap[i, j] = MapTypes.Player;
            //        }
            //        else if (i == 0 || j == 0 || i == 19 || j == 15)
            //        {
            //            enumMap[i, j] = MapTypes.Wall;
            //        }
            //        else if (j == 1 || j == 2 || i == 1 || ((i > 12 && i < 19) && (j > 0 && j < 8)) ||
            //            ((i > 12 && i < 19) && (j > 8 && j < 11)) || ((i == 17) && (j > 10 && j < 13)) ||
            //            ((i > 7 && i < 11) && (j > 6 && j < 13)) || ((i == 10) && (j > 8 && j < 11)))
            //        {
            //            enumMap[i, j] = MapTypes.Tree;
            //        }
            //        else
            //        {
            //            enumMap[i, j] = MapTypes.Empty;
            //        }
            //    }
            //}
        }


        public void UpdateMapCollider(Player player)
        {
            for (int i = 0; i < 20; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    if (i == player.previousPosition[0] && j == player.previousPosition[1])
                    {
                        enumMap[i, j] = MapTypes.Empty;
                    }
                    if (i == player.characterPosition[0] && j == player.characterPosition[1])
                    {
                        enumMap[i, j] = MapTypes.Player;
                    }
                }
            }




            //for (int i = 0; i < 20; i++)
            //{
            //    for (int j = 0; j < 16; j++)
            //    {
            //        if (i == player.characterPosition[0] && j == player.characterPosition[1])
            //        {
            //            enumMap[i, j] = MapTypes.Player;
            //        }
            //        else if (i == 0 || j == 0 || i == 19 || j == 15)
            //        {
            //            enumMap[i, j] = MapTypes.Wall;
            //        }
            //        else if (j == 1 || j == 2 || i == 1 || ((i > 12 && i < 19) && (j > 0 && j < 8)))
            //        {
            //            enumMap[i, j] = MapTypes.Tree;
            //        }
            //        else
            //        {
            //            enumMap[i, j] = MapTypes.Empty;
            //        }
            //    }
            //}
        }
    }
}
