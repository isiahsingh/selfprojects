﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Models.Characters.Humans;
using UI.Enums;

namespace BLL.MapCollider.Implementation
{
    public class OutsideMapCollider : ICollider
    {
        public MapTypes[,] enumMap { get; set; }

        public OutsideMapCollider()
        {
            enumMap = new MapTypes[20, 16];
            using (StreamReader sr = File.OpenText(@"..\..\..\Data\EnumMaps\OutsideMap.txt"))
            {
                string input = null;
                int x = 0;
                while ((input = sr.ReadLine()) != null)
                {
                    for (int y = 0; y < input.Length; y++)
                    {
                        int num = int.Parse(input[y].ToString());
                        enumMap[x, y] = (MapTypes)num;
                    }
                    x++;
                }
            }
        }

        public void UpdateMapCollider(Player player)
        {
            for (int i = 0; i < 20; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    if (i == player.previousPosition[0] && j == player.previousPosition[1])
                    {
                        enumMap[i, j] = MapTypes.Empty;
                    }
                    if ((i == player.characterPosition[0] && j == player.characterPosition[1]))
                    {
                        enumMap[i, j] = MapTypes.Player;
                    }
                }
            }
        }
    }
}
