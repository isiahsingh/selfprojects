﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Characters.Humans;
using UI.Enums;

namespace BLL.MapCollider
{
    public interface ICollider
    {
        MapTypes[,] enumMap { get; set; }
        void UpdateMapCollider(Player player);
    }
}
