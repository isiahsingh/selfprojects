﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI.Enums
{
    public enum MoveTypes
    {
        Woods,
        Water,
        Exit,
        Valid,
        Invalid
    }
}
