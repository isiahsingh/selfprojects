﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI.Enums
{
    public enum MapTypes
    {
        Empty,
        Wall,
        Tree,
        Water,
        Player
    }
}
