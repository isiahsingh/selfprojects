﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.MapCollider;
using Models.Characters.Humans;
using UI.Enums;

namespace BLL.MoveUser.Base
{
    public abstract class PlayerMovement
    {
        public MoveTypes movePlayer(ConsoleKey key, Player player, ICollider mapCollider, string[] possiblePositions)
        {
            int[] newPosition = new int[2];
            MoveTypes NextMovePath = new MoveTypes();

            switch (key)
            {
                case ConsoleKey.LeftArrow:
                    newPosition[0] = player.characterPosition[0];
                    newPosition[1] = player.characterPosition[1] - 1;
                    player.creatureCharacter = possiblePositions[0];
                    NextMovePath = ObjectInTheWay(newPosition, mapCollider, player);
                    break;
                case ConsoleKey.DownArrow:
                    newPosition[0] = player.characterPosition[0] + 1;
                    newPosition[1] = player.characterPosition[1];
                    player.creatureCharacter = possiblePositions[2];
                    NextMovePath = ObjectInTheWay(newPosition, mapCollider, player);
                    break;
                case ConsoleKey.RightArrow:
                    newPosition[0] = player.characterPosition[0];
                    newPosition[1] = player.characterPosition[1] + 1;
                    player.creatureCharacter = possiblePositions[3];
                    NextMovePath = ObjectInTheWay(newPosition, mapCollider, player);
                    break;
                case ConsoleKey.UpArrow:
                    newPosition[0] = player.characterPosition[0] - 1;
                    newPosition[1] = player.characterPosition[1];
                    player.creatureCharacter = possiblePositions[1];
                    NextMovePath = ObjectInTheWay(newPosition, mapCollider, player);
                    break;
                default:
                    Console.WriteLine("You entered an invalid key. Please use the arrow keys to move.");
                    NextMovePath = MoveTypes.Invalid;
                    break;
            }
            return NextMovePath;
        }

        public abstract MoveTypes ObjectInTheWay(int[] newPosition, ICollider mapCollider, Player player);
    }
}
