﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.MapCollider;
using Models.Characters.Humans;
using UI.Enums;

namespace BLL.MoveUser.Interface
{
    public interface IMover
    {
        MoveTypes movePlayer(ConsoleKey key, Player player, ICollider mapCollider, string[] possiblePositions);
        MoveTypes ObjectInTheWay(int[] newPosition, ICollider mapCollider, Player player);
    }
}
