﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.MapCollider;
using BLL.MoveUser.Base;
using BLL.MoveUser.Interface;
using Models.Characters.Humans;
using UI.Enums;

namespace BLL.MoveUser.Implementation
{
    public class WoodsMover : PlayerMovement, IMover
    {
        public override MoveTypes ObjectInTheWay(int[] newPosition, ICollider mapCollider, Player player)
        {
            if (newPosition[0] == 19 && newPosition[1] == 8)
            {
                return MoveTypes.Exit;
            }
            else if (newPosition[0] > 19 || newPosition[1] > 15 || newPosition[0] < 0 || newPosition[1] < 0)
            {
                return MoveTypes.Invalid;
            }

            if (mapCollider.enumMap[newPosition[0], newPosition[1]] == MapTypes.Tree)
            {
                return MoveTypes.Woods;
            }
            else if (mapCollider.enumMap[newPosition[0], newPosition[1]] == MapTypes.Empty)
            {
                player.previousPosition = player.characterPosition;
                player.characterPosition = newPosition;
                return MoveTypes.Valid;
            }
            else
            {
                return MoveTypes.Invalid;
            }

        }
    }
}
