﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.MapCollider;
using BLL.MapCollider.Implementation;
using BLL.MoveUser.Implementation;
using BLL.MoveUser.Interface;
using Models.Characters.Humans;
using UI.Enums;
using UI.Maps;
using UI.MoveResponse.Implementation;
using UI.MoveResponse.Interface;

namespace UI.Workflow
{

    public class StartGame
    {
        // private IMap _gameMap;
        private ICollider _mapCollider;
        private IMover _move;
        private IMoveDisplayer _dispInput;
        int[] _changeMapPlayerPosition = new int[2];
        private Player player;

        //Must Keep this order -- Left, Up, Down, Right
        public static string[] playerPositions = new[] { "  ◄ ", "  ▲ ", "  ▼ ", "  ► " };


        public StartGame(string playerName)
        {
            player = new Player(playerName) {creatureCharacter = "  ▲ "};
            _mapCollider = new OutsideMapCollider();
            _move = new OutsideMover();
            _dispInput = new OutsideMoveDisplayer();
        }

        public void Start()
        {
            GameResults results = GameResults.Invalid;
            do
            {
                ConsoleKeyInfo key;
                do
                {
                    MapEngine.DisplayMap(_mapCollider, player);
                    MoveTypes nextSpot = new MoveTypes();
                    Console.WriteLine("Use arrow keys to move your player.");
                    key = Console.ReadKey();
                    nextSpot = _move.movePlayer(key.Key, player, _mapCollider, playerPositions);
                    string enterResponse = _dispInput.NaturalObjectFinder(nextSpot, player, _mapCollider);
                    checkChangeMap(enterResponse, player);
                } while (key.Key != ConsoleKey.UpArrow || key.Key != ConsoleKey.DownArrow ||
                         key.Key != ConsoleKey.RightArrow || key.Key != ConsoleKey.LeftArrow);
            } while (results != GameResults.Dead || results != GameResults.Win);
        }

        public void checkChangeMap(string enterResponse, Player player)
        {
            switch (enterResponse)
            {
                case "yesTree":
                    _changeMapPlayerPosition = player.characterPosition;
                    player.characterPosition = new[] {19, 8};
                    _mapCollider = new WoodsMapCollider();
                    _move = new WoodsMover();
                    _dispInput = new WoodMoveDisplayer();
                    break;
                case "yesLeaveWoods":
                    player.characterPosition = _changeMapPlayerPosition;
                    _mapCollider = new OutsideMapCollider();
                    _move = new OutsideMover();
                    _dispInput = new OutsideMoveDisplayer();
                    break;
                case "noLeaveWoods":
                    break;
                case "yesCutTree":
                    //Check and subtract 8 points
                    break;
                case "noTree":
                    break;
                case "yesWater":

                    break;
                case "noWater":

                    break;
                case "Invalid":
                    //Console.WriteLine("Sorry, you can not move there. Press ENTER to continue.");
                    Console.ReadLine();
                    break;
                default:
                    break;
            }
        }

    }
}
