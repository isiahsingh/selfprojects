﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.MapCollider;
using Models.Characters.Humans;
using UI.Enums;

namespace UI.Maps
{
    public static class MapEngine
    {
        public static void DisplayMap(ICollider mapCollider, Player player)
        {
            Console.Clear();
            for (int x = 0; x < 20; x++)
            {
                for (int y = 0; y < 16; y++)
                {
                    if (mapCollider.enumMap[x, y] == MapTypes.Wall)
                    {
                        Console.Write("████");
                    }
                    else if (mapCollider.enumMap[x, y] == MapTypes.Tree)
                    {
                        Console.Write("↕↕↕↕");
                    }
                    else if (mapCollider.enumMap[x, y] == MapTypes.Water)
                    {
                        Console.Write("≈≈≈≈");
                    }
                    else if (mapCollider.enumMap[x, y] == MapTypes.Empty)
                    {
                        Console.Write("    ");
                    }
                    else if (mapCollider.enumMap[x, y] == MapTypes.Player)
                    {
                        Console.Write(player.creatureCharacter);
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
