﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.MapCollider;
using Models.Characters.Humans;
using UI.Enums;

namespace UI.MoveResponse.Interface
{
    public interface IMoveDisplayer
    {
        string NaturalObjectFinder(MoveTypes nextSpot, Player player, ICollider mapCollider);
    }
}
