﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.MapCollider;
using Models.Characters.Humans;
using UI.Enums;
using UI.Maps;
using UI.MoveResponse.Interface;

namespace UI.MoveResponse.Implementation
{
    public class OutsideMoveDisplayer : IMoveDisplayer
    {

        public string NaturalObjectFinder(MoveTypes nextSpot, Player player, ICollider mapCollider)
        {
            string response;
            if (nextSpot == MoveTypes.Woods)
            {
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("You are at the woods. Do you want to enter? (y/n)");
                response = Console.ReadLine();
                if (response == "y")
                    response = "yesTree";
                else
                    response = "noTree";
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            else if (nextSpot == MoveTypes.Water)
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("You are at a body of water. Do you want to go in? (y/n)");
                response = Console.ReadLine();
                if (response == "y")
                    response = "yesWater";
                else
                    response = "noWater";
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            else if (nextSpot == MoveTypes.Invalid)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("There is something in the way. You can't move there.");
                Console.ForegroundColor = ConsoleColor.Gray;
                response = "Invalid";
            }
            else if (nextSpot == MoveTypes.Valid)
            {
                mapCollider.UpdateMapCollider(player);
                MapEngine.DisplayMap(mapCollider, player);
                response = "";
            }
            else
            {
                response = "";
            }

            return response;
        }
    }
}
