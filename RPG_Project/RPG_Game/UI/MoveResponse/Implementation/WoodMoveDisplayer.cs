﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.MapCollider;
using Models.Characters.Humans;
using UI.Enums;
using UI.Maps;
using UI.MoveResponse.Interface;

namespace UI.MoveResponse.Implementation
{
    public class WoodMoveDisplayer : IMoveDisplayer
    {
       
        public string NaturalObjectFinder(MoveTypes nextSpot, Player player, ICollider mapCollider)
        {
            string response;
            if (nextSpot == MoveTypes.Woods)
            {
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("Would you like to cut down this tree? It costs 8 points. (y/n)");
                response = Console.ReadLine();
                if (response == "y")
                    response = "yesCutTree";
                else
                    response = "noTree";
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            else if (nextSpot == MoveTypes.Exit)
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("Do you want to leave the woods? (y/n)");
                response = Console.ReadLine();
                if (response == "y")
                    response = "yesLeaveWoods";
                else
                    response = "noLeaveWoods";
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            else if (nextSpot == MoveTypes.Invalid)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("You can not move off the board.");
                response = "Invalid";
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            else if (nextSpot == MoveTypes.Valid)
            {
                mapCollider.UpdateMapCollider(player);
                MapEngine.DisplayMap(mapCollider, player);
                response = "";
            }
            else
            {
                response = "";
            }

            return response;
        }
    }
}
