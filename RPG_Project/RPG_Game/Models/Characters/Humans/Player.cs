﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Characters.Base;
using Models.Inventory.Base;
using Models.Weapons;

namespace Models.Characters.Humans
{
    public class Player : Human
    {
        public Item ItemInHand { get; set; }

        public Player(string name)
        {
            Name = name;
            Description = "This is your player.";
            Weight = 150;
            ItemInHand = new Stick();
            Money = 500;
            Wood = 30;
            Points = 100;
            AbilityPoints = 40;
            characterPosition= new int[] {19, 8};
        }
    }
}
