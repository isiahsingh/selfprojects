﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Characters.CharacterEnum
{
    public enum CharacterType
    {
        Human,
        Monster,
        Animals,
        Unknown
    }
}
