﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Inventory.ItemEnum;

namespace Models.Inventory.Base
{
    public abstract class Item
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Weight { get; set; }
        public int Value { get; set; }
        public ItemType Type { get; set; }


        public Item()
        {
            Type = ItemType.Unknown;
        }
    }
}
