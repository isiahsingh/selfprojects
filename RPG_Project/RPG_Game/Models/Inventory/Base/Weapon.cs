﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Inventory.Base
{
    public class Weapon : Item
    {
        private int _damage;
        public int UsagePoints { get; set; }

        public int Damage
        {
            get { return _damage; }
            set
            {
                if (value > 0)
                {
                    _damage = value;
                }
            }
        }
    }
}
