﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Inventory.ItemEnum
{
    public enum ItemType
    {
        Unknown,
        Reagent,
        Weapon,
        Container
    }
}
