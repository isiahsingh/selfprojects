﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Inventory.Base;
using Models.Inventory.ItemEnum;

namespace Models.Weapons
{
    public class Knife : Weapon
    {
        public Knife()
        {
            Name = "Knife";
            Description = "Kitchen knife. Cut shit up!";
            Weight = 5;
            Value = 50;
            Type = ItemType.Weapon;
            Damage = 15;
            UsagePoints = 6;
        }
    }
}
