﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Inventory.Base;
using Models.Inventory.ItemEnum;

namespace Models.Weapons
{
    public class BattleAxe : Weapon
    {
        public BattleAxe()
        {
            Name = "Giant Steel Battleaxe";
            Description = "For the glory of Krong!;";
            Weight = 50;
            Value = 250;
            Type = ItemType.Weapon;
            Damage = 45;
            UsagePoints = 20;
        }
    }
}
