﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Inventory.Base;
using Models.Inventory.ItemEnum;

namespace Models.Weapons
{
    public class Stick : Weapon
    {
        public Stick()
        {
            Name = "A wooden boomerang";
            Description = "I found this in level 1";
            Weight = 1;
            Value = 5;
            Type = ItemType.Weapon;
            Damage = 5;
            UsagePoints = 3;
        }
    }
}
