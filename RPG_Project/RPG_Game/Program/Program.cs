﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UI;
using UI.Workflow;

namespace Program
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu menu = new Menu();
            string playerName = menu.StartMenu();
            StartGame game = new StartGame(playerName);
            game.Start();
        }
    }
}
