﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using RPG.Inventory.Containers;
using RPG.Inventory.Reagents;
using RPG.Inventory.Weapons;

namespace RPG.Inventory.Tests
{
    [TestFixture]
    public class BackpackTests
    {
        [Test]
        public void PutItemInBackpackTest()
        {
            // Arrange
            Backpack bag = new Backpack();
            BattleAxe axe = new BattleAxe();
            Mushroom shroom = new Mushroom();

            // Act
            bag.AddItem(axe);
            bag.AddItem(shroom);

            //Assert
            Assert.AreEqual(2, bag.ItemCount);

            bag.DisplayContents();
        }
    }
}
