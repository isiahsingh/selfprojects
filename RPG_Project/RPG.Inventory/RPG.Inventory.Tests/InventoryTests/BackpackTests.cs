﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using RPG.Inventory.Base;
using RPG.Inventory.Containers;
using RPG.Inventory.Reagents;
using RPG.Inventory.Weapons;

namespace RPG.Inventory.Tests
{
    [TestFixture]
    public class BackpackTests
    {
        [Test]
        public void PutItemInBackPackTest()
        {
            //Arrange 
            Backpack bag = new Backpack();
            BattleAxe axe = new BattleAxe();
            Mushroom mush = new Mushroom();

            //Act
            bag.AddItem(axe);
            bag.AddItem(mush);

            //Assert
            Assert.AreEqual(2, bag.ItemCount);

            bag.DisplayContents();
        }

    }
}
