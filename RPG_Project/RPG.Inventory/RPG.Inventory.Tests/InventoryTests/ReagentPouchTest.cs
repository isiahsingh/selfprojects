﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using RPG.Inventory.Containers;
using RPG.Inventory.Reagents;
using RPG.Inventory.Weapons;

namespace RPG.Inventory.Tests
{
    [TestFixture]
    public class ReagentPouchTest
    {
        [Test]
        public void CanNotAddNonReagent()
        {
            ReagentPouch pouch = new ReagentPouch();
            BattleAxe axe = new BattleAxe();
            
            //Act 
            pouch.AddItem(axe);

            //Assert
            Assert.AreEqual(0, pouch.ItemCount);
        }

        [Test]
        public void CanAddReagent()
        {
            ReagentPouch pouch = new ReagentPouch();
            Mushroom mush = new Mushroom();

            //Act
            pouch.AddItem(mush);

            //Assert
            Assert.AreEqual(1, pouch.ItemCount);
        }
    }
}
