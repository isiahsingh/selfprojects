﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using RPG.Inventory.Containers;
using RPG.Inventory.Weapons;

namespace RPG.Inventory.Tests
{
    [TestFixture]
    public class SatchelTest
    {
        [Test]
        public void CannotAddBigItem()
        {
            //Arrange
            Satchel bag = new Satchel();
            BattleAxe axe = new BattleAxe();

            //Act
            bag.AddItem(axe);

            //Assert
            Assert.AreEqual(0, bag.ItemCount);
        }

        [Test]
        public void CanAddLittleItem()
        {
            //Arrange
            Satchel bag = new Satchel();

            //Stick stick = new Stick();

            ////Act
            //bag.AddItem(stick);

            //Assert
            Assert.AreEqual(1, bag.ItemCount);
        }


    }
}
