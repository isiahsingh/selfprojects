﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Characters.Base;

namespace RPG.Characters.Monsters
{
    class MinhMonster : Monster
    {
        public MinhMonster()
        {
            Name = "Monster Minh";
            Description = "Level 1 Monster";
            Health = 70;
            Level = 1;
            PointReward = 10;
            MoneyReward = 75;
        }
    }
}
