﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;
using RPG.Inventory.Containers;

namespace RPG.Characters.Base
{
    public class Human : Creatures
    {
        private int _armor;
        Backpack bag = new Backpack();
        public int Armor { get; set; }
        public int Money { get; set; }
        public int Points { get; set; }
        public int Wood { get; set; }
        public int AbilityPoints { get; set; }

        public Human()
        {
            CharType = CharacterType.Human;
            Armor = 100;
        }
        
        public virtual void Attack(Creatures creature, Weapon weapon)
        {
            creature.Health = creature.Health - weapon.Damage;
        }

        public virtual void LevelUp(int Points)
        {
            switch (Points)
            {
                case 20:
                    Level = 2;
                    break;
                case 50:
                    Level = 3;
                    break;
                case 80:
                    Level = 4;
                    break;
                case 110:
                    Level = 5;
                    break;
                default:
                    break;
            }
        }
    }
}
