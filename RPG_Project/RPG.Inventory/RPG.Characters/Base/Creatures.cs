﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Characters.Base
{
    public abstract class Creatures
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Health { get; set; }
        public int Weight { get; set; }
        public int Level { get; set; }
        public CharacterType CharType { get; set; }

        protected Creatures()
        {
            Health = 100;
            CharType = CharacterType.Unknown;
            Level = 1;
        }

    }

   
}
