﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Characters.Base;
using RPG.Inventory.Base;
using RPG.Inventory.Weapons;

namespace RPG.Characters.Humans
{
    public class Player : Human
    {
        public Item ItemInHand { get; set; }
        public int[] playerPosition { get; set; }
        public string playerCharacter { get; set; }
        public int[] previousPosition { get; set; }

        public Player(string name)
        {
            Name = name;
            Description = "This is your player.";
            Weight = 150;
            ItemInHand = new Stick();
            Money = 500;
            Wood = 30;
            Points = 100;
            AbilityPoints = 40;
            playerPosition = new int[] {19,8};

        }



    }
}
