﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Characters.Base;

namespace RPG.Characters.Humans
{
    public class Killer : Human
    {
        public Killer()
        {
            Name = "El Chapo";
            Description = "This guy wants to kill you.";
            Health = 100;
            Weight = 150;
            Points = 300;
            Money = 150;
            AbilityPoints = 50;
            CharType = CharacterType.Human;
        }
    }
}
