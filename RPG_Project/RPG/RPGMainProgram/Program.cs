﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGMainProgram.UI;

namespace RPGMainProgram
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu menu = new Menu();
            string playerName = menu.StartMenu();
            GameStarter game = new GameStarter(playerName);
            game.StartGame();

        }
    }
}
