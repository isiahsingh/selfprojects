﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGMainProgram.BLL
{
    public enum MapType
    {
        Empty, 
        Wall,
        Tree,
        Water,
        Player
    }
}
