﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Characters.Humans;

namespace RPGMainProgram.BLL.MapCollider
{
    public interface ICollider
    {
        MapType[,] enumMap { get; set; }
        void UpdateMapCollider(Player player);
    }
}
