﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Characters.Humans;

namespace RPGMainProgram.BLL.MapCollider.Implementation
{
    public class WoodsMapCollider : ICollider
    {
        public MapType[,] enumMap { get; set; }

        public WoodsMapCollider(Player player)
        {
            enumMap = new MapType[20, 16];

            for (int i = 0; i < 20; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    if (i == player.playerPosition[0] && j == player.playerPosition[1])
                    {
                        enumMap[i, j] = MapType.Player;
                    }
                    else if (i == 0 || j == 0 || i == 19 || j == 15)
                    {
                        enumMap[i, j] = MapType.Wall;
                    }
                    else if (j == 1 || j == 2 || i == 1 || ((i > 12 && i < 19) && (j > 0 && j < 8)) ||
                        ((i > 12 && i < 19) && (j > 8 && j < 11)) || ((i ==17) && (j > 10 && j < 13)) ||
                        ((i > 7 && i < 11) && (j > 6 && j < 13)) || ((i ==10) && (j > 8 && j < 11)))
                    {
                        enumMap[i, j] = MapType.Tree;
                    }
                    else
                    {
                        enumMap[i, j] = MapType.Empty;
                    }
                }
            }
        }


        public void UpdateMapCollider(Player player)
        {
            for (int i = 0; i < 20; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    if (i == player.playerPosition[0] && j == player.playerPosition[1])
                    {
                        enumMap[i, j] = MapType.Player;
                    }
                    else if (i == 0 || j == 0 || i == 19 || j == 15)
                    {
                        enumMap[i, j] = MapType.Wall;
                    }
                    else if (j == 1 || j == 2 || i == 1 || ((i > 12 && i < 19) && (j > 0 && j < 8)))
                    {
                        enumMap[i, j] = MapType.Tree;
                    }
                    else
                    {
                        enumMap[i, j] = MapType.Empty;
                    }
                }
            }
        }
    }
}
