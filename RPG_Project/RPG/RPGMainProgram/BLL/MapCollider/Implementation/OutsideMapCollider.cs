﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Characters.Humans;
using RPGMainProgram.BLL.MapCollider;

namespace RPGMainProgram.BLL
{
    public class OutsideMapCollider : ICollider
    {

        public MapType[,] enumMap {get;set;}

        public OutsideMapCollider(Player player)
        {
            //using (StreamReader sr = File.OpenText("Outside.txt"))
            //{
            //    string input = null;
            //    while ((input = sr.ReadLine()) != null)
            //    {

            //    }

            //}

            enumMap = new MapType[20, 16];
            for (int i = 0; i < 20; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    if (i == player.playerPosition[0] && j == player.playerPosition[1])
                    {
                        enumMap[i, j] = MapType.Player;
                    }
                    else if (i == 0 || j == 0 || i == 19 || j == 15 || (i == 1 && j == 13) || (i == 2 && j == 13) ||
                        (i == 4 && j == 13) || (i == 4 && j == 14))
                    {
                        enumMap[i, j] = MapType.Wall;
                    }
                    else if ((i > 14 && i < 19) && (j > 0 && j < 4))
                    {
                        enumMap[i, j] = MapType.Tree;
                    }
                    else if (((i > 7 && i < 11) && (j > 5 && j < 11)) || ((i > 7 && i < 11) && (j > 11 && j < 15)))
                    {
                        enumMap[i, j] = MapType.Water;
                    }
                    else
                    {
                        enumMap[i, j] = MapType.Empty;
                    }
                }
            }
        }

        public void UpdateMapCollider(Player player)
        {
            for (int i = 0; i < 20; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    if (i == player.playerPosition[0] && j == player.playerPosition[1])
                    {
                        enumMap[i, j] = MapType.Player;
                    }
                    else if (i == 0 || j == 0 || i == 19 || j == 15 || (i == 1 && j == 13) || (i == 2 && j == 13) ||
                        (i == 4 && j == 13) || (i == 4 && j == 14))
                    {
                        enumMap[i, j] = MapType.Wall;
                    }
                    else if ((i > 14 && i < 19) && (j > 0 && j < 4))
                    {
                        enumMap[i, j] = MapType.Tree;
                    }
                    else if (((i > 7 && i < 11) && (j > 5 && j < 11)) || ((i > 7 && i < 11) && (j > 11 && j < 15)))
                    {
                        enumMap[i, j] = MapType.Water;
                    }
                    else
                    {
                        enumMap[i, j] = MapType.Empty;
                    }
                }
            }
        }
    }
}
