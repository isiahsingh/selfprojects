﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Characters.Humans;
using RPGMainProgram.BLL.MapCollider;

namespace RPGMainProgram.BLL.MoveUser.Base
{
    public abstract class PlayerMovement
    {
        public ValidMoveType movePlayer(ConsoleKey key, Player player, ICollider mapCollider, string[] possiblePositions)
        {
            int[] newPosition = new int[2];
            ValidMoveType NextMovePath = new ValidMoveType();

            switch (key)
            {
                case ConsoleKey.LeftArrow:
                    newPosition[0] = player.playerPosition[0];
                    newPosition[1] = player.playerPosition[1] - 1;
                    player.playerCharacter = possiblePositions[0];
                    NextMovePath = ObjectInTheWay(newPosition, mapCollider, player);
                    break;
                case ConsoleKey.DownArrow:
                    newPosition[0] = player.playerPosition[0] + 1;
                    newPosition[1] = player.playerPosition[1];
                    player.playerCharacter = possiblePositions[2];
                    NextMovePath = ObjectInTheWay(newPosition, mapCollider, player);
                    break;
                case ConsoleKey.RightArrow:
                    newPosition[0] = player.playerPosition[0];
                    newPosition[1] = player.playerPosition[1] + 1;
                    player.playerCharacter = possiblePositions[3];
                    NextMovePath = ObjectInTheWay(newPosition, mapCollider, player);
                    break;
                case ConsoleKey.UpArrow:
                    newPosition[0] = player.playerPosition[0] - 1;
                    newPosition[1] = player.playerPosition[1];
                    player.playerCharacter = possiblePositions[1];
                    NextMovePath = ObjectInTheWay(newPosition, mapCollider, player);
                    break;
                default:
                    Console.WriteLine("You entered an invalid key. Please use the arrow keys to move.");
                    NextMovePath = ValidMoveType.Invalid;
                    break;
            }
            return NextMovePath;
        }

        public abstract ValidMoveType ObjectInTheWay(int[] newPosition, ICollider mapCollider, Player player);
    }
}
