﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Characters.Humans;
using RPGMainProgram.BLL.MapCollider;
using RPGMainProgram.BLL.MoveUser;
using RPGMainProgram.BLL.MoveUser.Base;
using RPGMainProgram.UI.Maps.Interface;
using RPGMainProgram.UserResponse.Implementation;
using RPGMainProgram.UserResponse.Interface;

namespace RPGMainProgram.BLL
{
    public class UserMover : PlayerMovement, IMover 
    {

        public override ValidMoveType ObjectInTheWay(int[] newPosition, ICollider mapCollider, Player player)
        {
            IInputDisplayer displayResponse = new OutsideInputDisplayer();

            if (newPosition[0] > 19 || newPosition[1] > 15 || newPosition[0] < 0 || newPosition[1] < 0)
            {
                displayResponse.MoveResponse(ValidMoveType.Invalid);
                return ValidMoveType.Invalid;
            }

            if (mapCollider.enumMap[newPosition[0], newPosition[1]] == MapType.Tree)
            {
                return ValidMoveType.Woods;
            }
            else if (mapCollider.enumMap[newPosition[0], newPosition[1]] == MapType.Water)
            {
                return ValidMoveType.Water;
            }
            else if (mapCollider.enumMap[newPosition[0], newPosition[1]] == MapType.Empty)
            {
                player.previousPosition = player.playerPosition;
                player.playerPosition = newPosition;
                return ValidMoveType.Valid;
            }
            else
            {
                displayResponse.MoveResponse(ValidMoveType.Invalid);
                return ValidMoveType.Invalid;
            }

        }
    }
}
