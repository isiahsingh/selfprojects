﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Characters.Humans;
using RPGMainProgram.BLL.MapCollider;
using RPGMainProgram.UI.Maps.Interface;

namespace RPGMainProgram.BLL.MoveUser
{
    public interface IMover
    {
        ValidMoveType movePlayer(ConsoleKey key, Player player, ICollider mapCollider, string[] possiblePositions);
        ValidMoveType ObjectInTheWay(int[] newPosition, ICollider mapCollider, Player player);
    }
}
