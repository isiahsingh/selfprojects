﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Characters.Humans;
using RPGMainProgram.BLL.MapCollider;
using RPGMainProgram.UI.Maps;
using RPGMainProgram.UI.Maps.Interface;
using RPGMainProgram.UserResponse.Interface;

namespace RPGMainProgram.UserResponse.Implementation
{
    public class WoodsInputDisplayer : IInputDisplayer
    {

        public void MoveResponse(ValidMoveType nextSpot)
        {
            if (nextSpot == ValidMoveType.Invalid)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("There is something in the way. You can't move there.");
                Console.ForegroundColor = ConsoleColor.Gray;
            }
        }


        public string NaturalObjectFinder(ValidMoveType nextSpot, Player player, ICollider mapCollider)
        {
            string response;
            if (nextSpot == ValidMoveType.Woods)
            {
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("Would you like to cut down this tree? It costs 8 points. (y/n)");
                response = Console.ReadLine();
                if (response == "y")
                    response = "yesCutTree";
                else
                    response = "noTree";
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            else if (nextSpot == ValidMoveType.Exit)
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("Do you want to leave the woods? (y/n)");
                response = Console.ReadLine();
                if (response == "y")
                    response = "yesLeaveWoods";
                else
                    response = "noLeaveWoods";
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            else if (nextSpot == ValidMoveType.Invalid)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                //*********
                Console.WriteLine("You can not move off the board.");
                response = "Invalid";
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            else if (nextSpot == ValidMoveType.Valid)
            {
                mapCollider.UpdateMapCollider(player);
                MapEngine.DisplayMap(mapCollider, player);
                response = "";
            }
            else
            {
                response = "";
            }

            return response;
        }
    }
}
