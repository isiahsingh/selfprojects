﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Characters.Humans;
using RPGMainProgram.BLL;
using RPGMainProgram.BLL.MapCollider;
using RPGMainProgram.UI.Maps;
using RPGMainProgram.UI.Maps.Interface;
using RPGMainProgram.UserResponse.Interface;

namespace RPGMainProgram.UserResponse.Implementation
{
    public class OutsideInputDisplayer : IInputDisplayer
    {
        public void MoveResponse(ValidMoveType nextSpot)
        {
            if (nextSpot == ValidMoveType.Invalid)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("There is something in the way. You can't move there.");
                Console.ForegroundColor = ConsoleColor.Gray;
            }
        }

        public string NaturalObjectFinder(ValidMoveType nextSpot, Player player, ICollider mapCollider)
        {
            string response;
            if (nextSpot == ValidMoveType.Woods)
            {
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("You are at the woods. Do you want to enter? (y/n)");
                response = Console.ReadLine();
                if (response == "y")
                    response = "yesTree";
                else
                    response = "noTree";
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            else if (nextSpot == ValidMoveType.Water)
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("You are at a body of water. Do you want to go in? (y/n)");
                response = Console.ReadLine();
                if (response == "y")
                    response = "yesWater";
                else
                    response = "noWater";
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            else if (nextSpot == ValidMoveType.Invalid)
            {
                response = "Invalid";
            }
            else if (nextSpot == ValidMoveType.Valid)
            {
                mapCollider.UpdateMapCollider(player);
                MapEngine.DisplayMap(mapCollider, player);
                response = "";
            }
            else
            {
                response = "";
            }

            return response;
        }
    }
}
