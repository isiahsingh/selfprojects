﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Characters.Humans;
using RPGMainProgram.BLL.MapCollider;
using RPGMainProgram.UI.Maps.Interface;

namespace RPGMainProgram.UserResponse.Interface
{
    public interface IInputDisplayer
    {
        void MoveResponse(ValidMoveType nextSpot);
        string NaturalObjectFinder(ValidMoveType nextSpot, Player player, ICollider mapCollider);
    }
}
