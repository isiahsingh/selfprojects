﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Characters.Humans;
using RPGMainProgram.BLL;
using RPGMainProgram.BLL.Implementation;
using RPGMainProgram.BLL.MapCollider;
using RPGMainProgram.BLL.MapCollider.Implementation;
using RPGMainProgram.BLL.MoveUser;
using RPGMainProgram.UI.Maps;
using RPGMainProgram.UI.Maps.Interface;
using RPGMainProgram.UserResponse.Implementation;
using RPGMainProgram.UserResponse.Interface;

namespace RPGMainProgram.UI
{
    public class GameStarter
    {
       // private IMap _gameMap;
        private ICollider _mapCollider;
        private IMover _move;
        private IInputDisplayer _dispInput;
        int[] _changeMapPlayerPosition  = new int[2];
        private Player player;

        //Must Keep this order -- Left, Up, Down, Right
        public static string[] playerPositions = new[] { "  ◄ ", "  ▲ ", "  ▼ ", "  ► " };

        public GameStarter(string playerName)
        {
            player = new Player(playerName) { playerCharacter = "  ▲ " };
            _mapCollider = new OutsideMapCollider(player);
            _move = new UserMover();
            _dispInput = new OutsideInputDisplayer();
        }

        public void StartGame()
            {                
                GameResults results = GameResults.Invalid;

                do
                {
                    ConsoleKeyInfo key;
                    do
                    {
                        MapEngine.DisplayMap(_mapCollider, player);
                        ValidMoveType nextSpot = new ValidMoveType();
                        Console.WriteLine("Use arrow keys to move your player.");
                        key = Console.ReadKey();

                        nextSpot = _move.movePlayer(key.Key, player, _mapCollider, playerPositions);
                        string enterResponse = _dispInput.NaturalObjectFinder(nextSpot, player, _mapCollider);
                        checkChangeMap(enterResponse, player);
                    } while (key.Key != ConsoleKey.UpArrow || key.Key != ConsoleKey.DownArrow ||
                             key.Key != ConsoleKey.RightArrow || key.Key != ConsoleKey.LeftArrow);
                } while (results != GameResults.Dead || results != GameResults.Win);
            }

            public void checkChangeMap(string enterResponse, Player player)
            {
                switch (enterResponse)
                {
                    case "yesTree":
                        _changeMapPlayerPosition = player.playerPosition;
                        player.playerPosition = new[] { 18, 8 };
                        _mapCollider = new WoodsMapCollider(player);
                        _move = new WoodsMover();
                        _dispInput = new WoodsInputDisplayer();
                        break;
                    case "yesLeaveWoods":
                        player.playerPosition = _changeMapPlayerPosition;
                        _mapCollider = new OutsideMapCollider(player);
                        _move = new UserMover();
                        _dispInput = new OutsideInputDisplayer();
                        break;
                    case "noLeaveWoods":
                        break;
                    case "yesCutTree":
                        //Check and subtract 8 points
                        break;
                    case "noTree":
                        break;
                    case "yesWater":

                        break;
                    case "noWater":

                        break;
                    case "Invalid":
                        Console.WriteLine("Sorry, you can not move there. Press ENTER to continue.");
                        Console.ReadLine();
                        break;
                    default:
                        break;
                }
            }
        }
}
