﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGMainProgram.UI
{
    public class Menu
    {
        public string StartMenu()
        {
            Console.WriteLine(@"
              __  __                 ____                              
             |  \/  | __ _ _______  |  _ \ _   _ _ __  _ __   ___ _ __ 
             | |\/| |/ _` |_  / _ \ | |_) | | | | '_ \| '_ \ / _ \ '__|
             | |  | | (_| |/ /  __/ |  _ <| |_| | | | | | | |  __/ |   
             |_|  |_|\__,_/___\___| |_| \_\\__,_|_| |_|_| |_|\___|_|   
                                                           ");

            Console.WriteLine("\n\n\n\n\t\t Press ENTER to start game.");
            Console.ReadLine();

            Console.Write("\n\n Player, enter your name : ");
            string playerName = Console.ReadLine();

            return playerName;
        }
    }
}
