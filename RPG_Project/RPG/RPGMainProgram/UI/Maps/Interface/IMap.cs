﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGMainProgram.UI.Maps.Interface
{
    public interface IMap
    {
        string[,] boardMap { get; set; }
    }
}
