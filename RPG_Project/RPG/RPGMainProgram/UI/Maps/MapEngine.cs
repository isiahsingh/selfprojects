﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Characters.Humans;
using RPGMainProgram.BLL;
using RPGMainProgram.BLL.MapCollider;

namespace RPGMainProgram.UI.Maps
{
    public static class MapEngine
    {
        public static void DisplayMap(ICollider mapCollider, Player player)
        {
            Console.Clear();
            for (int x = 0; x < 20; x++)
            {
                for (int y = 0; y < 16; y++)
                {
                    if (mapCollider.enumMap[x, y] == MapType.Wall)
                    {
                        Console.Write("████");
                    }
                    else if (mapCollider.enumMap[x, y] == MapType.Tree)
                    {
                        Console.Write("↕↕↕↕");
                    }
                    else if (mapCollider.enumMap[x, y] == MapType.Water)
                    {
                        Console.Write("≈≈≈≈");
                    }
                    else if (mapCollider.enumMap[x, y] == MapType.Empty)
                    {
                        Console.Write("    ");
                    }
                    else if (mapCollider.enumMap[x, y] == MapType.Player)
                    {
                        Console.Write(player.playerCharacter);
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
                   