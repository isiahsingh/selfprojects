﻿using NUnit.Framework;
using SGBank.BLL;
using SGBank.Models;

namespace SGBank.Tests
{
    [TestFixture]
    public class AccountManagerTests
    {
        [Test]
        public void FoundAccountReturnsSuccess()
        {
            var manager = new AccountManager();
            Response<Account> response = manager.GetAccount(1);

            Assert.IsTrue(response.Success);
            Assert.AreEqual(1, response.Data.AccountNumber);
        }

        [Test]
        public void NotFoundAccountReturnsFails()
        {
            var manager = new AccountManager();
            Response<Account> response = manager.GetAccount(9999);

            Assert.IsFalse(response.Success);
        }

        [Test]
        public void DepositReturnsSuccessWithValidAccount()
        {
            var manager = new AccountManager();
            Account accountToUpdate = manager.GetAccount(2).Data;
            Response<DepositReceipt> response = manager.Deposit(accountToUpdate, 100.00m);

            Assert.IsTrue(response.Success);
            Assert.AreEqual(223.00M, response.Data.NewBalance);
            Assert.AreEqual(100.00M, response.Data.DepositAmount);
        }

        [Test]
        public void DepositReturnsFailWithInvalidAccount()
        {
            var manager = new AccountManager();
            Account accountToUpdate = manager.GetAccount(9999).Data;
            Response<DepositReceipt> response = manager.Deposit(accountToUpdate, 100m);

            Assert.IsFalse(response.Success);
        }

        [Test]
        public void WithdrawalReturnsSuccessWithValidAccount()
        {
            var manager = new AccountManager();
            Account accountToUpdate = manager.GetAccount(5).Data;
            Response<WithdrawalReceipt> response = manager.Withdrawal(accountToUpdate, 1000m);

            Assert.IsTrue(response.Success);
            Assert.AreEqual(1000m, response.Data.WithdrawalAmount);
            Assert.AreEqual(1000m, response.Data.NewBalance);
        }

        [Test]
        public void WithdrawalReturnsFailWithInvalidAccount()
        {
            var manager = new AccountManager();
            Account accountToUpdate = manager.GetAccount(9999).Data;
            Response<WithdrawalReceipt> response = manager.Withdrawal(accountToUpdate, 100m);

            Assert.IsFalse(response.Success);
        }

        [Test]
        public void TransferReturnsSuccessWithValidAccounts()
        {
            var manager = new AccountManager();
            Account transferToAccount = manager.GetAccount(3).Data;
            Account transferFromAccount = manager.GetAccount(4).Data;
            Response<TransferReceipt> response = manager.Transfer(transferFromAccount, transferToAccount, 100.00m);

            Assert.IsTrue(response.Success);
            Assert.AreEqual(100.00m, response.Data.TransferDepositReceipt.DepositAmount);
            Assert.AreEqual(100.00m, response.Data.TransferWithdrawalReceipt.WithdrawalAmount);
            Assert.AreEqual(655.00m, response.Data.TransferDepositReceipt.NewBalance);
            Assert.AreEqual(900.00m, response.Data.TransferWithdrawalReceipt.NewBalance);
        }

        [Test]
        public void TransferReturnsFailureWithInvalidAccounts()
        {
            var manager = new AccountManager();
            Account transferToAccount = manager.GetAccount(9999).Data;
            Account transferFromAccount = manager.GetAccount(555).Data;
            Response<TransferReceipt> response = manager.Transfer(transferFromAccount, transferToAccount, 100.00m);

            Assert.IsFalse(response.Success);
        }

        [Test]
        public void TransferReturnsFailureWithOneInvalidAccount()
        {
            var manager = new AccountManager();
            Account transferToAccount = manager.GetAccount(9999).Data;
            Account transferFromAccount = manager.GetAccount(6).Data;
            Response<TransferReceipt> response = manager.Transfer(transferFromAccount, transferToAccount, 500.00m);

            Assert.IsFalse(response.Success);
            Assert.AreEqual(3000.00m, transferFromAccount.Balance);
        }

        [Test]
        public void DeleteAccount()
        {
            var manager = new AccountManager();
            Account accountToDelete = manager.GetAccount(1).Data;

            manager.DeleteAccount(accountToDelete);
            Response<Account> deletedAccount = manager.GetAccount(1);

            Assert.IsFalse(deletedAccount.Success);
        }

        [Test]
        public void GenerateNewAccountNumberReturnsNextNumber()
        {
            var manager = new AccountManager();

            int actual = manager.GenerateNewAccountNumber();

            Assert.AreEqual(7, actual);
        }
    }
}