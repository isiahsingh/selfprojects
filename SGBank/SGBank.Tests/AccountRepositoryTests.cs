﻿using System.Collections.Generic;
using NUnit.Framework;
using SGBank.Data;
using SGBank.Models;

namespace SGBank.Tests
{
    [TestFixture]
    public class AccountRepositoryTests
    {
        [Test]
        public void CanLoadAccounts()
        {
            var repo = new AccountRepository();

            List<Account> accounts = repo.GetAllAcounts();

            Assert.GreaterOrEqual(accounts.Count, 1);
        }

        [TestCase(1, "Mary")]
        [TestCase(2, "Bob")]
        public void CanLoadSpecificAccount(int accountNumber, string expected)
        {
            var repo = new AccountRepository();

            Account account = repo.LoadAccount(accountNumber);

            Assert.AreEqual(expected, account.FirstName);
        }

        [Test]
        public void UpdateAccountSucceeds()
        {
            var repo = new AccountRepository();
            var accountToUpdate = repo.LoadAccount(1);
            accountToUpdate.Balance = 500.00m;
            repo.UpdateAccount(accountToUpdate);

            var result = repo.LoadAccount(1);

            Assert.AreEqual(500.00m, result.Balance);
        }

        [Test]
        public void DeleteAccount()
        {
            var repo = new AccountRepository();
            var accountToDelete = repo.LoadAccount(1);
            repo.DeleteAccount(accountToDelete);

            var resultList = repo.GetAllAcounts();

            Assert.AreEqual(3, resultList.Count);
        }
    }
}