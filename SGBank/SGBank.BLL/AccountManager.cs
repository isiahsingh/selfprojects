﻿using System;
using System.Collections.Generic;
using System.Linq;
using SGBank.Data;
using SGBank.Models;

namespace SGBank.BLL
{
    public class AccountManager
    {
        public Response<Account> GetAccount(int accountNumber)
        {
            var repo = new AccountRepository();
            var response = new Response<Account>();

            try
            {
                Account account = repo.LoadAccount(accountNumber);

                if (account == null)
                {
                    response.Success = false;
                    response.Message = "Account was not found!";
                }
                else
                {
                    response.Success = true;
                    response.Data = account;
                }
            }
            catch(Exception)
            {
                response.Success = false;
                response.Message = "There was an error.  Please try again later.";
                //log.logError(ex.Message);
            }
            return response;
        }

        public Response<DepositReceipt> Deposit(Account account, decimal depositAmount)
        {
            var response = new Response<DepositReceipt>();

            try
            {
                if (depositAmount <= 0)
                {
                    response.Success = false;
                    response.Message = "Must deposit a positive value.";
                }
                else
                {
                    account.Balance += depositAmount;
                    var repo = new AccountRepository();
                    repo.UpdateAccount(account);
                    response.Success = true;
                    response.Data = new DepositReceipt()
                    {
                        AccountNumber = account.AccountNumber,
                        DepositAmount = depositAmount,
                        NewBalance = account.Balance
                    };
                }
            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "There was an error.  Please try again later.";
                // log.logError(ex.Message)
            }
            return response;
        }

        public Response<WithdrawalReceipt> Withdrawal(Account account, decimal withdrawalAmount)
        {
            var response = new Response<WithdrawalReceipt>();
            try
            {
                if (withdrawalAmount > account.Balance)
                {
                    response.Success = false;
                    response.Message = "Insufficient funds.";
                }
                else
                {
                    account.Balance -= withdrawalAmount;
                    var repo = new AccountRepository();
                    repo.UpdateAccount(account);
                    response.Success = true;
                    response.Data = new WithdrawalReceipt()
                    {
                        AccountNumber = account.AccountNumber,
                        WithdrawalAmount = withdrawalAmount,
                        NewBalance = account.Balance
                    };
                }
            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "There was an error.  Please try again later.";
                // log.logError(ex.Message)
            }
            return response;
        }

        public Response<TransferReceipt> Transfer(Account transferFromAccount, Account transferToAccount, decimal transferAmount)
        {
            var repo = new AccountRepository();
            var transferResponse = new Response<TransferReceipt> {Data = new TransferReceipt()};
            Response<DepositReceipt> depositResponse;

            if (transferFromAccount.AccountNumber == transferToAccount.AccountNumber)
            {
                transferResponse.Success = false;
                transferResponse.Message = "You cannot transfer money to the same account.";
                return transferResponse;
            }

            Response<WithdrawalReceipt> withdrawalResponse = Withdrawal(transferFromAccount, transferAmount);

            transferToAccount = repo.LoadAccount(transferToAccount.AccountNumber);

            if (withdrawalResponse.Success)
            {
                transferResponse.Data.TransferWithdrawalReceipt = withdrawalResponse.Data;
                depositResponse = Deposit(transferToAccount, transferAmount);
            }
            else
            {
                transferResponse.Success = false;
                transferResponse.Message = "Insufficient funds.";
                return transferResponse;
            }

            if (depositResponse.Success)
            {
                transferResponse.Success = true;
                transferResponse.Data.TransferDepositReceipt = depositResponse.Data;
                return transferResponse;
            }

            Deposit(transferFromAccount, transferAmount);
            transferResponse.Success = false;
            transferResponse.Message = "There was an error.  Please try again later.";
            return transferResponse;
        }

        public Response<Account> CreateAccount(string firstName, string lastName, decimal startingBalance)
        {
            var response = new Response<Account>();
            var repo = new AccountRepository();

            int newAccountNumber = GenerateNewAccountNumber();

            var newAccount = new Account()
            {
                AccountNumber = newAccountNumber,
                Balance = startingBalance,
                FirstName = firstName,
                LastName = lastName
            };

            try
            {
                if (repo.GetAllAcounts().Contains(newAccount))
                {
                    response.Success = false;
                    response.Message = "This account number already exists.";
                }
                else
                {
                    repo.CreateAccount(newAccount);
                    response.Success = true;
                    response.Message = $"Account created successfully.  New account number is: {newAccount.AccountNumber}. ";
                    response.Data = newAccount;
                }
            }
            catch
            {
                response.Success = false;
                response.Message = "There was an error creating the account.  Please try again later.";
            }
            return response;
        } 

        public Response<Account> DeleteAccount(Account account)
        {
            var response = new Response<Account>();
            var repo = new AccountRepository();
            
            
            try
            {
                //if (accounts)
                //{
                    repo.DeleteAccount(account);
                    response.Success = true;
                    response.Message = "You have successfully deleted the acccount.";
                //}
                //else
                //{
                //    response.Success = false;
                //    response.Message = "There was an error deleting the account.  Please try again later.";
                //}
            }
            catch
            {
                response.Success = false;
                response.Message = "There was an error deleting the account.  Please try again later.";
            }
            return response;
        }

        public int GenerateNewAccountNumber()
        {
            var repo = new AccountRepository();

            return repo.GetAllAcounts().Last().AccountNumber + 1;
        }
    }
}