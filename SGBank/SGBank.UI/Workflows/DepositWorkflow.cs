﻿using System;
using SGBank.BLL;
using SGBank.Models;
using SGBank.UI.Utilities;

namespace SGBank.UI.Workflows
{
    internal class DepositWorkflow
    {
        public void Execute(Account account)
        {
            decimal amountToDeposit = UserPrompts.GetDepositAmount();

            var manager = new AccountManager();
            var response = manager.Deposit(account, amountToDeposit);

            if (response.Success)
            {
                Console.Clear();
                Console.Write("\n Deposited {0:C} to account {1}.", response.Data.DepositAmount, response.Data.AccountNumber);
                Console.Write("\n New balance is {0}.", response.Data.NewBalance);
                UserPrompts.PressKeyForContinue();
            }
            else
            {
                Console.Clear();
                Console.Write("\n An error occurred. {0}", response.Message);
                UserPrompts.PressKeyForContinue();
            }
        }
    }
}
