﻿using System;
using SGBank.BLL;
using SGBank.Models;
using SGBank.UI.Utilities;

namespace SGBank.UI.Workflows
{
    internal class LookupWorkflow
    {
        private Account _currentAccount;

        internal void Execute()
        {
            int accountNumber = UserPrompts.GetAccountNumberFromUser();
            DisplayAccountInformation(accountNumber);
        }

        private void DisplayAccountInformation(int accountNumber)
        {
            var manager = new AccountManager();
            var response = manager.GetAccount(accountNumber);

            Console.Clear();
            if (response.Success)
            {
                _currentAccount = response.Data;
                DisplayLookupMenu();
            }
            else
            {
                Console.Write("\n A problem occurred... ");
                Console.Write("\n {0}", response.Message);
                Console.Write("\n Press any key to continue... ");
                Console.ReadKey();
            }
        }

        

        private void DisplayLookupMenu()
        {
            do
            {
                Console.Clear();
                Displays.PrintAccountDetails(_currentAccount);
                Console.Write("\n 1. Deposit");
                Console.Write("\n 2. Withdraw");
                Console.Write("\n 3. Transfer");
                Console.Write("\n (Q) to Quit");
                Console.Write("\n\n Enter choice: ");
                string input = Console.ReadLine();
                if (input == "")
                {
                    continue;
                }
                if (input != null && input.Substring(0, 1).ToUpper() == "Q")
                    break;

                ProcessChoice(input);
            } while (true);
        }

        private void ProcessChoice(string choice)
        {
            switch (choice)
            {
                case "1":
                    var depositWorkflow = new DepositWorkflow();
                    depositWorkflow.Execute(_currentAccount);
                    break;
                case "2":
                    var withdrawalWorkflow = new WithdrawalWorkflow();
                    withdrawalWorkflow.Execute(_currentAccount);
                    break;
                case "3":
                    var transferWorkflow = new TransferWorkflow();
                    transferWorkflow.Execute(_currentAccount);
                    break;
            }
        }
    }
}
