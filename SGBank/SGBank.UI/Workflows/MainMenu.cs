﻿using System;

namespace SGBank.UI.Workflows
{
    internal class MainMenu
    {
        public void Execute()
        {
            do
            {
                Console.Clear();
                Console.Write("\n\t\t  Welcome to SG Corp. Bank");
                Console.Write("\n ==========================================");
                Console.Write("\n\n 1. Create Account");
                Console.Write("\n 2. Delete Account");
                Console.Write("\n 3. Lookup Account");
                Console.Write("\n\n (Q) to Quit");
                Console.Write("\n\n\n Enter Choice: ");
                string input = Console.ReadLine();
                if (input == "")
                {
                    continue;
                }
                if (input != null && input.Substring(0, 1).ToUpper() == "Q")
                    break;

                ProcessChoice(input);

            } while (true);
        }

        private void ProcessChoice(string choice)
        {
            switch (choice)
            {
                case "1":
                    var create = new CreateAccountWorkflow();
                    create.Execute();
                    break;
                case "2":
                    var delete = new DeleteWorkflow();
                    delete.Execute();
                    break;
                case "3":
                    var lookup = new LookupWorkflow();
                    lookup.Execute();
                    break;
            }
        }
    }
}
