﻿using System;
using SGBank.BLL;
using SGBank.Models;
using SGBank.UI.Utilities;

namespace SGBank.UI.Workflows
{
    public class WithdrawalWorkflow
    {
        public void Execute(Account account)
        {
            var manager = new AccountManager();


            decimal withdrawalAmount = UserPrompts.GetWithdrawalAmount();
            Response<WithdrawalReceipt> response = manager.Withdrawal(account, withdrawalAmount);
            Print(response);    
        }

        private void Print(Response<WithdrawalReceipt> response)
        {
            Console.Clear();
            if (response.Success)
            {
                Console.WriteLine("You Withdrew {0:C} from account {1}.",
                    response.Data.WithdrawalAmount,
                    response.Data.AccountNumber);

                Console.WriteLine("New Balance is {0:C}.", response.Data.NewBalance);
                UserPrompts.PressKeyForContinue();
            }
            else
            {
                Console.Write("\n {0}", response.Message);
                UserPrompts.PressKeyForContinue();
            }
        }
    }
}
