﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGBank.BLL;
using SGBank.Models;
using SGBank.UI.Utilities;

namespace SGBank.UI.Workflows
{
    internal class DeleteWorkflow
    {
        private Account _currentAccount;

        internal void Execute()
        {
            int accountNumber = UserPrompts.GetAccountNumberFromUser();
            DisplayAccountInformation(accountNumber);
        }

        private void DisplayAccountInformation(int accountNumber)
        {
            var manager = new AccountManager();
            var response = manager.GetAccount(accountNumber);

            Console.Clear();
            if (response.Success)
            {
                string input;
                _currentAccount = response.Data;
                
                Displays.PrintAccountDetails(_currentAccount);
                input = DisplayDeletedAccountNotice();
                if (input.ToUpper() == "Y")
                {
                    Response<Account> deleteConfirmation = PerformDeleteOfAccount();
                    DeleteConfirmation(deleteConfirmation);
                }
            }
            else
            {
                Console.Write("\n A problem occurred... ");
                Console.Write("\n {0}", response.Message);
                Console.Write("\n Press any key to continue... ");
                Console.ReadKey();
            }
        }

        private void DeleteConfirmation(Response<Account> response)
        {
            if (response.Success == true)
            {
                Console.WriteLine(response.Message);
                UserPrompts.PressKeyForContinue();
                return;
            }
            else
            {
                Console.WriteLine(response.Message);
                UserPrompts.PressKeyForContinue();
            }
            
        }

        public Response<Account> PerformDeleteOfAccount()
        {
            Response<Account> response = new Response<Account>();
            AccountManager manager = new AccountManager();
            response = manager.DeleteAccount(_currentAccount);
            return response;
        }

        public string DisplayDeletedAccountNotice()
        {
            Console.WriteLine("Are you sure you want to delete this account? (y/n)");
            string input = Console.ReadLine();
            return input;
        }
    }
}
