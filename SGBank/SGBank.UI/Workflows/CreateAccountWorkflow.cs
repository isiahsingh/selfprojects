﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGBank.BLL;
using SGBank.Models;
using SGBank.UI.Utilities;

namespace SGBank.UI.Workflows
{
    internal class CreateAccountWorkflow
    {
        internal void Execute()
        {
            var manager = new AccountManager();
            var response = new Response<Account>();

            string firstName = UserPrompts.GetNewCustomerFirstName();
            string lastName = UserPrompts.GetNewCustomerLastName();
            decimal startingBalance = UserPrompts.GetNewAccountBalance();

            response = manager.CreateAccount(firstName, lastName, startingBalance);

            if (response.Success)
            {
                Console.Clear();
                Displays.PrintAccountDetails(response.Data);
                Console.Write("\n\n {0}", response.Message);
                UserPrompts.PressKeyForContinue();
            }
            else
            {
                Console.Clear();
                Console.Write("\n An error occurred. {0}", response.Message);
                UserPrompts.PressKeyForContinue();
            }
        }
    }
}
