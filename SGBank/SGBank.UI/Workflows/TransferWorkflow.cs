﻿using System;
using SGBank.BLL;
using SGBank.Models;
using SGBank.UI.Utilities;

namespace SGBank.UI.Workflows
{
    internal class TransferWorkflow
    {
        public void Execute(Account fromAccount)
        {
            var manager = new AccountManager();
            Response<Account> toAccount;

            do
            {
                toAccount = manager.GetAccount(UserPrompts.GetAccountNumberFromUser());
                if (toAccount.Success == false)
                {
                    Console.WriteLine("Sorry this account does not exist.");
                    UserPrompts.PressKeyForContinue();
                    return;
                }
                if (toAccount.Data.AccountNumber == fromAccount.AccountNumber)
                {
                    Console.WriteLine("You cannot transfer money to the same account.");
                    UserPrompts.PressKeyForContinue();
                    return;
                }
            } while (toAccount.Success == false);
            
            decimal amountToTransfer = UserPrompts.GetDepositAmount();

            Response<TransferReceipt> response = manager.Transfer(fromAccount, toAccount.Data, amountToTransfer);

            if (response.Success)
            {
                Console.Clear();
                Console.Write("\n Deposited {0} into account {1} from account {2}.",
                    response.Data.TransferWithdrawalReceipt.WithdrawalAmount,
                    response.Data.TransferDepositReceipt.AccountNumber,
                    response.Data.TransferWithdrawalReceipt.AccountNumber);

                Console.Write("\n New balance for account {0}: {1:C}",
                    response.Data.TransferWithdrawalReceipt.AccountNumber,
                    response.Data.TransferWithdrawalReceipt.NewBalance);

                Console.Write("\n New balance for account {0}: {1:C}",
                    response.Data.TransferDepositReceipt.AccountNumber,
                    response.Data.TransferDepositReceipt.NewBalance);
                UserPrompts.PressKeyForContinue();
            }
            else
            {
                Console.Clear();
                Console.Write("\n An error occurred. {0}", response.Message);
                UserPrompts.PressKeyForContinue();
            }
        }
    }
}