﻿using System;
using SGBank.Models;

namespace SGBank.UI.Utilities
{
    internal static class Displays
    {
        internal static void PrintAccountDetails(Account account)
        {
            Console.Write("\n Account Information");
            Console.Write("\n =====================");
            Console.Write("\n Account Number {0}", account.AccountNumber);
            Console.Write("\n Name: {0} {1}", account.FirstName, account.LastName);
            Console.Write("\n Account Balance {0:C}\n", account.Balance);
        }
    }
}