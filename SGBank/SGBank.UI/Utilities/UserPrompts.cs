﻿using System;

namespace SGBank.UI.Utilities
{
    internal static class UserPrompts
    {
        internal static string GetNewCustomerFirstName()
        {
            do
            {
                Console.Clear();
                Console.Write("\n Enter new customer's first name: ");
                string input = InputFormatters.FormatName(Console.ReadLine());
                if (input.Length > 1)
                    return input;
                Console.Write("\n First name must be more than one letter.  Press any key to try again. ");
                Console.ReadKey();
            } while (true);
        }

        internal static string GetNewCustomerLastName()
        {
            do
            {
                Console.Clear();
                Console.Write("\n Enter new customer's last name: ");
                string input = InputFormatters.FormatName(Console.ReadLine());
                if (input.Length > 1)
                    return input;
                Console.Write("\n Last name must be more than one letter.  Press any key to try again. ");
                Console.ReadKey();
            } while (true);
        }

        internal static decimal GetNewAccountBalance()
        {
            do
            {
                Console.Clear();
                Console.Write("\n Enter new customer's starting balance: ");
                string input = Console.ReadLine();
                decimal startingBalance;
                if (decimal.TryParse(input, out startingBalance))
                    return startingBalance;
            } while (true);
        }

        internal static int GetAccountNumberFromUser()
        {
            do
            {
                Console.Clear();
                Console.Write("\n Enter an account number: ");
                string input = Console.ReadLine();
                int accountNumber;
                if (int.TryParse(input, out accountNumber))
                    return accountNumber;
                Console.Write("\n That is not a valid account number.  Press any key to try again. ");
                Console.ReadKey();
            } while (true);
        }

        internal static decimal GetDepositAmount()
        {
            do
            {
                Console.Write("\n Enter deposit amount: ");
                string input = Console.ReadLine();
                decimal depositAmount;
                if (decimal.TryParse(input, out depositAmount))
                    return depositAmount;

                Console.Write("\n That was not a valid amount.  Please try again. ");
                Console.ReadKey();
            } while (true);
        }

        internal static decimal GetWithdrawalAmount()
        {
            do
            {
                Console.WriteLine("Enter withdrawal amount: ");
                string input = Console.ReadLine();
                decimal withdrawalAmount;
                if (decimal.TryParse(input, out withdrawalAmount))
                {
                    return withdrawalAmount;
                }
                
                Console.WriteLine("\n That was not a valid amount. Please try again. ");
                Console.ReadKey();
            } while (true);
        }

        internal static void PressKeyForContinue()
        {
            Console.Write("\n Press any key to continue... ");
            Console.ReadKey();
        }

        
    }
}