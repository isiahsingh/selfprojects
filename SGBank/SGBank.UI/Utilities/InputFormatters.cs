﻿using System.Text;

namespace SGBank.UI.Utilities
{
    internal static class InputFormatters
    {
        internal static string FormatName(string input)
        {
            var name = new StringBuilder();
            for (var i = 0; i < input.Length; i++)
            {
                if (i == 0)
                    name.Append(input[0].ToString().ToUpper());
                if (i != 0)
                    name.Append(input[i].ToString().ToLower());
            }
            return name.ToString();
        }
    }
}
