﻿using SGBank.UI.Workflows;

namespace SGBank.UI
{
    internal static class Program
    {
        private static void Main()
        {
            var menu = new MainMenu();
            menu.Execute();
        }
    }
}