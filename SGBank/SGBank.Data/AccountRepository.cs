﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using SGBank.Models;

namespace SGBank.Data
{
    public class AccountRepository
    {
        private const string FilePath = @"DataFiles\Bank.txt";

        public List<Account> GetAllAcounts()
        {
            var accounts = new List<Account>();
            string[] reader = File.ReadAllLines(FilePath);

            for (var i = 1; i < reader.Length; i++)
            {
                string[] columns = reader[i].Split(',');
                var account = new Account
                {
                    AccountNumber = int.Parse(columns[0]),
                    FirstName = columns[1],
                    LastName = columns[2],
                    Balance = decimal.Parse(columns[3])
                };
                accounts.Add(account);
            }
            return accounts;
        }

        public Account LoadAccount(int accountNumber)
        {
            List<Account> accounts = GetAllAcounts();

            return accounts.FirstOrDefault(i => i.AccountNumber == accountNumber);
        }

        public void UpdateAccount(Account accoutToUpdate)
        {
            List<Account> accounts = GetAllAcounts();
            Account existingAccount = accounts.First(a => a.AccountNumber == accoutToUpdate.AccountNumber);

            existingAccount.FirstName = accoutToUpdate.FirstName;
            existingAccount.LastName = accoutToUpdate.LastName;
            existingAccount.Balance = accoutToUpdate.Balance;

            OverwriteFile(accounts);
        }

        public void CreateAccount(Account account)
        {
            List<Account> accounts = GetAllAcounts();
            accounts.Add(account);
            OverwriteFile(accounts);
        }

        public void DeleteAccount(Account account)
        {
            List<Account> accounts = GetAllAcounts();
            List<Account> accountsWithoutDeletedAccount =
                (accounts.Select(a => a).Where(acc => acc.AccountNumber != account.AccountNumber)).ToList();

            OverwriteFile(accountsWithoutDeletedAccount);
        }

        private void OverwriteFile(List<Account> accounts)
        {
            File.Delete(FilePath);

            using (StreamWriter writer = File.CreateText(FilePath))
            {
                writer.WriteLine("AccountNumber,FirstName,LastName,Balance");
                foreach (Account account in accounts)
                {
                    writer.WriteLine("{0},{1},{2},{3}",
                        account.AccountNumber, account.FirstName, account.LastName, account.Balance);
                }
            }
        }
    }
}