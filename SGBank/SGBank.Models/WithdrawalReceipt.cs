﻿namespace SGBank.Models
{
    public class WithdrawalReceipt
    {
        public int AccountNumber { get; set; }
        public decimal WithdrawalAmount { get; set; }
        public decimal NewBalance { get; set; }
    }
}