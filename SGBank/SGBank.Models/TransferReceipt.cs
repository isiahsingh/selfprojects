﻿namespace SGBank.Models
{
    public class TransferReceipt
    {
        public WithdrawalReceipt TransferWithdrawalReceipt { get; set; }
        public DepositReceipt TransferDepositReceipt { get; set; }
    }
}