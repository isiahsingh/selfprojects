﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HRPortal.BLL;
using Models;

namespace HRPortal.UI.Models
{
    public class CategoryWithPolicyList
    {
        public Category Category { get; set; }
        public List<Policy> PolicyList{ get; set; }

        public CategoryWithPolicyList(Category category)
        {
            var manager = new PolicyManager();
            PolicyList = manager.GetPoliciesForCategory(category).Data;
            Category = category;
        }
    }
}