﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using HRPortal.BLL;
using Models;

namespace HRPortal.UI.Models
{
    public class EmployeeListAndTimeSheet
    {
        public List<Employee> EmployeeList { get; set; }
        public TimeSheet TimeSheet { get; set; }

        public EmployeeListAndTimeSheet()
        {
            var manager = new EmployeeTimeManager();
            EmployeeList = manager.GetAllEmployees().Data;
            TimeSheet = new TimeSheet{ Date = DateTime.Now };
        }
    }
}