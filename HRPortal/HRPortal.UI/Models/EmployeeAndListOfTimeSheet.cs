﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HRPortal.BLL;
using Models;

namespace HRPortal.UI.Models
{
    public class EmployeeAndListOfTimeSheet
    {
        public Employee Employee { get; set; }
        public List<TimeSheet> TimesheetList { get; set; }
        public decimal TotalHours { get; set; }

        public EmployeeAndListOfTimeSheet(int empId)
        {
            var manager = new EmployeeTimeManager();
            TimesheetList = manager.GetAllTimeSheetsForEmployee(empId).Data;
            Employee = manager.GetEmployee(empId).Data;
            TotalHours = TimesheetList.Sum(h => h.Hours);
        }
    }
}