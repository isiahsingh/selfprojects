﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRPortal.BLL;
using Models;

namespace HRPortal.UI.Controllers
{
    public class EmployeeController : Controller
    {
        public ActionResult ListEmployees()
        {
            var manager = new EmployeeTimeManager();
            return View(manager.GetAllEmployees().Data);
        }

        public ActionResult AddEmployee()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddEmployee(Employee employee)
        {
            var manager = new EmployeeTimeManager();
            manager.AddEmployee(employee);
            return RedirectToAction("ListEmployees");
        }

    }
}