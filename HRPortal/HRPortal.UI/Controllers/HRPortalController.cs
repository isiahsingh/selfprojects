﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRPortal.BLL;
using HRPortal.UI.Models;
using Models;

namespace HRPortal.UI.Controllers
{
    public class HRPortalController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllApplications(string alertMsg, string userMsg)
        {
            var manager = new HRManager();
            if (alertMsg != null && userMsg != null)
            {
                ViewData["alertMsg"] = alertMsg;
                ViewData["userMsg"] = userMsg;
                return View(manager.GetAllApplications().Data);
            }
            return View(manager.GetAllApplications().Data);
        }

        public ActionResult EditApplicationInfo(int id)
        {
            var manager= new HRManager();
            return View(manager.GetApplication(id).Data);
        }

        [HttpPost]
        public ActionResult EditApplicationInfo(Application application)
        {
            if (ModelState.IsValid)
            {
                var manager = new HRManager();
                Response<Application> response = manager.EditApplication(application);
                if (response.Success)
                {
                    return RedirectToAction("GetAllApplications", new {alertMsg = "alert alert-success", userMsg = "You have successfully edited application."});
                }
                return RedirectToAction("GetAllApplications", new { alertMsg = "alert alert-danger", userMsg = "Error occured. No application was edited."});
            }
            return View(application);
        }

        public ActionResult ViewPolicies(string alertMsg, string userMsg)
        {
            var manager = new PolicyManager();
            var categoryPolicyList = new List<CategoryWithPolicyList>();
            List<Category> categoryList = manager.GetAllCategories().Data;
            foreach (var category in categoryList)
            {
                var categoryWithPolicies = new CategoryWithPolicyList(category);
                categoryPolicyList.Add(categoryWithPolicies);
            }
            if (alertMsg != null && userMsg != null)
            {
                ViewData["alertMsg"] = alertMsg;
                ViewData["userMsg"] = userMsg;
                return View(categoryPolicyList);
            }
            return View(categoryPolicyList);
        }

        public ActionResult ViewSpecificPolicy(int id)
        {
            var manager = new PolicyManager();
            return View(manager.GetSinglePolicy(id).Data);
        }

        public ActionResult ManageCategories(string alertMsg, string userMsg)
        {
            var manager = new PolicyManager();
            var response = manager.GetAllCategories();
            if (alertMsg != null && userMsg != null)
            {
                ViewData["alertMsg"] = alertMsg;
                ViewData["userMsg"] = userMsg;
                return View(response.Data);
            }
            return View(response.Data);
        }

        [HttpPost]
        public ActionResult AddCategory(Category categoryToAdd)
        {
            var manager = new PolicyManager();
            if (ModelState.IsValid)
            {
                Response<Category> response = manager.AddCategory(categoryToAdd);
                if (response.Success)
                {
                    return RedirectToAction("ManageCategories", new { alertMsg = "alert alert-success", userMsg = "You have successfully added a new category." });
                }
            }

            return RedirectToAction("ManageCategories", new { alertMsg = "alert alert-danger", userMsg = "Error occured, there was no new category created."});
        }

        [HttpPost]
        public ActionResult RemoveCategory(Category category)
        {
            var manager = new PolicyManager();
            var response = manager.RemoveCategory(category);
            if (response.Success)
            {
                return RedirectToAction("ManageCategories", new { alertMsg = "alert alert-success", userMsg = "You have successfully removed a category"});
            }
            return RedirectToAction("ManageCategories", new { alertMsg = "alert alert-danger", userMsg = "Error occured, there was no category removed."});
        }

        [HttpPost]
        public ActionResult RemovePolicy(int Id)
        {
            var manager = new PolicyManager();
            var response = manager.RemovePolicy(Id);
            if (response.Success)
            {
                return RedirectToAction("ViewPolicies", new { alertMsg = "alert alert-success", userMsg = "You have successfully removed a policy" });
            }
            return RedirectToAction("ViewPolicies", new { alertMsg = "alert alert-danger", userMsg = "Error occured, there was no policy removed." });
        }

        [HttpPost]
        public ActionResult AddPolicy(Policy newPolicy)
        {
            int catId = int.Parse(Request.Form["categoryId"]);
            var manager = new PolicyManager();
            if (ModelState.IsValid)
            {
                var response = manager.AddPolicy(newPolicy, catId);
                if (response.Success)
                {
                    return RedirectToAction("ManagePolicies", new { alertMsg = "alert alert-success", userMsg = "You have successfully added a new policy." });
                }
            }

            return RedirectToAction("ManagePolicies", new { alertMsg = "alert alert-danger", userMsg = "Error occured, there was no new policy created."});
        }

        public ActionResult ManagePolicies(string alertMsg, string userMsg)
        {
            var manager = new PolicyManager();
            var response = manager.GetAllCategories();
            if (alertMsg != null && userMsg != null)
            {
                ViewData["alertMsg"] = alertMsg;
                ViewData["userMsg"] = userMsg;
                return View("AddPolicy", response.Data);
            }
            return View("AddPolicy", response.Data);
        }
        
    }
}