﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRPortal.BLL;
using Models;

namespace HRPortal.UI.Controllers
{
    public class ApplicationController : Controller
    {
        // GET: Application
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ApplicationForm()
        {
            return View();
        }

        public ActionResult AppSubmit()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ApplicationForm(Application app)
        {
            var manager = new HRManager();
            if (ModelState.IsValid)
            {
                manager.AddApplication(app);
                return RedirectToAction("AppSubmit");
            }

            return View();
        }

        [HttpPost]
        public ActionResult ApplicationDeleteForm(int id)
        {
            var manager = new HRManager();
            manager.RemoveApplication(id);
            return RedirectToRoute(new {controller = "HRPortal", action = "GetAllApplications"});
        }

    }
}