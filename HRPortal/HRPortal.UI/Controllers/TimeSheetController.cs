﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRPortal.BLL;
using HRPortal.UI.Models;
using Models;

namespace HRPortal.UI.Controllers
{
    public class TimeSheetController : Controller
    {
        private readonly EmployeeTimeManager _manager;

        public TimeSheetController()
        {
            _manager = new EmployeeTimeManager();
        }

        public ActionResult SubmitTimesheet()
        {
            var empListAndTimesheet = new EmployeeListAndTimeSheet();
            return View(empListAndTimesheet);
        }

        [HttpPost]
        public ActionResult SubmitTimesheet(EmployeeListAndTimeSheet viewModel)
        {
            if (ModelState.IsValid)
            {
                _manager.AddTimeSheet(viewModel.TimeSheet);
                return RedirectToAction("ViewTimesheet");
            }
            else
            {
                //var empListAndTimesheet = new EmployeeListAndTimeSheet();
                return View(viewModel);
            }
        }

        public ActionResult ViewTimesheet()
        {
            return View(_manager.GetAllEmployees().Data);
        }

        public ActionResult ShowEmployeeInfo(int empId)
        {
            var employeeAndTimesheets = new EmployeeAndListOfTimeSheet(empId);
            return View(employeeAndTimesheets);
        }

        [HttpPost]
        public ActionResult RemoveTimeSheet(int id, int empId)
        {
            Response<TimeSheet> response =  _manager.RemoveTimeSheet(id);
            var employeeAndTimesheets = new EmployeeAndListOfTimeSheet(empId);
            return View("ShowEmployeeInfo", employeeAndTimesheets);
        }
    }
}

