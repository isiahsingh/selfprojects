﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRPortal.Data;
using Models;
using NUnit.Framework;

namespace HRPortal.Tests
{
    [TestFixture]
    public class EmployeeTests
    {
        [Test]
        public void CreateEmployeeTest()
        {
            var newEmployee = new Employee()

            {
                
                FirstName = "Steven",
                LastName = "James",
                HireDate = DateTime.Parse("1/1/2016")
            };
            var newEmployee2 = new Employee()

            {

                FirstName = "Scoopy",
                LastName = "James",
                HireDate = DateTime.Parse("1/1/2016")
            };

            var repo = new EmployeeRepository();
            repo.CreateEmployee(newEmployee);
            repo.CreateEmployee(newEmployee2);
            var actual = repo._employeeList.Count;
            Assert.AreEqual(2,actual);

        }

        [Test]
        public void GetEmployeeTest()
        {
            var repo = new EmployeeRepository();
            var fetchEmployee = repo.GetEmployee(1);
            var actual = new Employee()
            {
                FirstName = "Steven",
                LastName = "James",
                Id = 1,
                HireDate = DateTime.Parse("1/1/2016")
            };
            Assert.AreEqual(fetchEmployee.Id,actual.Id);
            Assert.AreEqual(fetchEmployee.FirstName,actual.FirstName);
            Assert.AreEqual(fetchEmployee.LastName,actual.LastName);
            Assert.AreEqual(fetchEmployee.HireDate,actual.HireDate);
        }

        [Test]
        public void EditEmployeeTest()
        {
            var repo = new EmployeeRepository();
            var updateEmployee = new Employee()
            {
                Id = 1,
                FirstName = "Stephanie",
                LastName = "James",
                HireDate = DateTime.Parse("1/2/2016")
            };
            repo.UpdateEmployee(updateEmployee);
            var fetchEmployee = repo.GetEmployee(1);
            Assert.AreEqual(fetchEmployee.Id, updateEmployee.Id);
            Assert.AreEqual(fetchEmployee.FirstName, updateEmployee.FirstName);
            Assert.AreEqual(fetchEmployee.LastName, updateEmployee.LastName);
            Assert.AreEqual(fetchEmployee.HireDate, updateEmployee.HireDate);

        }

        [Test]
        public void DeleteEmployeeTest()
        {
            var repo = new EmployeeRepository();
            repo.DeleteEmployee(1);
            repo.DeleteEmployee(2);
            Assert.AreEqual(0,repo._employeeList.Count);
        }
    }
}
