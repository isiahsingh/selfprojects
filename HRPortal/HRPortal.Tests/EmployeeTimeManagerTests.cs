﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRPortal.BLL;
using HRPortal.Data;
using Models;
using NUnit.Framework;

namespace HRPortal.Tests
{
    [TestFixture]
    public class EmployeeTimeManagerTests
    {
        [Test]
        public void AddEmployeeValid()
        {
            var employeeToAdd = new Employee
            {
                FirstName = "John",
                LastName = "Doe",
                HireDate = Convert.ToDateTime("Feb 4, 1998")
            };
            var manager = new EmployeeTimeManager();
            var response = manager.AddEmployee(employeeToAdd);

            Assert.IsTrue(response.Success);
            Assert.AreEqual(1, response.Data.Id);
        }

        [Test]
        public void AddTwoEmployees()
        {
            var employeeToAdd1 = new Employee
            {
                FirstName = "Han",
                LastName = "Solo",
                HireDate = Convert.ToDateTime("May 25, 1977")
            };
            var employeeToAdd2 = new Employee
            {
                FirstName = "Chew",
                LastName = "bacca",
                HireDate = Convert.ToDateTime("May 25, 1977")
            };

            var manager = new EmployeeTimeManager();
            var response1 = manager.AddEmployee(employeeToAdd1);
            var response2 = manager.AddEmployee(employeeToAdd2);

            Assert.IsTrue(response1.Success);
            Assert.IsTrue(response2.Success);
            Assert.AreEqual(2, response1.Data.Id);
            Assert.AreEqual(3, response2.Data.Id);
        }

        [Test]
        public void GetValidEmployee()
        {
            var manager = new EmployeeTimeManager();
            var response = manager.GetEmployee(1);

            Assert.IsTrue(response.Success);
            Assert.AreEqual(1, response.Data.Id);
            Assert.AreEqual("John", response.Data.FirstName);
        }

        [Test]
        public void GetInvalidEmployeeReturnsError()
        {
            var manager = new EmployeeTimeManager();
            var response = manager.GetEmployee(8);

            Assert.IsFalse(response.Success);
        }

        [Test]
        public void AddTimeSheetforValidEmployee()
        {
            var newTime = new TimeSheet
            {
                EmpId = 1,
                Date = Convert.ToDateTime("Jan 15, 2016"),
                Hours = 8
            };
            var manager = new EmployeeTimeManager();
            var response = manager.AddTimeSheet(newTime);

            Assert.IsTrue(response.Success);
            Assert.AreEqual(8, response.Data.Hours);
        }

        [Test]
        public void AddTimeSheetforInvalidEmployeeReturnsError()
        {
            var newTime = new TimeSheet
            {
                EmpId = 10,
                Date = Convert.ToDateTime("Jan 15, 2016"),
                Hours = 8
            };
            var manager = new EmployeeTimeManager();
            var response = manager.AddTimeSheet(newTime);

            Assert.IsFalse(response.Success);
        }

        [Test]
        public void RetrieveTimeSheetWithValidDate()
        {
            var date = Convert.ToDateTime("Jan 15, 2016");
            var empId = 1;
            var manager = new EmployeeTimeManager();
            var response = manager.GetTimeSheetsForSpecificDate(empId, date);

            Assert.IsTrue(response.Success);
            Assert.AreEqual(8, response.Data.Hours);
        }

        [Test]
        public void RetrieveTimeSheetWithInvalidDateReturnsError()
        {
            var date = Convert.ToDateTime("Mar 3, 1999");
            var empId = 1;
            var manager = new EmployeeTimeManager();
            var response = manager.GetTimeSheetsForSpecificDate(empId, date);

            Assert.IsFalse(response.Success);
        }

        [Test]
        public void RetrieveTimeSheetsWithValidEmployee()
        {
            var empId = 1;
            var manager = new EmployeeTimeManager();
            var response = manager.GetAllTimeSheetsForEmployee(empId);

            Assert.IsTrue(response.Success);
        }

        [Test]
        public void RetriveTimeSheetWithInvalidEmployeeReturnsError()
        {
            var empId = 88;
            var manager = new EmployeeTimeManager();
            var response = manager.GetAllTimeSheetsForEmployee(empId);

            Assert.IsFalse(response.Success);
        }
    }
}
