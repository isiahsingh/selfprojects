﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRPortal.Data;
using Models;
using NUnit.Framework;

namespace HRPortal.Tests
{
    [TestFixture]
    public class ApplicationRepositoryTest
    {
        [Test]
        public void LoadApplicationsFromFile()
        {
            var appList = new List<Application>();
            var repo = new ApplicationRepository();
            appList = repo._applicationList;

            Assert.AreEqual(2, appList.Count);
        }

        [Test]
        public void AddApplicationToFile()
        {
            var applicationToAdd = new Application
            {
                Name = "Bily",
                PhoneNumber = "5550125",
                Email = "billy@yahoo.com",
                Skillset = "Driving, Video Games",
                WorkExp = "SWG, PARTA",
                JobInterest = "Computer SHIT",
                ReviewStatus = "Accepted"
            };

            var applicationToAdd2 = new Application
            {
                Name = "Guy",
                PhoneNumber = "5550125",
                Email = "billy@yahoo.com",
                Skillset = "Driving, Video Games",
                WorkExp = "SWG, PARTA",
                JobInterest = "Computer SHIT",
                ReviewStatus = "Accepted"
            };

            var repo = new ApplicationRepository();
            repo.CreateApplication(applicationToAdd);
            repo.CreateApplication(applicationToAdd2);

            var allApps = repo._applicationList;

            Assert.AreEqual(2, allApps.Count);
        }

        [Test]
        public void AddApplicationToFile2()
        {
            var applicationToAdd3 = new Application
            {
                Name = "Guy",
                PhoneNumber = "5550125",
                Email = "billy@yahoo.com",
                Skillset = "Driving, Video Games",
                WorkExp = "SWG, PARTA",
                JobInterest = "Computer SHIT",
                ReviewStatus = "Accepted"
            };

            var repo = new ApplicationRepository();
            repo.CreateApplication(applicationToAdd3);

            var allApps = repo._applicationList;

            Assert.AreEqual(3, allApps.Count);
        }

        [Test]
        public void RemoveApplicationTest()
        {
            var repo = new ApplicationRepository();
            var applicationToAdd3 = new Application
            {
                Name = "Guy",
                PhoneNumber = "5550125",
                Email = "billy@yahoo.com",
                Skillset = "Driving, Video Games",
                WorkExp = "SWG, PARTA",
                JobInterest = "Computer SHIT",
                ReviewStatus = "Accepted"
            };
            repo.CreateApplication(applicationToAdd3);
            var applicationToAdd4 = new Application
            {

                Name = "Guy",
                PhoneNumber = "5550125",
                Email = "billy@yahoo.com",
                Skillset = "Driving, Video Games",
                WorkExp = "SWG, PARTA",
                JobInterest = "Computer SHIT",
                ReviewStatus = "Accepted"
            };
            repo.CreateApplication(applicationToAdd4);
            repo.DeleteApplication(1);
            repo.DeleteApplication(2);
            Assert.AreEqual(3, repo._applicationList.Count);
        }
    }
}
