﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using HRPortal.Data;
//using Models;
//using NUnit.Framework;

//namespace HRPortal.Tests
//{
//    [TestFixture]
//    public class TimeSheetTests
//    {
//        [Test]
//        public void AddTimeSheetToFileTest()
//        {
//            var timeList = new List<TimeSheet>();
//            var repo = new TimeSheetRepository();
//            var newEmployee = new Employee()
//            {
//                Id = 1,
//                FirstName = "Steve",
//                LastName = "James",
//                HireDate = DateTime.Parse("1/1/2016")
//            };
//            var newTimeSheet = new TimeSheet()
//            {
//                EmpId = 1,
//                Date = DateTime.Parse("2/18/2016"),
//                Hours = 9,
//                Id = 1
//            };
//            var newTimeSheet2 = new TimeSheet()
//            {
//                EmpId = 1,
//                Date = DateTime.Parse("2/19/2016"),
//                Hours = 9,
//                Id = 2
//            };
//            repo.CreateTimeSheet(newTimeSheet);
//            repo.CreateTimeSheet(newTimeSheet2);
//            timeList = repo.GetTimeSheetForSpecificEmployee(newEmployee);
//            var actual = timeList.Count;
//            Assert.AreEqual(2, actual);
//        }

//        [Test]
//        public void DeleteTimeSheetTest()
//        {
//            var repo = new TimeSheetRepository();
//            var timeSheetToDelete = new TimeSheet()
//            {
//                Id = 2
//            };
//            repo.DeleteTimeSheet(timeSheetToDelete);
//            var actual = repo._timeSheetList.Count;
//            Assert.AreEqual(1, actual);
//        }
//    }
//}
