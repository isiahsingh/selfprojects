﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRPortal.Data;
using Models;

namespace HRPortal.BLL
{
    public class HRManager
    {
        public Response<Application> AddApplication(Application newApp)
        {
            var repo = new ApplicationRepository();
            var response = new Response<Application>();

            try
            {
                repo.CreateApplication(newApp);
                if (repo._applicationList.Any(a => a.Id == newApp.Id))
                {
                    response.Success = true;
                    response.Message = "You have successfully added an application";
                    response.Data = newApp;
                }
                else
                {
                    response.Success = false;
                    response.Message = "Application could not be added.";
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = "There was an error adding the application. Try again later.";
            }

            return response;
        }

        public Response<Application> RemoveApplication(int Id)
        {
            var repo = new ApplicationRepository();
            var response = new Response<Application>();

            try
            {
                repo.DeleteApplication(Id);
                if (repo._applicationList.Any(a => a.Id == Id))
                {
                    response.Success = false;
                    response.Message = "Your application was not removed";
                }
                else
                {
                    response.Success = true;
                    response.Message = "Your application has been removed.";
                    
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = "There was an error removing the application. Try again later.";
            }
            return response;
        }

        public Response<Application> GetApplication(int AppID)
        {
            var repo = new ApplicationRepository();
            var response = new Response<Application>();

            try
            {
                var applicationToDisplay = repo.GetApplication(AppID);
                response.Success = true;
                response.Message = "Application Found.";
                response.Data = applicationToDisplay;
            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "Invalid application or not found.";


            }
            return response;
        }

        public Response<List<Application>> GetAllApplications()
        {
            var repo = new ApplicationRepository();
            var response = new Response<List<Application>>();

            try
            {
                var allApps = repo._applicationList;
                response.Success = true;
                response.Data = allApps;
            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "There has been an error in retrieving your applications.";

            }
            return response;
        }

        public Response<Application> EditApplication(Application AppToEdit)
        {
            var repo = new ApplicationRepository();
            var response = new Response<Application>();

            try
            {
                repo.UpdateApplication(AppToEdit);
                response.Success = true;
                response.Data = AppToEdit;
                response.Message = "App edited.";

            }
            catch
            {
                response.Success = false;
                response.Message = "Error in editing.";
            }
            return response;
        }

    }
}
