﻿using HRPortal.Data;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace HRPortal.BLL
{
    public class PolicyManager

    {
        public Response<Policy> AddPolicy(Policy newPolicy, int catId)
        {
            var repo = new PolicyRepository();
            var response = new Response<Policy>();
            try
            {
                repo.CreatePolicy(newPolicy, catId);
                if (repo._policyList.Any(p => p.Id == newPolicy.Id))
                {
                    response.Success = true;
                    response.Message = "You have successfully added a policy.";
                    response.Data = newPolicy;
                }
                else
                {
                    response.Success = false;
                    response.Message = "Policy could not be added.";
                }
            }
            catch (Exception)
            {

                response.Success = false;
                response.Message = "There was an error adding the policy.";
            }
            return response;
        }

        public Response<Category> AddCategory(Category categoryToAdd)
        {
            var repo = new CategoryRepository();
            var response = new Response<Category>();

            try
            {
                repo.CreateCategory(categoryToAdd);
                if (repo._categoryList.Any(c => c.Id == categoryToAdd.Id))
                {
                    response.Success = true;
                    response.Message = "You have successfully added a category.";
                    response.Data = categoryToAdd;
                }
                else
                {
                    response.Success = false;
                    response.Message = "Category could not be added.";
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = "There was an error adding the category. Please try again later.";
            }

            return response;
        }

        public Response<Category> RemoveCategory(Category categoryToDelete)
        {
            var catRepo = new CategoryRepository();
            var polRepo = new PolicyRepository();
            var response = new Response<Category>();

            try
            {
                catRepo.DeleteCategory(categoryToDelete.Id);
                polRepo.DeleteAllPoliciesInCategory(categoryToDelete.Id);
                if (catRepo._categoryList.Any(c => c.Id == categoryToDelete.Id) ||
                    polRepo._policyList.Any(p => p.CategoryId == categoryToDelete.Id))
                {
                    response.Success = false;
                    response.Message = "Category or policies associated with the category were not all deleted.";
                }
                else
                {
                    response.Success = true;
                    response.Message =
                        "You have successfully deleted the category and all policies associated with that category.";
                    response.Data = categoryToDelete;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = "There was an error removing the category. Try again later.";
            }
            return response;
        } 

        public Response<Policy> RemovePolicy(int Id)
        {
            var repo = new PolicyRepository();
            var response = new Response<Policy>();

            try
            {
                repo.DeletePolicy(Id);
                if (repo._policyList.Any(a => a.Id == Id))
                {
                    response.Success = false;
                    response.Message = "Your policy couldn not be deleted.";

                }
                else
                {
                    response.Success = true;
                    response.Message = "Your policy has been removed.";
                   
                }
            }
            catch (Exception)
            {

                response.Success = false;
                response.Message = "There was an error removing the policy. Try again later.";
            }
            return response;
        }

        public Response<Policy> GetSinglePolicy(int id)
        {
            var repo = new PolicyRepository();
            var response = new Response<Policy>();
            try
            {
                Policy policy = repo.GetPolicy(id);
                response.Success = true;
                response.Data = policy;
            }
            catch
            {
                response.Success = false;
                response.Message = "There was no policy retrieved.";
            }
            return response;
        } 

        public Response<List<Policy>> GetPoliciesForCategory(Category category)
        {
            var repo = new PolicyRepository();
            var response = new Response<List<Policy>>();

            try
            {
                List<Policy> policyList = repo.GetPoliciesForSpecificCategory(category);
                response.Success = true;
                response.Message = "Your policies have been retrieved.";
                response.Data = policyList;
            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "Couldn't retrieve policies. Try again later.";
            }
            return response;
        }

        public Response<List<Category>> GetAllCategories()
        {
            var repo = new CategoryRepository();
            var response = new Response<List<Category>>();
            try
            {
                List<Category> categoryList = repo._categoryList;
                if (categoryList.Count != 0)
                {
                    response.Success = true;
                    response.Data = categoryList;
                }
                else
                {
                    response.Success = false;
                    response.Message = "There are no categories. Please add a category.";
                }
            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "There has been an error in retrieving all the categories. Try again later.";
            }
            return response;
        }

        public Response<Category> EditCategory(Category editedCategory)
        {
            var repo = new CategoryRepository();
            var response = new Response<Category>();

            try
            {
                repo.UpdateCategory(editedCategory);
                response.Success = true;
                response.Message = "Your category has been edited";
                response.Data = editedCategory;
            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "There has been an error editing your category. Please try again later.";
            }
            return response;
        }

        public Response<Policy> EditPolicy(Policy editedPolicy)
        {
            var repo = new PolicyRepository();
            var response = new Response<Policy>();

            try
            {
                repo.UpdatePolicy(editedPolicy);
                response.Success = true;
                response.Message = "Your policy has been editied.";
                response.Data = editedPolicy;
            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "There was an error editing the policy. Try again later.";
            }
            return response;
        }
    }
}