﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRPortal.Data;
using Models;

namespace HRPortal.BLL
{
    public class EmployeeTimeManager
    {
        public Response<TimeSheet> AddTimeSheet(TimeSheet newTimeSheet)
        {
            var repo = new TimeSheetRepository();
            var response = new Response<TimeSheet>();
            try
            {
                repo.CreateTimeSheet(newTimeSheet);
                if (repo._timeSheetList.Any(p => p.Id == newTimeSheet.EmpId))
                {
                    response.Success = true;
                    response.Message = "You have successfully added a time sheet.";
                    response.Data = newTimeSheet;
                }
                else
                {
                    response.Success = false;
                    response.Message = "Time sheet was unable to be added.";
                }
            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "There was an error adding the time sheet.";
            }
            return response;
        }

        public Response<Employee> AddEmployee(Employee employeeToAdd)
        {
            var repo = new EmployeeRepository();
            var response = new Response<Employee>();

            try
            {
                repo.CreateEmployee(employeeToAdd);
                if (repo._employeeList.Any(e => e.Id == employeeToAdd.Id))
                {
                    response.Success = true;
                    response.Message = "You have successfully added an employee.";
                    response.Data = employeeToAdd;
                }
                else
                {
                    response.Success = false;
                    response.Message = "Employee unable to be added.";
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = "There was an error adding the employee. " +
                                   "Please try again later.";
            }

            return response;
        }

        public Response<List<Employee>> GetAllEmployees()
        {
            var repo = new EmployeeRepository();
            var response = new Response<List<Employee>>();
            try
            {
                List<Employee> listOfEmployees = repo._employeeList;
                response.Data = listOfEmployees;
                response.Success = true;
            }
            catch
            {
                response.Success = false;
            }
            return response;
        }

        public Response<Employee> GetEmployee(int empId)
        {
            var repo = new EmployeeRepository();
            var response = new Response<Employee>();

            try
            {
                var employee = repo.GetEmployee(empId);
                if (employee == null)
                {
                    response.Success = false;
                    response.Message = "Invalid Employee Id, please try again.";
                }
                else
                {
                    response.Success = true;
                    response.Message = "The employee's information has been retrieved.";
                    response.Data = employee;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = "The employee could not be retrieved. Please try again.";
            }

            return response;
        } 

        public Response<TimeSheet> GetTimeSheetsForSpecificDate(int empId, DateTime date)
        {
            var repo = new TimeSheetRepository();
            var response = new Response<TimeSheet>();

            try
            {
                var empresponse = GetEmployee(empId);
                if (empresponse.Success)
                {
                    var timeSheet = repo.GetTimeSheetForSpecificDate(empresponse.Data, date);
                    if (timeSheet == null)
                    {
                        response.Success = false;
                        response.Message = "Invalid Date, Unable to retrieve time sheets.";
                    }
                    else
                    {
                        response.Success = true;
                        response.Message = "Your time sheets have been retrieved.";
                        response.Data = timeSheet;
                    }
                }
                else
                {
                    response.Success = false;
                    response.Message = empresponse.Message;
                }
            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "Unable to retrieve time sheets. Try again later.";
            }
            return response;
        }

        public Response<List<TimeSheet>> GetAllTimeSheetsForEmployee(int id)
        {
            var repo = new TimeSheetRepository();
            var response = new Response<List<TimeSheet>>();
            try
            {
                List<TimeSheet> timeSheetList = repo._timeSheetList.Where(t => t.EmpId == id).ToList();
                response.Success = true;
                response.Data = timeSheetList;
            }
            catch
            {
                response.Success = false;
            }
            return response;
           
        }

        public Response<Employee> EditEmployee(Employee employeeToEdit)
        {
            var repo = new EmployeeRepository();
            var response = new Response<Employee>();

            try
            {
                repo.UpdateEmployee(employeeToEdit);
                response.Success = true;
                response.Message = "This employee's information has been edited";
                response.Data = employeeToEdit;
            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "There has been an error editing this employee." +
                                   " Please try again later.";
            }
            return response;
        }

        public Response<TimeSheet> RemoveTimeSheet(int timeSheetId)
        {
            var repo = new TimeSheetRepository();
            var response = new Response<TimeSheet>();

            try
            {
                repo.DeleteTimeSheet(timeSheetId);
                if (repo._timeSheetList.Any(t => t.Id == timeSheetId))
                {
                    response.Success = false;
                    response.Message = "The specified time sheet could not be deleted.";
                }
                else
                {
                    response.Success = true;
                    response.Message = "Your time sheet has been deleted.";
                }
            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "There was an error removing the time sheet. Try again later.";
            }
            return response;
        }

        public Response<Employee> RemoveEmployee(int empId)
        {
            var repo = new EmployeeRepository();
            var response = new Response<Employee>();

            try
            {
                repo.DeleteEmployee(empId);
                if (repo._employeeList.Any(e => e.Id == empId))
                {
                    response.Success = false;
                    response.Message = "The specified employee's information could not be deleted.";
                }
                else
                {
                    response.Success = true;
                    response.Message = "The employee's information has been deleted.";
                }
            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "There was an error removing the employee. Try again later.";
            }
            return response;
        }
    }
}
