﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Policy
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Content { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (string.IsNullOrWhiteSpace(Title))
            {
                errors.Add(new ValidationResult("Please Enter a Policy Title",
                    new[] { "Title" }));
            }
            if (string.IsNullOrWhiteSpace(Content))
            {
                errors.Add(new ValidationResult("Please Enter Policy Content",
                    new[] { "Content" }));
            }
            return errors;
        }
    }
}
