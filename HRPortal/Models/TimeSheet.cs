﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class TimeSheet : IValidatableObject
    {
        public int EmpId { get; set; }
        public int Id { get; set; }
        [Required]
        public decimal Hours { get; set; }
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }


        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (Hours < 1 || Hours > 16)
            {
                errors.Add(new ValidationResult("You can not work more than 16 hours, or less than 0.",
                    new[] { "Hours" }));
            }

            return errors;
        }
    }
}
