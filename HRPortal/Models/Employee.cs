﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Employee : IValidatableObject
    {
        public string FirstName  { get; set; }
        public string LastName { get; set; }
        public int Id { get; set; }
        [DataType(DataType.Date)]
        public DateTime HireDate { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var errors = new List<ValidationResult>();

            if (string.IsNullOrWhiteSpace(FirstName))
            {
                errors.Add(new ValidationResult("First Name can not be blank.", new [] {FirstName}));    
            }

            if (string.IsNullOrWhiteSpace(LastName))
            {
                errors.Add(new ValidationResult("Last Name can not be blank", new [] {LastName}));
            }

            if (HireDate < DateTime.Today)
            {
                errors.Add(new ValidationResult("You can not enter a date in the past."));
            }

            return errors;
        }
    }
}
