﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class Application : IValidatableObject
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [MaxLength(length: 10, ErrorMessage = "Phone number must be no more than 10 digits.")]
        [MinLength(length: 10, ErrorMessage = "Must be no less than 10 digits.")]
        public string PhoneNumber { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Skillset { get; set; }
        [Required]
        public string WorkExp { get; set; }
        [Required]
        public string JobInterest { get; set; }
        public string ReviewStatus { get; set; } 

        public Application()
        {
            ReviewStatus = "Not Reviewed";
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (string.IsNullOrWhiteSpace(Name))
            {
                errors.Add(new ValidationResult("Please Enter Your Name",
                    new[] { "Name" }));
            }
            if (string.IsNullOrWhiteSpace(PhoneNumber)|| PhoneNumber.Length < 10 || PhoneNumber.Length > 10)
            {
                errors.Add(new ValidationResult("Please Enter a Phone Number.",
                    new[] { "PhoneNumber" }));
            }
            if (string.IsNullOrWhiteSpace(Email))
            {
                errors.Add(new ValidationResult("Please Enter an E-Mail Address.",
                    new[] { "Email" }));
            }
            if (string.IsNullOrWhiteSpace(Skillset))
            {
                errors.Add(new ValidationResult("Please Enter Your Relevant Skills.",
                    new[] {"Skillset"}));
            }
            if (string.IsNullOrWhiteSpace(WorkExp))
            {
                errors.Add(new ValidationResult("Please Enter Your Previous Work Experience.",
                    new[] { "WorkExp" }));
            }
            if (string.IsNullOrWhiteSpace(JobInterest))
            {
                errors.Add(new ValidationResult("Please Enter Why You Are Interested in this Position.",
                    new[] { "JobInterest" }));
            }
            return errors;
        }
    }
}
