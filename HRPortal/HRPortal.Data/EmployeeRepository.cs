﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Xml;
using System.Xml.Linq;
using Models;

namespace HRPortal.Data
{
    public class EmployeeRepository
    {
        private string _employeePath = HostingEnvironment.MapPath(@"\App_Data\Employee.xml");
        //private string _employeePath = @"Test_App_Data\Employee.xml";
        public List<Employee> _employeeList { get; set; }

        public EmployeeRepository()
        {
            _employeeList = Load();
        }

        private List<Employee> Load()
        {
             var employeeList = (from a in XDocument.Load(_employeePath).Root?.Elements("Employee")
                                select new Employee()
                                {
                                    Id = (int)a.Element("id"),
                                    FirstName = (string)a.Element("firstname"),
                                    LastName = (string)a.Element("lastname"),
                                    HireDate = (DateTime)a.Element("hiredate")
                                }).ToList();

            return employeeList;
        }

        public void CreateEmployee(Employee employeeToAdd)
        {
            if (_employeeList.Count == 0)
            {
                employeeToAdd.Id = 1;
            }
            else
            {
                employeeToAdd.Id = _employeeList.Max(a => a.Id) + 1;
            }
            
            _employeeList.Add(employeeToAdd);
            OverWriteEmployees(_employeeList);

        }

        public void OverWriteEmployees(List<Employee> employeeList)
        {
            var employeeFile = new XDocument();

            if (!File.Exists(_employeePath))
            {
                employeeFile = new XDocument(new XElement("Employees"));
                employeeFile.Save(_employeePath);
            }
            File.Delete(_employeePath);

            using (XmlWriter writer = XmlWriter.Create(_employeePath))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Employees");

                foreach (var employee in employeeList)
                {
                    writer.WriteStartElement("Employee");

                    
                    writer.WriteElementString("firstname", employee.FirstName);
                    writer.WriteElementString("lastname", employee.LastName);
                    writer.WriteElementString("id", employee.Id.ToString());
                    writer.WriteElementString("hiredate", employee.HireDate.ToShortDateString());

                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }
        
        public Employee GetEmployee(int id)
        {
            return _employeeList.FirstOrDefault(p => p.Id == id);
        }

        public void UpdateEmployee(Employee newEmployee)
        {
            var existingEmployee = _employeeList.FirstOrDefault(a => a.Id == newEmployee.Id);

            if (existingEmployee != null)
            {
                existingEmployee.Id = newEmployee.Id;
                existingEmployee.FirstName = newEmployee.FirstName;
                existingEmployee.LastName = newEmployee.LastName;
                existingEmployee.HireDate = newEmployee.HireDate;
            }

             OverWriteEmployees(_employeeList);
        }
     
        public void DeleteEmployee(int id)
        {
            var employeeToDelete = GetEmployee(id);
            _employeeList.Remove(employeeToDelete);
            OverWriteEmployees(_employeeList);
        }
    }
}
