﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Xml;
using System.Xml.Linq;
using Models;

namespace HRPortal.Data
{
    public class CategoryRepository
    {
        private string _categoryPath = HostingEnvironment.MapPath(@"\App_Data\Categories.xml");
        //private readonly string _categoryPath = @"Test_App_Data\Categories.xml";
        public List<Category> _categoryList { get; set; }

        public CategoryRepository()
        {
            _categoryList = Load();
        }

        private List<Category> Load()
        {
            var categoryList = (from a in XDocument.Load(_categoryPath).Root?.Elements("Category")
                              select new Category()
                              {
                                  Id = (int)a.Element("id"),
                                  Type = (string)a.Element("type"),
                              }).ToList();

            return categoryList;
        }

        public void CreateCategory(Category categoryToAdd)
        {
            if (_categoryList.Count == 0)
            {
                categoryToAdd.Id = 1;
            }
            else
            {
                categoryToAdd.Id = _categoryList.Max(a => a.Id) + 1;
            }
            _categoryList.Add(categoryToAdd);
            OverwriteCategories(_categoryList);
        }

        public void OverwriteCategories(List<Category> list)
        {
            var categoryFile = new XDocument();

            if (!File.Exists(_categoryPath))
            {
                categoryFile = new XDocument(new XElement("Categories"));
                categoryFile.Save(_categoryPath);
            }
            File.Delete(_categoryPath);
            using (XmlWriter writer = XmlWriter.Create(_categoryPath))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Categories");

                foreach (var category in list)
                {
                    writer.WriteStartElement("Category");

                    writer.WriteElementString("id", category.Id.ToString());
                    writer.WriteElementString("type", category.Type);

                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }

        public Category GetCategory(int id)
        {
            return _categoryList.FirstOrDefault(c => c.Id == id);
        }

        public void UpdateCategory(Category newCategory)
        {
            var existingCategory = _categoryList.FirstOrDefault(c => c.Id == newCategory.Id);

            if (existingCategory != null)
            {
                existingCategory.Id = newCategory.Id;
                existingCategory.Type = newCategory.Type;
            }
            OverwriteCategories(_categoryList);
        }

        public void DeleteCategory(int id)
        {
            var categoryToDelete = GetCategory(id);
            _categoryList.Remove(categoryToDelete);
            OverwriteCategories(_categoryList);
        }
    }
}
