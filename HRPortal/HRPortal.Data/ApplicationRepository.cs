﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Models;

namespace HRPortal.Data
{
    public class ApplicationRepository
    {
        private string _filepath = HostingEnvironment.MapPath(@"\App_Data\ApplicationFile.xml");
        //private string _filepath = @"Test_App_Data\ApplicationFile.xml";
        public List<Application> _applicationList { get; set; }

        public ApplicationRepository()
        {
            _applicationList = Load();
        }

        private List<Application> Load()
        {
            var applicationList = (from a in XDocument.Load(_filepath).Root?.Elements("Application")
                                   select new Application()
                                   {
                                       Id = (int)a.Element("applicantkey"),
                                       Name = (string)a.Element("name"),
                                       PhoneNumber = (string)a.Element("phone"),
                                       Email = (string)a.Element("email"),
                                       Skillset = (string)a.Element("skillset"),
                                       WorkExp = (string)a.Element("workexp"),
                                       JobInterest = (string)a.Element("jobinterest"),
                                       ReviewStatus = (string)a.Element("status")
                                   }).ToList();

            return applicationList;
        }

        public void CreateApplication(Application application)
        {
            if (_applicationList.Count == 0)
            {
                application.Id = 1;
            }
            else
            {
                var maxApplicantKey = _applicationList.Max(a => a.Id);
                application.Id = maxApplicantKey + 1;
            }

            _applicationList.Add(application);

            OverWriteApplications(_applicationList);
        }

        public void OverWriteApplications(List<Application> applicationList)
        {
            var appfile = new XDocument();

            if (!File.Exists(_filepath))
            {
                appfile = new XDocument(new XElement("Applications"));
                appfile.Save(_filepath);
            }

            File.Delete(_filepath);

            using (XmlWriter writer = XmlWriter.Create(_filepath))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Applications");

                foreach (var app in applicationList)
                {
                    writer.WriteStartElement("Application");

                    writer.WriteElementString("applicantkey", app.Id.ToString());
                    writer.WriteElementString("name", app.Name);
                    writer.WriteElementString("phone", app.PhoneNumber);
                    writer.WriteElementString("email", app.Email);
                    writer.WriteElementString("skillset", app.Skillset);
                    writer.WriteElementString("workexp", app.WorkExp);
                    writer.WriteElementString("jobinterest", app.JobInterest);
                    writer.WriteElementString("status", app.ReviewStatus);

                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }

        public Application GetApplication(int id)
        {
            var app = _applicationList.FirstOrDefault(a => a.Id == id);

            return app;
        }

        public void UpdateApplication(Application newApplication)
        {
            var existingApplication = _applicationList.FirstOrDefault(a => a.Id == newApplication.Id);

            existingApplication.Name = newApplication.Name;
            existingApplication.Email = newApplication.Email;
            existingApplication.PhoneNumber = newApplication.PhoneNumber;
            existingApplication.Skillset = newApplication.Skillset;
            existingApplication.WorkExp = newApplication.WorkExp;
            existingApplication.JobInterest = newApplication.JobInterest;
            existingApplication.ReviewStatus = newApplication.ReviewStatus;

            OverWriteApplications(_applicationList);
        }

        public void DeleteApplication(int id)
        {
            var appToDelete = GetApplication(id);
            _applicationList.Remove(appToDelete);
            OverWriteApplications(_applicationList);
        }
    }
}
