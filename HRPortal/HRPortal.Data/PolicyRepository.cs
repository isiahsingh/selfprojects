﻿using Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml.XPath;

namespace HRPortal.Data
{
    public class PolicyRepository
    {
        private string _policyPath = HostingEnvironment.MapPath(@"\App_Data\Policies.xml");
        //private readonly string _policyPath = @"Test_App_Data\Policies.xml";
        public List<Policy> _policyList { get; set; }

        public PolicyRepository()
        {
            _policyList = Load();
        }

        private List<Policy> Load()
        {
            var policyList = (from a in XDocument.Load(_policyPath).Root?.Elements("Policy")
                                   select new Policy()
                                   {
                                       Id = (int)a.Element("id"),
                                       CategoryId= (int)a.Element("categoryId"),
                                       Title = (string)a.Element("title"),
                                       Content = (string)a.Element("content")
                                   }).ToList();

            return policyList;
        }

        public void CreatePolicy(Policy policyToAdd, int catId)
        {
            if (_policyList.Count == 0)
            {
                policyToAdd.Id = 1;
            }
            else
            {
                policyToAdd.Id = _policyList.Max(a => a.Id) + 1;
            }
            policyToAdd.CategoryId = catId;
            _policyList.Add(policyToAdd);
            OverWritePolicies(_policyList);

        }

        public void OverWritePolicies(List<Policy> policyList)
        {
            var policyFile = new XDocument();

            if (!File.Exists(_policyPath))
            {
                policyFile = new XDocument(new XElement("Policies"));
                policyFile.Save(_policyPath);
            }
            File.Delete(_policyPath);

            using (XmlWriter writer = XmlWriter.Create(_policyPath))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Policies");

                foreach(var policy in policyList)
                {
                    writer.WriteStartElement("Policy");

                    writer.WriteElementString("id", policy.Id.ToString());
                    writer.WriteElementString("categoryId", policy.CategoryId.ToString());
                    writer.WriteElementString("title", policy.Title);
                    writer.WriteElementString("content", policy.Content);

                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }

        public List<Policy> GetPoliciesForSpecificCategory(Category category)
        {
            return (_policyList.Select(p => p).Where(p => p.CategoryId == category.Id)).ToList();
        } 

        public Policy GetPolicy(int id)
        {
            return _policyList.FirstOrDefault(p => p.Id == id);
        }

        public void UpdatePolicy(Policy newPolicy)
        {
            var existingPolicy = _policyList.FirstOrDefault(a => a.Id == newPolicy.Id);

            if (existingPolicy != null)
            {
                existingPolicy.Id = newPolicy.Id;
                existingPolicy.CategoryId = newPolicy.CategoryId;
                existingPolicy.Title = newPolicy.Title;
                existingPolicy.Content = newPolicy.Content;
            }

            OverWritePolicies(_policyList);
        }

        public void DeleteAllPoliciesInCategory(int categoryId)
        {
            _policyList.RemoveAll(p => p.CategoryId == categoryId);
            OverWritePolicies(_policyList);
        }

        public void DeletePolicy(int id)
        {
            var policyToDelete = GetPolicy(id);
            _policyList.Remove(policyToDelete);
            OverWritePolicies(_policyList);
        }
    }
}
