﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Xml;
using System.Xml.Linq;
using Models;

namespace HRPortal.Data
{
    public class TimeSheetRepository
    {
        private string _timeSheetPath = HostingEnvironment.MapPath(@"\App_Data\TimeSheet.xml");
        //private string _timeSheetPath = @"Test_App_Data\TimeSheet.xml";

        public List<TimeSheet> _timeSheetList { get; set; }

        public TimeSheetRepository()
        {
            _timeSheetList = Load();
        }

        private List<TimeSheet> Load()
        {
            var timeSheetList = (from a in XDocument.Load(_timeSheetPath).Root?.Elements("TimeSheet")
                              select new TimeSheet()
                              {
                                  EmpId = (int)a.Element("empId"),
                                  Id = (int) a.Element("id"),
                                  Date = (DateTime) a.Element("date"),
                                  Hours = (int) a.Element("hours")
                              }).ToList();

            return timeSheetList;
        }

        public void CreateTimeSheet( TimeSheet timeSheet)
        {
            if (_timeSheetList.Count == 0)
            {
                timeSheet.Id = 1;
            }
            else
            {
                timeSheet.Id = _timeSheetList.Max(a => a.Id) + 1;
            }
            _timeSheetList.Add(timeSheet);
            OverWriteTimeSheets(_timeSheetList);
        }

        public void OverWriteTimeSheets(List<TimeSheet> timeSheetList)
        {
            var timeSheetFile = new XDocument();

            if (!File.Exists(_timeSheetPath))
            {
                timeSheetFile = new XDocument(new XElement("TimeSheets"));
                timeSheetFile.Save(_timeSheetPath);
            }
            File.Delete(_timeSheetPath);

            using (XmlWriter writer = XmlWriter.Create(_timeSheetPath))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("TimeSheets");

                foreach (var timesheet in timeSheetList)
                {
                    writer.WriteStartElement("TimeSheet");

                    writer.WriteElementString("empId", timesheet.EmpId.ToString());
                    writer.WriteElementString("id", timesheet.Id.ToString());
                    writer.WriteElementString("date", timesheet.Date.ToShortDateString());
                    writer.WriteElementString("hours", timesheet.Hours.ToString());

                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }

        public List<TimeSheet> GetTimeSheetForSpecificEmployee(Employee employee)
        {
            return (_timeSheetList.Select(t => t).Where(t => t.EmpId == employee.Id)).ToList();
        }

        public TimeSheet GetTimeSheetForSpecificDate(Employee employee, DateTime date)
        {
            return _timeSheetList.FirstOrDefault(t => t.EmpId == employee.Id && t.Date == date);
        }

        public void DeleteTimeSheet(int id)
        {
            var timeSheetToDelete = _timeSheetList.FirstOrDefault(t=> t.Id == id);
            _timeSheetList.Remove(timeSheetToDelete);
            OverWriteTimeSheets(_timeSheetList);
        }
    }
}
