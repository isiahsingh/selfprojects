﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessingGameV3
{
    class Program
    {
        static void Main(string[] args)
        {
            int theAnswer;
            int playerGuess;
            string playerInput;
            bool isNumberGuessed = false;
            string playerName;
            int countGuesses = 0;

            Random r = new Random();
            theAnswer = r.Next(1, 21);

            // get player name
            Console.WriteLine("Enter your name : ");
            playerName = Console.ReadLine();

            do
            {
                // get player input
                Console.WriteLine("{0}, enter your guess (1-20): ", playerName);
                playerInput = Console.ReadLine();
                if (playerInput.ToUpper() == "Q")
                {
                    break;
                }
                else
                {
                    //attempt to convert the string to a number
                    if (int.TryParse(playerInput, out playerGuess))
                    {
                        countGuesses++;
                        if (playerGuess >= 1 && playerGuess <= 20)
                        {
                            if (playerGuess == theAnswer)
                            {
                                if (countGuesses == 1)
                                {
                                    ConsoleColor normalColor = Console.ForegroundColor;
                                    Console.ForegroundColor = ConsoleColor.Green;
                                    Console.WriteLine("Nice! You got it in one try!");
                                    Console.ForegroundColor = normalColor;
                                }
                                Console.WriteLine("You Win!!! It took you {0} guesses!", countGuesses);
                                isNumberGuessed = true;
                            }
                            else
                            {
                                Console.WriteLine("Price is Wrong Bitch!");
                                if (playerGuess > theAnswer)
                                {
                                    Console.WriteLine("Too High!");
                                }
                                else
                                {
                                    Console.WriteLine("Too Low!");
                                }
                            }
                        }
                        else
                        {
                            Console.WriteLine("{0}, please enter a valid number between 1 and 20", playerName);
                        }
                    }
                    else
                    {
                        Console.WriteLine("That wasn't a valid number noob!");
                    }
                }
            } while (!isNumberGuessed);

            Console.WriteLine("Thanks for playing. Press any key to quit.");
            Console.ReadKey();
        }
    }
}
