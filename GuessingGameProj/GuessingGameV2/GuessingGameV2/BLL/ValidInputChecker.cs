﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessingGameV2.BLL
{
    public class ValidInputChecker
    {
        private bool isValid;
        public int validNumber { get; set; }
        int number;

        public bool parseNumber(string str)
        {
            isValid = int.TryParse(str, out number);
            validNumber = number;
            return isValid;
        }

        public bool isBetween(int num, string level)
        {
            if (level == "1" && (num >= 1 && num <= 50))
            {
                return true;
            }
            else if (level == "2" && (num >= 1 && num <= 100))
            {
                return true;
            }
            else if (level == "3" && (num >= 1 && num <= 1000))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool checkDifficultyOption()
        {
            if (number < 1 || number > 3)
            {
                return false;
            }
            return true;
        }

    }
}
