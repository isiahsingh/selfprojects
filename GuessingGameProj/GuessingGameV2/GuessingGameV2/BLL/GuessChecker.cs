﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessingGameV2.BLL
{
    public class GuessChecker
    {
        public int CheckGuess(int guess, int theNumber)
        {
            if (guess == theNumber)
            {
                return 2;
            }
            else if (guess > theNumber)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
