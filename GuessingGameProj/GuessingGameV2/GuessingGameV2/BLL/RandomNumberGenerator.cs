﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessingGameV2.BLL
{

    class RandomNumberGenerator
    {
        Random r = new Random();
        private int theNumber;

        public int getRandomNumber(string str)
        {
            if (str == "1")
            {
                theNumber = r.Next(1, 51);
                return theNumber;
            }
            else if(str == "2")
            {
                theNumber = r.Next(1, 101);
            }
            else
            {
                theNumber = r.Next(1, 1001);
            }
            return theNumber;
        }

    }
}
