﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using GuessingGameV2.BLL;
using GuessingGameV2.Models;

namespace GuessingGameV2.UI
{
    class Game
    {
        Player player = new Player();
        RandomNumberGenerator numberGenerator = new RandomNumberGenerator();
        ValidInputChecker checkInput = new ValidInputChecker();
        GuessChecker compareGuess = new GuessChecker();

        private List<int> guessList = new List<int>(); 
        private String level;
        private int secretNumber;
        private int playerGuess;

        public void Start()
        {
            int response;

            setUserName();
            level = pickDifficulty();
            secretNumber = numberGenerator.getRandomNumber(level);
            do
            {
                playerGuess = askUserForGuess();
                response = compareGuess.CheckGuess(playerGuess, secretNumber);
                displayResponse(response);
            } while (response != 2);

        }

        public void setUserName()
        {
            Console.WriteLine("Welcome to the guessing game.\n");
            Console.Write("Please enter your name : ");
            string name = Console.ReadLine();
            player.playerName = name;
        }

        public string pickDifficulty()
        {
            Console.WriteLine("\nChoose a difficulty : ");
            Console.WriteLine("1. Easy");
            Console.WriteLine("2. Medium");
            Console.WriteLine("3. Hard");
            string difficulty = Console.ReadLine();

            bool isValid = checkInput.parseNumber(difficulty);
            bool isBetween = checkInput.checkDifficultyOption();

            if (isValid == false || isBetween == false)
            {
                Console.WriteLine("Sorry, you did not enter a valid input. Press ENTER to try again.");
                Console.ReadLine();
                pickDifficulty();
            }
            return difficulty;
        }

        public int askUserForGuess()
        {
            bool isValid;
            bool isBetween;
            string guess;
            int parsedGuess;

            do
            {
                if (level == "1")
                {
                    Console.WriteLine("\nPlease enter a guess between 1 and 50\n");
                }
                else if (level == "2")
                {
                    Console.WriteLine("\nPlease enter a guess between 1 and 100\n");
                }
                else if (level == "3")
                {
                    Console.WriteLine("\nPlease enter a guess between 1 and 1000\n");
                }

                guess = Console.ReadLine();

                isValid = checkInput.parseNumber(guess);
                parsedGuess = checkInput.validNumber;
                isBetween = checkInput.isBetween(parsedGuess, level);

                if (isValid == false || isBetween == false)
                {
                    Console.WriteLine("Sorry, you either entered a number not in range or an invalid number. Press ENTER to try again.");
                }
                if (guessList.Contains(parsedGuess))
                {
                    Console.WriteLine("You already guessed that number! Pick another one.\n");
                }

            } while (isValid == false || isBetween == false || guessList.Contains(parsedGuess));

            guessList.Add(parsedGuess);
            return parsedGuess;
        }

        public void displayResponse(int comparedGuess)
        {
            if (comparedGuess == 2)
            {
                Console.WriteLine("\nCongrats! You guessed the number!\n");
                printGuesses();
                Console.ReadLine();
            }
            else if (comparedGuess == 1)
            {
                Console.WriteLine("Your guess is too high.\n");
            }
            else
            {
                Console.WriteLine("Your guess is too low.\n");
            }

        }

        public void printGuesses()
        {
            Console.WriteLine("It took you {0} guesses to win!", guessList.Count);
            Console.WriteLine("Your guesses were : ");
            foreach (int element in guessList)
            {
                Console.WriteLine(element);
            }
        }
    


    }
}
