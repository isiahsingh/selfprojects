﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RockPaperScissorsWithInterfaces.Implementations;

namespace RockPaperScissorsWithInterfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            string input;
            Game game;

            Console.WriteLine("(W)eighted mode, (R)andom mode, (T)wo Player mode: ");
            input = Console.ReadLine();

            if (input.ToUpper() == "W")
                game = new Game(new WeightedRockPicker());
            else if(input.ToUpper() == "R")
                game = new Game(new RandomChoicePicker());
            else 
                game = new Game(new TwoPlayerMode());

            do
            {
                Choice userChoice = GetUserChoice();
                var result = game.PlayRound(userChoice);
                ProcessResult(result);

                Console.Write("Press Q to quit: ");
                input = Console.ReadLine().ToUpper();
            } while (input.ToUpper() != "Q");
        }

        private static void ProcessResult(MatchResults result)
        {
            Console.WriteLine("You picked {0}, your opponent picked {1}", Enum.GetName(typeof(Choice), result.UserChoice),
                Enum.GetName(typeof(Choice), result.OpponentChoice));
            switch (result.Result)
            {
                case GameResult.Tie:
                    Console.WriteLine("You Tied!");
                    break;
                case GameResult.Loss:
                    Console.WriteLine("Player 2 wins!");
                    break;
                default:
                    Console.WriteLine("Player 1 Wins!");
                    break;
            }
        }

        private static Choice GetUserChoice()
        {
            Console.Clear();
            Console.Write("Player 1: Enter a choice (R)ock, (P)aper, (S)cissors: ");
            string input = Console.ReadLine().ToUpper();

            switch (input)
            {
                case "P":
                    return Choice.Paper;
                case "S":
                    return Choice.Scissors;
                default:
                    return Choice.Rock;
            }
        }
    }
}
