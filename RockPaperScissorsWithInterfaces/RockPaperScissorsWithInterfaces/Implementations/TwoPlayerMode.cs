﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RockPaperScissorsWithInterfaces.Interfaces;

namespace RockPaperScissorsWithInterfaces.Implementations
{
    public class TwoPlayerMode : IChoiceSelector
    {
        public Choice GetOpponentChoice()
        {
            Console.Clear();
            Console.Write("Player 2: Enter a choice (R)ock, (P)aper, (S)cissors: ");
            string input = Console.ReadLine().ToUpper();

            switch (input)
            {
                case "P":
                    return Choice.Paper;
                case "S":
                    return Choice.Scissors;
                default:
                    return Choice.Rock;
            }
        }
    }
}
