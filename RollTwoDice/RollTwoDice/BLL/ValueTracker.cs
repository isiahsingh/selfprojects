﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
//using RollTwoDice.Models;

namespace RollTwoDice.BLL
{
    
    public class ValueTracker
    {
        Random r = new Random();

        Dictionary<int, int> ValueCounter = new Dictionary<int, int>()
        {
            {2, 0},
            {3, 0},
            {4, 0},
            {5, 0},
            {6, 0},
            {7, 0},
            {8, 0},
            {9, 0},
            {10, 0},
            {11, 0},
            {12, 0}
        };
         
        public void BeginTracking()
        {
            for (int i = 0; i < 100; i++)
            {
                int sum = rollDie();
                UpdateValueCounter(sum);
            }
        }

        public int rollDie()
        {
            int num1 = r.Next(1, 7);
            int num2 = r.Next(1, 7);
            int add = num1 + num2;
            return add;
        }

        public void UpdateValueCounter(int sum)
        {
            int valueCount = ValueCounter[sum];
            valueCount++;
            ValueCounter[sum] = valueCount;
        }

        public Dictionary<int, int> GetCounter()
        {
            return ValueCounter;
        } 
    }
}
