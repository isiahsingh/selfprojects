﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RollTwoDice.UI
{
    public class Printer
    {
        public void DisplayResults(Dictionary<int, int> results)
        {
            foreach (KeyValuePair<int, int> element in results)
            {
                Console.WriteLine("{0} was rolled {1} times.", element.Key, element.Value);
            }
            Console.ReadLine();
        }
    }
}
