﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RollTwoDice.BLL;
//using RollTwoDice.Models;

namespace RollTwoDice.UI
{
    class Game
    {
        //Dice die1 = new Dice();
        //Dice die2 = new Dice();

        public void Start()
        {
            Console.WriteLine("This program will roll 2 die 100 times and will display how many times each number (2-12) was rolled.");
            Console.WriteLine("\n Press ENTER to begin ... ");
            Console.ReadLine();

            ValueTracker startTracking = new ValueTracker();
            startTracking.BeginTracking();

            Dictionary<int, int> counterResult = new Dictionary<int, int>();
            counterResult = startTracking.GetCounter();

            Printer printResult = new Printer();
            printResult.DisplayResults(counterResult);
        }
    }
}
