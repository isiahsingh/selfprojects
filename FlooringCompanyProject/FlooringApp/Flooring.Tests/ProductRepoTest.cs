﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Data.ProductRepos;
using Flooring.Models;
using NUnit.Framework;

namespace Flooring.Tests
{
    [TestFixture]
    public class ProductRepoTest
    {

        [Test]
        public void GetAllProductsTest()
        {
            IProductRepository repo = new ProductRepository();
            List<Product> getAllProducts = repo.GetAll();

            Assert.AreEqual(4, getAllProducts.Count);
        }
        [Test]
        public void GetProductByType()
        {
            IProductRepository repo = new ProductRepository();
            Product product = repo.GetByType("WOOD");

            Assert.AreEqual(5.15, product.CostPSF);
        }
        [Test]
        public void GetProductLaborCost()
        {
            IProductRepository repo = new ProductRepository();
            Product product = repo.GetByType("WOOD");

            Assert.AreEqual(4.75, product.LaborCostPSF);
        }
  
    }
}
