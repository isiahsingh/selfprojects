﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Data.StateTaxRepos;
using Flooring.Models;
using NUnit.Framework;

namespace Flooring.Tests
{
    [TestFixture]
    public class StateTaxRepoTest
    {
        [Test]
        public void GetByState()
        {
            var repo = new StateTaxRepository();
            var stateTax = repo.GetByState("OH");


            Assert.AreEqual(6.25, stateTax.TaxRate);

        }

        [Test]
        public void GetAll()
        {
            var repo = new StateTaxRepository();
            var stateTaxList = repo.GetAll();

            Assert.AreEqual(4, stateTaxList.Count);

        }

    }
}
