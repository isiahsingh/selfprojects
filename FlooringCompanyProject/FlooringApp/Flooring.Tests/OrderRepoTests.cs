﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Data;
using Flooring.Data.OrderRepos;
using Flooring.Models;
using NUnit.Framework;

namespace Flooring.Tests
{
    [TestFixture]
    public class OrderRepoTests
    {

        [Test]
        public void TestGetAllOrders()
        {
            var repo = new OrderRepository();
            var orderList = repo.GetAll(DateTime.Now);

            var expected = 1;
            var actual = orderList.Count;

            Assert.AreEqual(expected, actual);
        }


        [Test]
        public void TestCreateOrder()
        {
            var repo = new OrderRepository();
            var newOrderToAdd = new Order()
            {
                orderNumber = 1,
                customerName = "Jake",
                state = "FL",
                taxRate = 6,
                productType = "GLASS",
                area = 100,
                costPSF = 10,
                laborCostPSF = 8,
                materialCost = 100,
                laborCost = 80,
                tax = 100,
                total = 1000
            };

            repo.Create(newOrderToAdd, DateTime.Now);

            var orderList = repo.GetAll(DateTime.Now);

            var expected = 5;
            var actual = orderList.Count;

            Assert.AreEqual(expected, actual);
        }


        [Test]
        public void TestDeleteOrder()
        {
            var repo = new OrderRepository();

            string date = "02/08/2016";
            DateTime dateInput;
            bool isValidDate = DateTime.TryParse(date, out dateInput);

            repo.Delete(dateInput, 1);

            var orderList = repo.GetAll(DateTime.Now);

            var expected = 0;
            var actual = orderList.Count;

            Assert.AreEqual(expected, actual);
        }

        
    }
}
