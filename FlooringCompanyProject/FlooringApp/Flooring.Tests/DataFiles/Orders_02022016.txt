﻿OrderNumber,CustomerName,State,TaxRate,ProductType,Area,CostPSF,LaborCostPSF,MaterialCost,LaborCost,Tax,Total
1,Jake,Ohio,5.75,Tiles,100.00,10.00,8.00,1000.00,800.00,103.50,1903.50
2,Isiah,Florida,6.00,Wood,100.00,7.00,8.00,700.00,800.00,90.00,1590.00