﻿//using System;
//using System.Collections.Generic;
//using Flooring.BLL;
//using Flooring.Data.Factory;
//using Flooring.Data.OrderRepos;
//using Flooring.Models;
//using NUnit.Framework;

//namespace Flooring.Tests
//{
//    [TestFixture]
//    class OrderManagerBLLTests
//    {
//        [Test]
//        public void TestGetOrdersByDateReturnsResponseOfListOfOrders()
//        {
//            var manager = new OrderManager();
//            var response = new Response<List<Order>>();

//            response = manager.GetOrdersByDate(DateTime.Now);

//            var actual = response.Success;

//            Assert.IsTrue(actual);
//        }

//        [Test]
//        public void GenerateOrderNumberReturns()
//        {
//            DateTime date = DateTime.Now;
//            IDataRepository repo = new OrderRepository();
//            var manager = new OrderManager();
//            int actual = manager.GenerateOrderNumber(repo, date);

//            Assert.AreEqual(1, actual);
//        }

//        [Test]
//        public void AddOrderToFileReturnsResponseWithOrder()
//        {
//            var manager = new OrderManager();
//            var response = new Response<Order>();

//            List<string> stringOrder = new List<string>() {"Jake", "FL", "GLASS", "100"};

//            response = manager.AddOrder(stringOrder, DateTime.Now);

//            var actual = response.Success;

//            Assert.IsTrue(actual);
//        }

//        [Test]
//        public void AddOrderWithWrongState()
//        {
//            var manager = new OrderManager();
//            var response = new Response<Order>();

//            List<string> stringOrder = new List<string>() { "Jake", "FL", "Tile", "100" };

//            response = manager.AddOrder(stringOrder, DateTime.Now);

//            var actual = response.Success;

//            Assert.IsFalse(actual);
//        }

//        [Test]
//        public void AddOrderWithWrongProduct()
//        {
//            var manager = new OrderManager();
//            var response = new Response<Order>();

//            List<string> stringOrder = new List<string>() { "Jake", "OH", "Dirt", "100" };

//            response = manager.AddOrder(stringOrder, DateTime.Now);

//            var actual = response.Success;

//            Assert.IsFalse(actual);
//        }

//        [Test]
//        public void DeletingAnOrder()
//        {

//            Response<int> response = new Response<int>();

//            var manager = new OrderManager();

//            response = manager.DeleteOrder(DateTime.Now, 1);

//            var actual = response.Success;

//            Assert.IsTrue(actual);
//        }

//        [Test]
//        public void UpdatingAnOrder()
//        {

//            Response<Order> response = new Response<Order>();
//            List<string> stringOrder = new List<string>() { "Jake", "CA", "MARBLE", "100" };
//            var manager = new OrderManager();

//            response = manager.EditOrder(stringOrder,DateTime.Now, 1);

//            var actual = response.Success;

//            Assert.IsTrue(actual);
//        }

//        [Test]
//        public void GeneratingNextOrderNumber()
//        {

//            int response;
//            List<string> stringOrder = new List<string>() { "Jake", "CA", "MARBLE", "100" };
//            var manager = new OrderManager();
//            IDataRepository repo = new OrderRepository();
//            response = manager.GenerateOrderNumber(repo, DateTime.Now);

//            var actual = response;

//            Assert.AreEqual(1, actual);
//        }

//        [Test]
//        public void CheckState()
//        {

//            bool response = true;
//            var manager = new OrderManager();

//            response = manager.CheckState("CA");

//            var actual = response;

//            Assert.IsTrue(actual);
//        }

//        [Test]
//        public void CheckProduct()
//        {

//            bool response = true;
//            var manager = new OrderManager();

//            response = manager.CheckProductType("GLASS");

//            var actual = response;

//            Assert.IsTrue(actual);
//        }

//        [Test]
//        public void GetSingleOrder()
//        {
//            Response<Order> response = new Response<Order>();
//            var manager = new OrderManager();
//            IDataRepository repo = new OrderRepository();
//            response = manager.GetSingleOrder(DateTime.Now,1);

//            var actual = response.Success;

//            Assert.IsTrue(actual);
//        }

//    }
//}
