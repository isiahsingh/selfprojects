﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Flooring.Data.Factory;
using Flooring.Models;
using Flooring.Models.Interfaces;
using Ninject;
using Ninject.Modules;

namespace Flooring.BLL
{
    public class OrderManager
    {
        private readonly IDataRepository _orderRepo;
        private readonly IErrorRepository _errorRepo;
        private readonly IProductRepository _productRepo;
        private readonly IStateTaxRepository _stateTaxRepo;

        public OrderManager()
        {
            //Ninject
            NinjectModule module = new Bindings();
            IKernel kernel = new StandardKernel(module);

            kernel.Load(Assembly.GetExecutingAssembly());
            _orderRepo = kernel.Get<IDataRepository>();
            _errorRepo = kernel.Get<IErrorRepository>();
            _productRepo = kernel.Get<IProductRepository>();
            _stateTaxRepo = kernel.Get<IStateTaxRepository>();
        }

        public Response<List<Order>> GetOrdersByDate(DateTime date)
        {
            var response = new Response<List<Order>>();
            try
            {
                response.Data = _orderRepo.GetAll(date);
                if (response.Data.Count == 0)
                {
                    response.Success = false;
                    response.Message = "There are no orders in this file.";
                    _errorRepo.AddError("Error occured, trying to lookup all orders for a date " +
                                       $"that does not exist. Date that failed was : {date}");
                }
                else
                {
                    response.Success = true;
                }
            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "An error has occured. Please try again later.";
                _errorRepo.AddError("An error has occured in retrieving all orders for a given date.");
            }
            return response;
        }

      
        public Response<Order> AddOrder(List<string> orderInfo, DateTime date)
        {
            var response = new Response<Order>();  
            try
            {
                if (!(CheckState(orderInfo[1])))
                {
                    response.Success = false;
                    response.Message = "You did not enter a valid state.";
                    _errorRepo.AddError("User did not enter a valid state.");
                    return response;
                }
                if (!(CheckProductType(orderInfo[2])))
                {
                    response.Success = false;
                    response.Message = "You did not enter a valid Product Type.";
                    _errorRepo.AddError("User did not enter a valid Product Type.");
                    return response;
                }
                Order addedOrder = SetUpOrder(orderInfo, date);
                _orderRepo.Create(addedOrder, date);
                if (_orderRepo.GetAll(date).Any(o => o.orderNumber == addedOrder.orderNumber))
                {
                    response.Success = true;
                    response.Message = "Your order has been placed.";
                    response.Data = addedOrder;
                }
                else
                {
                    response.Success = false;
                    response.Message = "There was an error placing your order.";
                    _errorRepo.AddError("User has attempted to place an order but error has occured and it was not placed in the .txt file.");
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = "An error has occured. Please try again later.";
                _errorRepo.AddError("User has attempted to place an order, but an error has occured. " +
                                   $"\n\tError Message : {ex.Message}");
            }
            return response;
        }

        public Response<int> DeleteOrder(DateTime date, int orderNumber)
        {
            Response<int> response = new Response<int>();
            try
            {
                _orderRepo.Delete(date, orderNumber);
                response.Success = true;
                response.Data = orderNumber;
                response.Message = "Order Has Been Deleted ";
            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "This Order Does Not Exist.";
                _errorRepo.AddError($"User has attempted to delete order number {orderNumber}. " +
                                   "However that order does not exist.");
            }
            return response;
        }

        public Response<Order> GetSingleOrder(DateTime date, int orderNumber)
        {
            var response = new Response<Order>();
            try
            {
                Order order = _orderRepo.GetOrderByID(orderNumber, date);
                if (order == null)
                {
                    response.Success = false;
                    response.Message = "This Order Was Not Found.";
                    _errorRepo.AddError("Error occured in GetSingleOrder method in" +
                                       $" OrderManager in BLL. Error occured with order number : {orderNumber}");
                }
                else
                {
                    response.Success = true;
                    response.Data = order;
                }
            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "Single order not found.";
                _errorRepo.AddError("Error occured in GetSingleOrder method in OrderManager in BLL." +
                                   $"Error occured with order number : {orderNumber}");
            }
            return response;
        }

        public Response<Order> EditOrder(List<string> newOrderInfo, DateTime date, int orderNumber)
        {
            var response = new Response<Order>();
            try
            {
                Order editedOrder = SetUpOrder(newOrderInfo, date);
                editedOrder.orderNumber = orderNumber;
                List<Order> getAllOrders = _orderRepo.GetAll(date);
                foreach (var order in getAllOrders)
                {
                    if (order.orderNumber == orderNumber)
                    {
                        order.customerName = editedOrder.customerName;
                        order.state = editedOrder.state;
                        order.taxRate = editedOrder.taxRate;
                        order.productType = editedOrder.productType;
                        order.area = editedOrder.area;
                        order.costPSF = editedOrder.costPSF;
                        order.laborCostPSF = editedOrder.laborCostPSF;
                        order.materialCost = editedOrder.materialCost;
                        order.laborCost = editedOrder.laborCost;
                        order.tax = editedOrder.tax;
                        order.total = editedOrder.total;
                    }
                }
                _orderRepo.Update(getAllOrders, date);
                response.Success = true;
                response.Data = editedOrder;
            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "An error has occured while editing an order. Please try again later.";
                _errorRepo.AddError("An error has occured in EditOrder method in BLL. Error occured with " +
                                   $"DateTime : {date} and OrderNumber : {orderNumber}");
            }
            return response;
        }
    

        public Order SetUpOrder(List<string> orderInfo, DateTime date)
        {
            var orderToAdd = new Order
            {
                orderNumber = GenerateOrderNumber(_orderRepo, date),
                customerName = orderInfo[0],
                state = orderInfo[1],
                taxRate = _stateTaxRepo.GetByState(orderInfo[1]).TaxRate,
                productType = orderInfo[2],
                area = int.Parse(orderInfo[3]),
                costPSF = _productRepo.GetByType(orderInfo[2]).CostPSF,
                laborCostPSF = _productRepo.GetByType(orderInfo[2]).LaborCostPSF
            };
            orderToAdd.materialCost = orderToAdd.area * orderToAdd.costPSF;
            orderToAdd.laborCost = orderToAdd.area * orderToAdd.laborCostPSF;
            orderToAdd.tax = (orderToAdd.materialCost + orderToAdd.laborCost) * (orderToAdd.taxRate / 100);
            orderToAdd.total = orderToAdd.materialCost + orderToAdd.laborCost + orderToAdd.tax;

            return orderToAdd;
        } 

        public int GenerateOrderNumber(IDataRepository repo, DateTime date)
        {
            if (repo.GetAll(date).Count == 0)
                return 1;
            else
                return repo.GetAll(date).Last().orderNumber + 1;
        }

        public bool CheckState(string stateInput)
        {
            bool isValid = false;
            List<string> stateTaxList = GetAllStates();
            foreach (var st in stateTaxList)
            {
                if (st.ToUpper() == stateInput)
                {
                    isValid = true;
                }
            }
            return isValid;
        }

        public bool CheckProductType(string productType)
        {
            List<string> productTypeList = GetAllProductTypes();
            bool isValid = false;
            foreach (var prod in productTypeList)
            {
                if (prod.ToUpper() == productType)
                {
                    isValid = true;
                }
            }
            return isValid;
        }

        public List<string> GetAllProductTypes()
        {
            List<Product> productTypeList = _productRepo.GetAll();
            List<string> types = (productTypeList.Select(p => p.Type)).ToList();
            return types;
        }

        public List<string> GetAllStates()
        {
            List<StateTax> stateTypeList = _stateTaxRepo.GetAll();
            List<string> types = (stateTypeList.Select(p => p.ST)).ToList();
            return types;
        }
    }
}


