﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Data.AdminRepos;
using Flooring.Models;

namespace Flooring.BLL
{
    public class AdminManager
    {
        public Response<string> checkAdminLogin(string[] loginInfo)
        {
            var adminList = new List<Admin>();
            var response = new Response<string>();
            AdminRepository repo = new AdminRepository();
            adminList = repo.GetAll();
            foreach (var admin in adminList)
            {
                if (admin.UserName == loginInfo[0] && admin.Password == loginInfo[1])
                {
                    response.Success = true;
                    response.Data = admin.UserName;
                    return response;
                }
            }
           
            response.Success = false;
            response.Data = loginInfo[0];
            return response;
        }
    }
}
