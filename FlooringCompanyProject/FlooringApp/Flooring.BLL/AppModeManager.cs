﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Data.Factory;
using Flooring.Models;
using Flooring.Models.Interfaces;

namespace Flooring.BLL
{
    public class AppModeManager
    {
        public AppModeManager()
        {
            AppModeFactory.CreateDataRepository();    
        }

        public Response<string> ChangeConfigMode(string mode)
        {
            var response = new Response<string>();
            IErrorRepository errorRepo = AppModeFactory.GetErrorRepo();
            try
            {
                AppModeFactory.SetConfigFileMode(mode);
                if (AppModeFactory.GetConfigFileMode() != mode)
                {
                    response.Success = false;
                    response.Message = "There was an error trying to change the config mode.";
                    response.Data = AppModeFactory.GetConfigFileMode();
                    errorRepo.AddError($"Error trying to change config mode to : {mode}");
                }
                else
                {
                    response.Success = true;
                    response.Message = $"Successfully changed the mode.";
                    response.Data = AppModeFactory.GetConfigFileMode();
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = "An error occured trying to change the config mode.";
                errorRepo.AddError($"There was an error trying to change config mode. Error : {ex.Message}");
            }
           
            return response;
        }

        public string GetCurrentMode()
        {
            string currentMode = AppModeFactory.GetConfigFileMode();
            return currentMode;
        }
    }
}
