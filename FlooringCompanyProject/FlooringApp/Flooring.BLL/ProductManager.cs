﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Data.ErrorRepos;
using Flooring.Data.Factory;
using Flooring.Models;
using Flooring.Models.Interfaces;

namespace Flooring.BLL
{
    public class ProductManager
    {
        public ProductManager()
        {
            AppModeFactory.CreateDataRepository();    
        }

        public Response<Product> AddProduct(string[] productInfo)
        {
            IErrorRepository errorRepo = AppModeFactory.GetErrorRepo();
            IProductRepository repo = AppModeFactory.GetProductRepo();
            Response<Product> response = new Response<Product>();
            Product newProduct = new Product()
            {
                Type = productInfo[0],
                CostPSF = decimal.Parse(productInfo[1]),
                LaborCostPSF = decimal.Parse(productInfo[2])
            };

            try
            {
                repo.AddProduct(newProduct);
                if (repo.GetAll().Any(p => p.Type == newProduct.Type))
                {
                    response.Success = true;
                    response.Message = "You have successfully added a product.";
                    response.Data = newProduct;
                }
                else
                {
                    response.Success = false;
                    response.Message = "There was an error adding a product.";
                    errorRepo.AddError("Error adding product as an Admin.");
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                errorRepo.AddError($"There was an error attempting to add product of type {productInfo[0]}. " +
                                   $"Error Message : {ex.Message}");
            }
            return response;
        } 
    }
}
