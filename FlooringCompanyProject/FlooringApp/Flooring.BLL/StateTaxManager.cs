﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Data.Factory;
using Flooring.Models;
using Flooring.Models.Interfaces;

namespace Flooring.BLL
{
    public class StateTaxManager
    {
        public StateTaxManager()
        {
            AppModeFactory.CreateDataRepository();    
        }

        public Response<StateTax> AddStateTax(string[] stateTaxInfo)
        {
            IErrorRepository errorRepo = AppModeFactory.GetErrorRepo();
            IStateTaxRepository repo = AppModeFactory.GetOrderStateTaxRepo();
            var response = new Response<StateTax>();
            StateTax newStateTax = new StateTax()
            {
                ST = stateTaxInfo[0],
                State = stateTaxInfo[1],
                TaxRate = decimal.Parse(stateTaxInfo[2])
            };

            try
            {
                repo.AddStateTax(newStateTax);
                if (repo.GetAll().Any(st => st.ST == newStateTax.ST))
                {
                    response.Success = true;
                    response.Message = "You have successfully added a state.";
                    response.Data = newStateTax;
                }
                else
                {
                    response.Success = false;
                    response.Message = "There was an error adding a state.";
                    errorRepo.AddError("Error adding state as an Admin.");
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                errorRepo.AddError($"There was an error attempting to add state of type {stateTaxInfo[0]}. " +
                                   $"Error Message : {ex.Message}");
            }
            return response;
        }
    }
}
