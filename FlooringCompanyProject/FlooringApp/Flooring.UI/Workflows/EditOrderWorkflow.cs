﻿using System;
using System.Collections.Generic;
using Flooring.BLL;
using Flooring.Models;
using Flooring.Models.Interfaces;
using Flooring.UI.UserPrompt;

namespace Flooring.UI.Workflows
{
    internal class EditOrderWorkflow
    {
        //private readonly IDataRepository _orderRepo;
        //private readonly IErrorRepository _errorRepo;
        //private readonly IProductRepository _productRepo;
        //private readonly IStateTaxRepository _stateTaxRepo;

        //internal EditOrderWorkflow(IDataRepository orderRepo, IErrorRepository errorRepo, IProductRepository productRepo, IStateTaxRepository stateTaxRepo)
        //{
        //    _orderRepo = orderRepo;
        //    _errorRepo = errorRepo;
        //    _productRepo = productRepo;
        //    _stateTaxRepo = stateTaxRepo;
        //}

        internal void Execute()
        {
            Order existingOrder = new Order();
            var response = new Response<Order>();
            OrderManager manager = new OrderManager();
            Console.WriteLine("\n\n\t\t\t               EDIT ORDER");
            Console.WriteLine("\t\t\t==========================================");
            int orderNumber;
            DateTime date;
            do
            {
                date = GetUserInfo.GetDate();
                orderNumber = GetUserInfo.GetOrderNumber();
                existingOrder = GetExistingOrder(date, orderNumber);
            } while (existingOrder == null);
            List<string> newOrder = PromptUserForEdit(existingOrder);
            
            response = manager.EditOrder(newOrder, date, orderNumber);
            DisplayEditedOrder(response);
            Console.ReadLine();
        }

        internal void DisplayEditedOrder(Response<Order> response)
        {
            if (response.Success)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\n\t\t\tUpdated Order Information");
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("\t\t\t============================");
                Console.WriteLine("\t\t\tOrder Number  : {0}", response.Data.orderNumber);
                Console.WriteLine("\t\t\tCustomer Name : {0}",response.Data.customerName);
                Console.WriteLine("\t\t\tState         : {0}", response.Data.state);
                Console.WriteLine("\t\t\tProduct Type  : {0}", response.Data.productType);
                Console.WriteLine("\t\t\tTax           : {0:C}", response.Data.tax);
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("\t\t\tTotal         : {0:C}", response.Data.total);
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            else
            {
                Console.WriteLine(response.Message);
            }
        }

        internal List<string> PromptUserForEdit(Order existingOrder)
        {
            var manager = new OrderManager();
            List<string> stateList = manager.GetAllStates();
            List<string> productTypeList = manager.GetAllProductTypes();
            List<string> newOrder = new List<string>();
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\n\n\t\t\tCurrent name is {0}.", existingOrder.customerName);
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("\n\tPress ENTER to keep existing name, or enter a new name : ");
            string change = Console.ReadLine();
            if (change == "")
                newOrder.Add(existingOrder.customerName);
            else
            {
                change = GetUserInfo.GetUserName(change);
                newOrder.Add(change);
            }
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\n\n\t\t\tCurrent state is {0}.", existingOrder.state);
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("\n\tPress ENTER to keep existing state, or enter a new state : ");
            change = Console.ReadLine();
            if (change == "")
                newOrder.Add(existingOrder.state);
            else
            {
                change = GetUserInfo.GetUserState(stateList, string.Join(",", stateList), change);
                newOrder.Add(change);
            }
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\n\n\t\t\tCurrent product type is {0}.", existingOrder.productType);
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("\n\tPress ENTER to keep existing product type, or enter a new product type.");
            change = Console.ReadLine();
            if (change == "")
                newOrder.Add(existingOrder.productType);
            else
            {
                change = GetUserInfo.GetProductType(productTypeList, string.Join(",", productTypeList), change);
                newOrder.Add(change);
            }
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\n\n\t\t\tCurrent area is {0}.", existingOrder.area);
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("\n\tPress ENTER to keep existing area, or enter a new area : ");
            change = Console.ReadLine();
            if (change == "")
                newOrder.Add(existingOrder.area.ToString());
            else
            {
                change = GetUserInfo.GetArea(change);
                newOrder.Add(change);
            }
            return newOrder;
        }

        internal Order GetExistingOrder(DateTime date, int orderNumber)
        {
            var manager = new OrderManager();
            var response = new Response<Order>();
            response = manager.GetSingleOrder(date, orderNumber);
            if (response.Success)
            {
                return response.Data;
            }
            else
            {
                Console.WriteLine(response.Message);
                return null;
            }
        }      
    }
}
