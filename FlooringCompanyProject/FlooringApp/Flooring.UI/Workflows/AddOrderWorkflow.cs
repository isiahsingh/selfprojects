﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Flooring.BLL;
using Flooring.Models;
using Flooring.Models.Interfaces;
using Flooring.UI.UserPrompt;
using Ninject;

namespace Flooring.UI.Workflows
{
    internal class AddOrderWorkflow
    {
       
        internal void Execute()
        {
            List<string> orderInfo = GetInfo();
            Response<Order> response = AddOrder(orderInfo);
            DisplayResponse(response);
        }

        internal List<string> GetInfo()
        {
            string input;
            var manager = new OrderManager();
            List<string> stateList = manager.GetAllStates();
            List<string> productTypeList = manager.GetAllProductTypes();
            string states = ConvertListToString(stateList);
            string productTypes = ConvertListToString(productTypeList);
            var orderInfo = new List<string>();
            Console.Write("\n\t\t\tEnter Your Name : ");
            input = Console.ReadLine();
            orderInfo.Add(GetUserInfo.GetUserName(input));
            Console.Write("\n\t\t\tEnter Your State ({0}) : ", states);
            input = Console.ReadLine();
            orderInfo.Add(GetUserInfo.GetUserState(stateList, states, input));
            Console.Write("\n\t\t\tEnter The Product Type ({0}) : ", productTypes);
            input = Console.ReadLine();
            orderInfo.Add(GetUserInfo.GetProductType(productTypeList, productTypes, input));
            Console.Write("\n\t\t\tEnter The Area (In Square Feet) : ");
            input = Console.ReadLine();
            orderInfo.Add(GetUserInfo.GetArea(input));

            return orderInfo;
        }

        internal Response<Order> AddOrder(List<string> orderInfo)
        {
            var manager = new OrderManager();
            Response<Order> response = manager.AddOrder(orderInfo, DateTime.Now);
            return response;
        }

        internal void DisplayResponse(Response<Order> response)
        {
            if (response.Success)
            {
                Console.WriteLine("\n\t\t\t{0}",response.Message);
                Console.WriteLine("\t\t\tOrder Number : {0}", response.Data.orderNumber);
                Console.WriteLine("\t\t\tArea         : {0}", response.Data.area);
                Console.WriteLine("\t\t\tCustomer     : {0}", response.Data.customerName);
                Console.WriteLine("\t\t\tProduct Type : {0}", response.Data.productType);
                Console.WriteLine("\t\t\tTax          : {0:C}", response.Data.tax);
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("\t\t\tTotal        : {0:C}", response.Data.total);
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            else
            {
                Console.WriteLine(response.Message);
            }
            Console.ReadLine();
        }

        internal string ConvertListToString(List<string> stringList)
        {
            string combinedList = string.Join(",", stringList);
            return combinedList;
        }
    }
}
