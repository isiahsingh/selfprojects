﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.BLL;
using Flooring.Models;
using Flooring.Models.Interfaces;
using Flooring.UI.UserPrompt;

namespace Flooring.UI.Workflows
{
    internal class DisplayOrdersWorkflow
    {
        private List<Order> _orders;
      
        internal void Execute()
        {
            var manager = new OrderManager();
            var dateInput = GetUserInfo.GetDate();

            var response = manager.GetOrdersByDate(dateInput);

            if (response.Success)
            {
                _orders = response.Data;
                PrintOrderDetails();
            }
            else
            {
                Console.WriteLine("\t\t\tOrder Date Not Found. Press ENTER to try again.");
                Console.ReadLine();
            }
        }

        internal void PrintOrderDetails()
        {
            Console.WriteLine("\n\t\t\tOrder Information");
            foreach (var ord in _orders)
            {
                Console.WriteLine("\n\t\t\t================================");
                Console.WriteLine("\n\t\t\tOrder Number  : {0}", ord.orderNumber);
                Console.WriteLine("\t\t\t\tCustomer Name : {0}", ord.customerName);
                Console.WriteLine("\t\t\t\tState         : {0}", ord.state);
                Console.WriteLine("\t\t\t\tTax           : {0:C}", ord.tax);
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("\t\t\t\tTotal         : {0:C}", ord.total);
                Console.ForegroundColor = ConsoleColor.Gray;
            }

            Console.WriteLine("\n\t\t\tPress Enter to continue.");
            Console.ReadLine();
        }
    }
}
