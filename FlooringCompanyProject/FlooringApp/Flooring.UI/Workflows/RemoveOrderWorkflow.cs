﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.BLL;
using Flooring.Models;
using Flooring.Models.Interfaces;
using Flooring.UI.UserPrompt;

namespace Flooring.UI.Workflows
{
    internal class RemoveOrderWorkflow
    {
 
        internal void Execute()
        {
            var response = new Response<Order>();
            DateTime date = GetUserInfo.GetDate();
            int orderNumber =  GetUserInfo.GetOrderNumber();

            OrderManager manager = new OrderManager();
            response =  manager.GetSingleOrder(date, orderNumber);
            if (response.Success)
            {
                string removeInput = DisplayOrder(response.Data);
                if (removeInput.ToUpper() == "Y")
                {
                    DeleteOrder(date, orderNumber);
                }
                else
                {
                    Console.WriteLine("\n\t\t\tOrder Not Deleted");
                }

            }
            else
            {
                Console.WriteLine("\n\t\t\tOrder Not In System!");
            }
            Console.ReadLine();
        }


        internal string DisplayOrder(Order order)
        {
            Console.WriteLine("\t\t\tOrder Number  : {0}", order.orderNumber);
            Console.WriteLine("\t\t\tCustomer Name : {0}", order.customerName);
            Console.WriteLine("\t\t\tState         : {0}", order.state);
            Console.WriteLine("\t\t\tProduct Type  : {0}", order.productType);
            Console.WriteLine("\t\t\tTax           : {0:C}", order.tax);
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\t\t\tTotal         : {0:C}", order.total);

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\n\t\t\tAre you Sure You Want To Delete Your Order?");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("\t\t\t(Y)es/(N)o : ");
            string userInput = Console.ReadLine();

            return userInput;
        }

        internal void DeleteOrder(DateTime date, int orderNumber)
        {
            var response = new Response<int>();
            OrderManager manager = new OrderManager();
            response = manager.DeleteOrder(date, orderNumber);
            if (response.Success)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("\t\t\tYou Have Deleted Order {0}", response.Data);
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\t\t\t{0}",response.Message);
                Console.ForegroundColor = ConsoleColor.Gray;
            }
        }
    }
}
