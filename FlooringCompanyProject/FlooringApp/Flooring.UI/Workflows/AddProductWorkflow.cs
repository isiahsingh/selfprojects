﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.BLL;
using Flooring.Models;
using Flooring.UI.Utilities;

namespace Flooring.UI.Workflows
{
    public class AddProductWorkflow
    {
        public void Execute()
        {
            string[] productInfo = GetInfo();
            Response<Product> response = AddProduct(productInfo);
            DisplayResponse(response);
        }

        internal string[] GetInfo()
        {
            string[] addProductInfo = new string[3];
            var manager = new ProductManager();
            addProductInfo[0] = GetProductInfo.GetProductType();
            addProductInfo[1] = GetProductInfo.GetCostPSF();
            addProductInfo[2] = GetProductInfo.GetLaborCPSF();
            
            return addProductInfo;
        }

        internal Response<Product> AddProduct(string[] productInfo)
        {
            var manager = new ProductManager();
            Response<Product> response = manager.AddProduct(productInfo);
            return response;
        }

        internal void DisplayResponse(Response<Product> response)
        {
            if (response.Success)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("\n\t\t\t{0}",response.Message);
                Console.WriteLine("\t\t\tProduct Type               : {0}",response.Data.Type);
                Console.WriteLine("\t\t\tCost Per Square Foot       : {0:C}", response.Data.CostPSF);
                Console.WriteLine("\t\t\tLabor Cost Per Square Foot : {0:C}", response.Data.LaborCostPSF);
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(response.Message);
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            Console.ReadLine();
        }
    }
}
