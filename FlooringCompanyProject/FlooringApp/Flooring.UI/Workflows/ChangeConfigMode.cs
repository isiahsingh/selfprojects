﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.BLL;
using Flooring.UI.UserPrompt;

namespace Flooring.UI.Workflows
{
    internal class ChangeConfigMode
    {
        internal void Execute()
        {
            GetInfo();
            Console.ReadLine();
        }

        internal void GetInfo()
        {
            AppModeManager manager = new AppModeManager();
            Console.WriteLine("\n\t\t\tThe current mode of the app is {0}", manager.GetCurrentMode());
            Console.Write("\n\t\t\tWould you like to change the mode? (Y/N) : ");
            string answer = Console.ReadLine();
            if (answer.ToUpper() == "Y")
            {
                if (manager.GetCurrentMode() == "Test")
                {
                    manager.ChangeConfigMode("Production");
                    Console.WriteLine("\n\t\t\tThe config mode has changed to Production.");
                }
                else
                {
                    manager.ChangeConfigMode("Test");
                    Console.WriteLine("\n\t\t\tThe config mode has changed to Test.");
                }
            }
            else if (answer.ToUpper() == "N")
            {
                Console.WriteLine("\t\t\tThe config mode will not change.");
                return;
            }
            else
            {
                Console.WriteLine("\t\t\tYou did not enter a valid input. The mode will remain the same.");
                Console.WriteLine("\t\t\tPress ENTER to continue.");
            }
        }
    }
}
