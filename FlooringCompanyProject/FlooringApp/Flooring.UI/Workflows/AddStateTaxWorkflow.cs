﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.BLL;
using Flooring.Models;
using Flooring.UI.Utilities;

namespace Flooring.UI.Workflows
{
    public class AddStateTaxWorkflow
    {
        public void Execute()
        {
            string[] stateTaxInfo = GetInfo();
            Response<StateTax> response = AddStateTax(stateTaxInfo);
            DisplayResponse(response);
        }
       
        internal string[] GetInfo()
        {
            string[] addStateTaxInfo = new string[3];
            var manager = new StateTaxManager();
            addStateTaxInfo[0] = GetStateTaxInfo.GetStateAbbr();
            addStateTaxInfo[1] = GetStateTaxInfo.GetStateName();
            addStateTaxInfo[2] = GetStateTaxInfo.GetTaxRate();

            return addStateTaxInfo;
        }

        internal Response<StateTax> AddStateTax(string[] stateTaxInfo)
        {
            var manager = new StateTaxManager();
            Response<StateTax> response = manager.AddStateTax(stateTaxInfo);
            return response;
        }

        internal void DisplayResponse(Response<StateTax> response)
        {
            if (response.Success)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("\n\t\t\t{0}",response.Message);
                Console.WriteLine("\t\t\tState Abbr : {0}", response.Data.ST);
                Console.WriteLine("\t\t\tState      : {0}", response.Data.State);
                Console.WriteLine("\t\t\tTax Rate   : {0:C}", response.Data.TaxRate);
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(response.Message);
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            Console.ReadLine();
        }
    }
}
