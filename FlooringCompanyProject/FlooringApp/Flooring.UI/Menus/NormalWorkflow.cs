﻿using System;
using System.Reflection;
using Flooring.Models;
using Flooring.Models.Interfaces;
using Flooring.UI.Workflows;
using Ninject;

namespace Flooring.UI.Menus
{
    public class NormalWorkflow
    {
        //private readonly IDataRepository _orderRepo;
        //private readonly IErrorRepository _errorRepo;
        //private readonly IProductRepository _productRepo;
        //private readonly IStateTaxRepository _stateTaxRepo;

        //public NormalWorkflow()
        //{
        //    //Ninject
        //    var kernel = new StandardKernel();
        //    kernel.Load(Assembly.GetExecutingAssembly());
        //    _orderRepo = kernel.Get<IDataRepository>();
        //    _errorRepo = kernel.Get<IErrorRepository>();
        //    _productRepo = kernel.Get<IProductRepository>();
        //    _stateTaxRepo = kernel.Get<IStateTaxRepository>();
        //}

        public void Execute()
        {
            do
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\n\t\t\t\tWELCOME TO ISIAH JAKE FLOORING");
                Console.ForegroundColor = ConsoleColor.Green;
                string options = @"    
                      ╔═════════╦════════╦════════╦════════╦════════╗
                      ║ Display ║   Add  ║  Edit  ║ Remove ║  Quit  ║
                      ║   [1]   ║   [2]  ║   [3]  ║   [4]  ║   [5]  ║
                      ╚═════════╩════════╩════════╩════════╩════════╝";
               
                Console.WriteLine(options);
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.Write("\n\n\t\tPlease Enter Your Choice (1-5): ");
                string input = Console.ReadLine();

                if (input == "")
                    continue;
                if (input == "5")
                    break;

                ProcessInput(input);
            } while (true);
        }

        public void ProcessInput(string input)
        {
            switch (input)
            {
                case "1":
                    var display = new DisplayOrdersWorkflow();
                    display.Execute();
                    break;
                case "2":
                    var add = new AddOrderWorkflow();
                    add.Execute();
                    break;
                case "3":
                    var edit = new EditOrderWorkflow();
                    edit.Execute();
                    break;
                case "4":
                    var remove = new RemoveOrderWorkflow();
                    remove.Execute();
                    break;
                default:
                    Console.WriteLine("\t\tYou entered an invalid input. Press ENTER to continue");
                    Console.ReadLine();
                    break;
            }
        }
    }
}
