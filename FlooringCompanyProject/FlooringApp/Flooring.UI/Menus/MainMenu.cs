﻿using System;
using Flooring.UI.Utilities;
using Flooring.UI.Workflows;

namespace Flooring.UI.Menus
{
    public class MainMenu
    {
        public void StartApp()
        {
            do
            {
                CheckMode();
            } while (true);
        }

        internal void CheckMode()
        {
            Console.Clear();
            ConsoleKeyInfo[] isAdmin = new ConsoleKeyInfo[3];
            WelcomeScreen.DisplayWelcome();
            isAdmin[0] = Console.ReadKey(true);
            if (isAdmin[0].Key == ConsoleKey.Enter)
            {
                NormalWorkflow startNormalMode = new NormalWorkflow();
                startNormalMode.Execute();
                return;
            }
            isAdmin[1] = Console.ReadKey(true);
            isAdmin[2] = Console.ReadKey(true);

            if (isAdmin[0].Key == ConsoleKey.Escape && isAdmin[1].Key == ConsoleKey.Delete &&
                isAdmin[2].Key == ConsoleKey.A)
            {
                string[] userPass = AdminLogIn.LogIn();
                if (userPass.Length == 2)
                {
                    AdminWorkflow adminWorkflow = new AdminWorkflow();
                    adminWorkflow.Execute(userPass);
                }
            }
            else
            {
                Console.WriteLine("\t\tInvalid Input. Press ENTER to continue.");
                Console.ReadLine();
            }
        }
    }
}
                            