﻿using System;
using Flooring.BLL;
using Flooring.Models;
using Flooring.UI.Workflows;

namespace Flooring.UI.Menus
{
    public class AdminWorkflow
    {
        public void Execute(string[] userInfo)
        {
            AdminManager manager = new AdminManager();
            var response = new Response<string>();
            response = manager.checkAdminLogin(userInfo);

            if (response.Success)
            {
                AdminWelcomeScreen(response.Data);
            }
            else
            {
                Console.WriteLine("Sorry incorrect username or password. Press ENTER to continue.");
                Console.ReadLine();
                return;
            }
        }

        public void AdminWelcomeScreen(string adminName)
        {
            do
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\n\t\t\t\tWelcome {0}.", adminName);
                Console.ForegroundColor = ConsoleColor.Green;
                string options = @"    
                      ╔═══════════╦═════════════╦══════════╦════════╗
                      ║   Add     ║     Add     ║  Change  ║  Quit  ║
                      ║  Product  ║  State/Tax  ║  Config  ║        ║
                      ║   [1]     ║     [2]     ║    [3]   ║   [4]  ║
                      ╚═══════════╩═════════════╩══════════╩════════╝";
                Console.WriteLine(options);
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.Write("\n\n\n\n\t\tPlease enter an option (1-4) : ");
                string input = Console.ReadLine();

                if (input == "")
                    continue;
                if (input == "4")
                    break;

                ProcessInput(input);
            } while (true);
        }

        public void ProcessInput(string input)
        {
            switch (input)
            {
                case "1":
                    var addProduct = new AddProductWorkflow();
                    addProduct.Execute();
                    break;
                case "2":
                    var addStateTax = new AddStateTaxWorkflow();
                    addStateTax.Execute();
                    break;
                case "3":
                    var changeConfigMode = new ChangeConfigMode();
                    changeConfigMode.Execute();
                    break;
                default:
                    Console.WriteLine("\t\tYou entered an invalid input. Press ENTER to continue");
                    Console.ReadLine();
                    break;
            }
        }
    }
}
