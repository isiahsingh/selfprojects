﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.UI.UserPrompt
{
    public static class GetUserInfo
    {
        public static string GetUserName(string input)
        {
            do
            {
                if (input == "")
                {
                    Console.WriteLine("\t\t\tYou must enter a name. Press ENTER to try again.");
                    Console.ReadLine();
                    Console.Write("\n\t\t\tEnter Your Name : ");
                    input = Console.ReadLine();
                }
            } while (input == "");

            return input;
        }

        public static string GetUserState(List<string> stateList, string states, string input)
        {
            bool isValidState;
            do
            {
                input = input.ToUpper();
                isValidState = stateList.Any(s => s.ToUpper() == input);
                if (!isValidState)
                {
                    Console.WriteLine("\t\t\tYou must enter a valid state. Press ENTER to try again.");
                    Console.ReadLine();
                    Console.Write("\n\t\t\tEnter Your State ({0}) : ", states);
                    input = Console.ReadLine();
                }
                else if (input == "")
                {
                    Console.WriteLine("\t\t\tThis field can not be blank. Press ENTER to try again.");
                    Console.ReadLine();
                    Console.Write("\n\t\t\tEnter Your State ({0}) : ", states);
                    input = Console.ReadLine();
                }
            } while (input == "" || isValidState == false);

            return input;
        }

        public static string GetProductType(List<string> productTypeList, string productTypes, string input)
        {
            bool isValidType;
            do
            {
                input = input.ToUpper();
                isValidType = productTypeList.Any(t => t.ToUpper() == input);
                if (!isValidType)
                {
                    Console.WriteLine("\t\t\tYou must enter a valid type. Press ENTER to try again.");
                    Console.ReadLine();
                    Console.Write("\n\t\t\tEnter The Product Type ({0}) : ", productTypes);
                    input = Console.ReadLine();
                }
                else if (input == "")
                {
                    Console.WriteLine("\t\t\tThis field can not be blank. Press ENTER to try again.");
                    Console.ReadLine();
                    Console.Write("\n\t\t\tEnter The Product Type ({0}) : ", productTypes);
                    input = Console.ReadLine();
                }
            } while (input == "" || isValidType == false);

            return input;
        }

        public static string GetArea(string input)
        {
            bool isValidNumber;
            int area;
            do
            {
                isValidNumber = int.TryParse(input, out area);
                if (isValidNumber == false || area <= 0)
                {
                    Console.WriteLine("\t\tYou must enter a valid number greater than zero. Press ENTER to try again.");
                    Console.ReadLine();
                    Console.Write("\n\t\t\tEnter The Area (In Square Feet) : ");
                    input = Console.ReadLine();
                }
            } while (isValidNumber == false || area <= 0);

            return input;
        }

        public static DateTime GetDate()
        {
            DateTime dateInput;
            bool isValidDate;
            do
            {
                Console.Write("\n\t\t\tPlease Enter Your Order Date(MM/DD/YYYY) : ");
                string date = Console.ReadLine();

                isValidDate = DateTime.TryParse(date, out dateInput);
                if (isValidDate == false)
                {
                    Console.WriteLine("\t\t\tOrder Date Not Found. Please Enter Vaild Date.");
                    Console.ReadLine();
                }
            } while (isValidDate == false);
            return dateInput;
        }

        public static int GetOrderNumber()
        {
            int orderNumber;
            bool isValid;
            do
            {
                Console.Write("\n\t\t\tPlease Enter Your Order Number : ");
                string input = Console.ReadLine();
                isValid = int.TryParse(input, out orderNumber);
                if (isValid == false)
                {
                    Console.WriteLine("\t\t\tYou Did Not Enter A Valid Order Number. Press ENTER to try again.");
                    Console.ReadLine();
                }
            } while (isValid == false);

            return orderNumber;
        }
    }
}
