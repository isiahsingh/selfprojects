﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.UI.Utilities
{
    public static class WelcomeScreen
    {
        public static void DisplayWelcome()
        {
            string welcome = @"
                  ___     _       _           _       _        
                 |_ _|___(_) __ _| |__       | | __ _| | _____ 
                  | |/ __| |/ _` | '_ \   _  | |/ _` | |/ / _ \
                  | |\__ \ | (_| | | | | | |_| | (_| |   <  __/
                 |___|___/_|\__,_|_| |_|  \___/ \__,_|_|\_\___|
                  _____ _                  _                   
                 |  ___| | ___   ___  _ __(_)_ __   __ _       
                 | |_  | |/ _ \ / _ \| '__| | '_ \ / _` |      
                 |  _| | | (_) | (_) | |  | | | | | (_| |      
                 |_|   |_|\___/ \___/|_|  |_|_| |_|\__, |      
                                                   |___/       ";

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(welcome);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\n\n\n\n\t\tPress ENTER to continue ....");
        }
    }
}
