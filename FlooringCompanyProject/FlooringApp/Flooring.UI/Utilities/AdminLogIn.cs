﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.UI.Menus;

namespace Flooring.UI.Utilities
{
    public static class AdminLogIn
    {
        public static string[] LogIn()
        {
            string[] adminInfo = new string[2];
            List<string> info = new List<string>();
            Console.Clear();
            Console.WriteLine("\t\t\t\t\t\t\tQ - Back to Main Screen.");
            Console.WriteLine("\n\n\n\t\t   ╔══════════════════════════════════════════════════╗");
            Console.WriteLine("\t\t   ║                   ADMIN LOG IN                   ║");
            Console.WriteLine("\t\t   ╚══════════════════════════════════════════════════╝");
            Console.Write("\t\t\t       Username: "); string userName = Console.ReadLine();
            if (userName.ToUpper() == "Q")
            {
                MainMenu menu = new MainMenu();
                menu.StartApp();
            }
            Console.Write("\n\t\t\t       Password: "); string password = PasswordEntry();
            if (password.ToUpper() == "Q")
            {
                MainMenu menu = new MainMenu();
                menu.StartApp();
            }

            Console.WriteLine("\n\n\n\t\tPress ENTER to Log In.");
            adminInfo[0] = userName;
            adminInfo[1] = password;
            Console.ReadLine();

            return adminInfo;
        }

        internal static string PasswordEntry()
        {
            string password = "";
            ConsoleKeyInfo key;
            do
            {
                key = Console.ReadKey(true);

                if (key.Key != ConsoleKey.Backspace && key.Key != ConsoleKey.Enter)
                {
                    password += key.KeyChar;
                    Console.Write("*");
                }
                else
                {
                    if (key.Key == ConsoleKey.Backspace && password.Length > 0)
                    {
                        password = password.Substring(0, (password.Length - 1));
                        Console.Write("\b \b");
                    }
                }
            }
            // Stops Receving Keys Once Enter is Pressed
            while (key.Key != ConsoleKey.Enter);
            return password;
        }
    }
}

