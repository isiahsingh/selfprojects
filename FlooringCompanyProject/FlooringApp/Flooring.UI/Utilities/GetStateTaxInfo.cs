﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.UI.Utilities
{
    internal class GetStateTaxInfo
    {
        public static string GetStateAbbr()
        {
            string stateAbbr;
            do
            {
                Console.Write("\n\t\t\tEnter the state abbreviation : ");
                stateAbbr = Console.ReadLine();
                if (stateAbbr == "")
                {
                    Console.WriteLine("\t\t\tYou must enter something for the state abbreviation. Press ENTER to try again.");
                    Console.ReadLine();
                }
            } while (stateAbbr == "");

            return stateAbbr;
        }

        public static string GetStateName()
        {
            string stateName;
            do
            {
                Console.Write("\n\t\t\tEnter the state name : ");
                stateName = Console.ReadLine();
                if (stateName == "")
                {
                    Console.WriteLine("\t\t\tYou must enter something for the state name. Press ENTER to try again.");
                    Console.ReadLine();
                }
            } while (stateName == "");

            return stateName;
        }

        public static string GetTaxRate()
        {
            string stringTaxRate;
            bool isValid;
            decimal taxRate;
            do
            {
                Console.Write("\n\t\t\tEnter the tax rate : ");
                stringTaxRate = Console.ReadLine();
                isValid = decimal.TryParse(stringTaxRate, out taxRate);
                if (isValid == false)
                {
                    Console.WriteLine("\t\t\tYou must enter a decimal number. Press ENTER to try again.");
                    Console.ReadLine();
                }
            } while (isValid == false);

            return stringTaxRate;
        }
    }
}
