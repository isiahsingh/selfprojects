﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.UI.Utilities
{
    internal static class GetProductInfo
    {
        public static string GetProductType()
        {
            string productType;
            do
            {
                Console.Write("\n\t\t\tEnter the product type : ");
                productType = Console.ReadLine();
                if (productType == "")
                {
                    Console.WriteLine("\t\t\tYou must enter something for product type. Press ENTER to try again.");
                    Console.ReadLine();
                }
            } while (productType == "");
           
            return productType;
        }

        public static string GetCostPSF()
        {
            string stringCostPSF;
            bool isValid;
            decimal costPSF;
            do
            {
                Console.Write("\n\t\t\tEnter the cost per square foot : ");
                stringCostPSF = Console.ReadLine();
                isValid = decimal.TryParse(stringCostPSF, out costPSF);
                if (isValid == false)
                {
                    Console.WriteLine("\t\t\tYou must enter a decimal number. Press ENTER to try again.");
                    Console.ReadLine();
                }
            } while (isValid == false);
            
            return stringCostPSF;
        }

        public static string GetLaborCPSF()
        {
            string stringLaborCPSF;
            bool isValid;
            decimal laborCPSF;
            do
            {
                Console.Write("\n\t\t\tEnter the labor cost per square foot : ");
                stringLaborCPSF = Console.ReadLine();
                isValid = decimal.TryParse(stringLaborCPSF, out laborCPSF);
                if (isValid == false)
                {
                    Console.WriteLine("\t\t\tYou must enter a decimal number. Press ENTER to try again.");
                    Console.ReadLine();
                }
            } while (isValid == false);
           
            return stringLaborCPSF;
        }
    }
}
