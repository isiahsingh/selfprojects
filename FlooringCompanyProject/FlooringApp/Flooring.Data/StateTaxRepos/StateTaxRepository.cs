﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Models;

namespace Flooring.Data.StateTaxRepos
{
    public class StateTaxRepository : IStateTaxRepository
    {
        public string _filePath { get; set; }

        public StateTaxRepository()
        {
            _filePath = @"DataFiles\StateTax.txt";
        }

        public List<StateTax> GetAll()
        {
            var stateTaxList = new List<StateTax>();
            string[] reader = File.ReadAllLines(_filePath);

            for (int i = 1; i < reader.Length; i++)
            {
                StateTax stateTax = new StateTax().ConvertCSVToStateTax(reader[i]);
                stateTaxList.Add(stateTax);
            }
            return stateTaxList;
        }

        public void AddStateTax(StateTax stateTaxToAdd)
        {
            List<StateTax> allStateTax = GetAll();
            allStateTax.Add(stateTaxToAdd);
            List<string> stateTaxStringList = (allStateTax.Select(st => st.ConvertToCSV())).ToList();
            string stateTaxHeader = "StateAbbreviation,StateName,TaxRate";
            CSVWritter writer = new CSVWritter();
            writer.WriteToFile(stateTaxStringList, _filePath, stateTaxHeader);
        }

        public StateTax GetByState(string inputState)
        {
            List<StateTax> getallStates = GetAll();
            StateTax stateToReturn = new StateTax(); // = null
            foreach (var s in getallStates)
            {
                if (s.ST.ToUpper() == inputState)
                {
                    stateToReturn = s;
                }
            }
            return stateToReturn;
        }
    }
}
