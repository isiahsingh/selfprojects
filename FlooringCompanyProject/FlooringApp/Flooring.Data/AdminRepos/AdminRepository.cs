﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Models;

namespace Flooring.Data.AdminRepos
{
    public class AdminRepository
    {
        private readonly string _filePath;

        public AdminRepository()
        {
            _filePath = @"DataFiles\AdminLogin.txt";
        }

        public List<Admin> GetAll()
        {
            var adminList = new List<Admin>();
            string[] reader = File.ReadAllLines(_filePath);

            for (int i = 1; i < reader.Length; i++)
            {
                Admin adminUser = new Admin().ConvertCSVToProducts(reader[i]);
                adminList.Add(adminUser);
            }
            return adminList;
        }
    }
}
