﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Models.Interfaces;

namespace Flooring.Data.ErrorRepos
{
    public class TestErrorRepository : IErrorRepository
    {
        private const string _filePath = @"TestData\TestErrorLog.txt";

        public List<string> GetAllErrors()
        {
            string[] reader = File.ReadAllLines(_filePath);
            List<string> messageList = reader.ToList();
            return messageList;
        }

        public void AddError(string message)
        {
            List<string> messageList = GetAllErrors();
            messageList.Add(message);
            Overwrite(messageList);
        }

        public void Overwrite(List<string> messageList)
        {
            File.Delete(_filePath);

            using (StreamWriter writer = File.CreateText(_filePath))
            {
                foreach (string message in messageList)
                {
                    writer.WriteLine(message);
                }
            }
        }
    }
}
