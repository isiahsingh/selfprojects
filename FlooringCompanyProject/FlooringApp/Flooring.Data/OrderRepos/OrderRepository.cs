﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Flooring.Models;

namespace Flooring.Data.OrderRepos
{
    public class OrderRepository : IDataRepository
    {
        private string _filePath;

        public void SetFilePath(DateTime date)
        {
            _filePath = $@"DataFiles\Orders_{date.ToString("MMddyyyy")}.txt";
        }

        public List<Order> GetAll(DateTime date)
        {
            SetFilePath(date);
            var orderList = new List<Order>();
            if (File.Exists(_filePath))
            {
                string[] reader = File.ReadAllLines(_filePath);

                for (int i = 1; i < reader.Length; i++)
                {
                    Order order = new Order().ConvertCSVToObject((reader[i]));
                    orderList.Add(order);
                }
              
            }
            return orderList;
        }

        public Order GetOrderByID(int orderNumber, DateTime date)
        {
            Order order = null;
            SetFilePath(date);
            if ((File.Exists(_filePath)))
            {
                List<Order> orderNumberList = GetAll(date);
                if (orderNumberList.Count == 0)
                {
                    return null;
                }
                foreach (var ord in orderNumberList.Where(ord => ord.orderNumber == orderNumber))
                {
                    order = ord;
                }
                return order;
            }
            return null;
        }

        public void Update(List<Order> editedOrderList, DateTime date)
        {
            SetFilePath(date);
            if ((File.Exists(_filePath)))
            {
                List<string> stringList = editedOrderList.Select(order => order.ConvertToCSV()).ToList();
                CSVWritter writer = new CSVWritter();
                string orderHeader =
               "OrderNumber,CustomerName,State,TaxRate,ProductType,Area,CostPSF,LaborCostPSF,MaterialCost,LaborCost,Tax,Total";
                writer.WriteToFile(stringList, _filePath, orderHeader);
            }
        }

        public void Create(Order newOrder, DateTime date)
        {
            List<Order> orderList = GetAll(date);
            orderList.Add(newOrder);

            List<string> stringList = orderList.Select(order => order.ConvertToCSV()).ToList();
            string orderHeader =
                "OrderNumber,CustomerName,State,TaxRate,ProductType,Area,CostPSF,LaborCostPSF,MaterialCost,LaborCost,Tax,Total";
            CSVWritter writer = new CSVWritter();
            writer.WriteToFile(stringList, _filePath, orderHeader);
        }


        public void Delete(DateTime date, int orderNumber)
        {
            SetFilePath(date);
            if ((File.Exists(_filePath)) == false)
            {
                return;
            }
            List<Order> orderList = GetAll(date);
            List<Order> newListWithoutDeletedOrder = (orderList.Select(o => o).Where(o => o.orderNumber != orderNumber)).ToList();
            List<string> stringList = newListWithoutDeletedOrder.Select(order => order.ConvertToCSV()).ToList();
            string orderHeader =
               "OrderNumber,CustomerName,State,TaxRate,ProductType,Area,CostPSF,LaborCostPSF,MaterialCost,LaborCost,Tax,Total";
            CSVWritter writer = new CSVWritter();
            writer.WriteToFile(stringList, _filePath, orderHeader);
        }
    }
}
