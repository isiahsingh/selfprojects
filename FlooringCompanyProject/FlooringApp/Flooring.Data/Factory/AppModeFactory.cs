﻿using System.Configuration;
using Flooring.Data.ErrorRepos;
using Flooring.Data.OrderRepos;
using Flooring.Data.ProductRepos;
using Flooring.Data.StateTaxRepos;
using Flooring.Models;
using Flooring.Models.Interfaces;

namespace Flooring.Data.Factory
{
    public static class AppModeFactory
    {
        private static string _mode;

        public static void CreateDataRepository()
        {
           _mode = ConfigurationManager.AppSettings.Get("Mode");
        }

        public static string GetConfigFileMode()
        {
            return _mode;
        }

        public static void SetConfigFileMode(string mode)
        {
            ConfigurationManager.AppSettings.Set("Mode", mode);
        }

        public static IDataRepository GetOrderRepo()
        {
            if (_mode == "Test")
            {
                return new TestOrderRepository();
            }
            else
            {
                return new OrderRepository();
            }
            
        }

        public static IStateTaxRepository GetOrderStateTaxRepo()
        {
            if (_mode == "Test")
            {
                return new TestStateTaxRepository();
            }
            else
            {
                return new StateTaxRepository();
            }
        }

        public static IProductRepository GetProductRepo()
        {
            if (_mode == "Test")
            {
                return new TestProductRepository();
            }
            else
            {
                return new ProductRepository();
            }
        }

        public static IErrorRepository GetErrorRepo()
        {
            if (_mode == "Test")
            {
                return new TestErrorRepository();
            }
            else
            {
                return new ErrorRepository();
            }
        }
    }
}
