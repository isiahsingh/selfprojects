﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Data.ErrorRepos;
using Flooring.Data.OrderRepos;
using Flooring.Data.ProductRepos;
using Flooring.Data.StateTaxRepos;
using Flooring.Models;
using Flooring.Models.Interfaces;
using Ninject.Modules;

namespace Flooring.Data.Factory
{
    public class Bindings : NinjectModule
    {
        public override void Load()
        {
            if (ConfigurationManager.AppSettings.Get("Mode") == "Production")
            {
                Bind<IDataRepository>().To<OrderRepository>();
                Bind<IErrorRepository>().To<ErrorRepository>();
                Bind<IProductRepository>().To<ProductRepository>();
                Bind<IStateTaxRepository>().To<StateTaxRepository>();
            }
            else
            {
                Bind<IDataRepository>().To<TestOrderRepository>();
                Bind<IErrorRepository>().To<TestErrorRepository>();
                Bind<IProductRepository>().To<TestProductRepository>();
                Bind<IStateTaxRepository>().To<TestStateTaxRepository>();
            }

}
    }
}
