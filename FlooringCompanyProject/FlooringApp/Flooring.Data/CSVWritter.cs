﻿using System.Collections.Generic;
using System.IO;

namespace Flooring.Data
{
    public class CSVWritter 
    {
        public void WriteToFile(List<string> stringList, string filePath, string orderHeader)
        {
            File.Delete(filePath);

            using (var writer = File.CreateText(filePath))
            {
                
                writer.WriteLine(orderHeader);
                foreach (var order in stringList)
                {
                    writer.WriteLine(order);
                }
            }
        }
    }
}
