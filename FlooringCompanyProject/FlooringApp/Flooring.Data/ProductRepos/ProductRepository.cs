﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Models;

namespace Flooring.Data.ProductRepos
{
    public class ProductRepository : IProductRepository
    {
        private readonly string _filePath;

        public ProductRepository()
        {
            _filePath = @"DataFiles\Products.txt";
        }

        public List<Product> GetAll()
        {
            var productList = new List<Product>();
            string[] reader = File.ReadAllLines(_filePath);

            for (int i = 1; i < reader.Length; i++)
            {
                Product product = new Product().ConvertCSVToProducts(reader[i]);
                productList.Add(product);
            }
            return productList;
        }

        public void AddProduct(Product productToAdd)
        {
            List<Product> allProducts = GetAll();
            allProducts.Add(productToAdd);
            List<string> productStringList = (allProducts.Select(prod => prod.ConvertToCSV())).ToList();
            string productHeader = "ProductType,CostPerSquareFoot,LaborCostPerSquareFoot";
            CSVWritter writer = new CSVWritter();
            writer.WriteToFile(productStringList, _filePath, productHeader);
        }

        public Product GetByType(string productType)
        {
            List<Product> productList = GetAll();
            Product product = new Product();
            foreach (var p in productList)
            {
                if (p.Type.ToUpper() == productType)
                {
                    product = p;
                }
            }
            return product;
        }
    }
}
