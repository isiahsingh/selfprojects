﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Flooring.Models
{
    public class Order
    {
        public int orderNumber { get; set; }
        public string customerName { get; set; }
        public string state { get; set; }
        public decimal taxRate { get; set; }
        public string productType { get; set; }
        public decimal area { get; set; }
        public decimal costPSF{ get; set; }
        public decimal laborCostPSF { get; set; }
        public decimal materialCost { get; set; }
        public decimal laborCost { get; set; }
        public decimal tax { get; set; }
        public decimal total { get; set; }

        public string ConvertToCSV()
        {
            string OrderString = string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\"", this.orderNumber,
                this.customerName, this.state, this.taxRate, this.productType, this.area, this.costPSF,
                this.laborCostPSF, this.materialCost, this.laborCost, this.tax, this.total);

            return OrderString;
        }

        public Order ConvertCSVToObject(string info)
        {
            string regEx = @"("",?""?)";

            var orderInfo = Regex.Replace(info, regEx, "\n").Split('\n');

            this.orderNumber = int.Parse(orderInfo[1]);
            this.customerName = orderInfo[2];
            this.state = orderInfo[3];
            this.taxRate = decimal.Parse(orderInfo[4]);
            this.productType = orderInfo[5];
            this.area = decimal.Parse(orderInfo[6]);
            this.costPSF = decimal.Parse(orderInfo[7]);
            this.laborCostPSF = decimal.Parse(orderInfo[8]);
            this.materialCost = decimal.Parse(orderInfo[9]);
            this.laborCost = decimal.Parse(orderInfo[10]);
            this.tax = decimal.Parse(orderInfo[11]);
            this.total = decimal.Parse(orderInfo[12]);

            return this;
        }
    }
}
