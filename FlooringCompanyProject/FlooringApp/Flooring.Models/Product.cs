﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.Models
{
    public class Product
    {
        public string Type { get; set; }
        public decimal CostPSF { get; set; }
        public decimal LaborCostPSF { get; set; }

        public Product ConvertCSVToProducts(string info)
        {
            string[] productInfo = info.Split(',');

            this.Type = productInfo[0];
            this.CostPSF = decimal.Parse(productInfo[1]);
            this.LaborCostPSF = decimal.Parse(productInfo[2]);

            return this;
        }


        public string ConvertToCSV()
        {
            string productString = string.Format("{0},{1},{2}", this.Type, this.CostPSF, this.LaborCostPSF);

            return productString;
        }
    }
}
