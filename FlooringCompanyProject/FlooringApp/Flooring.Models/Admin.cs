﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.Models
{
    public class Admin
    {
        public string UserName { get; set; }
        public string Password { get; set; }

        public Admin ConvertCSVToProducts(string info)
        {
            string[] adminInfo = info.Split(',');

            this.UserName = adminInfo[0];
            this.Password = adminInfo[1];

            return this;
        }
    }
}
