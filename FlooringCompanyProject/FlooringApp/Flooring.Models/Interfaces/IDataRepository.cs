﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.Models
{
    public interface IDataRepository
    {
        void SetFilePath(DateTime date);
        List<Order> GetAll(DateTime date);
        void Create(Order newOrder, DateTime date);
        void Delete(DateTime date, int id);
        void Update(List<Order> editedOrderList,DateTime date);
        Order GetOrderByID(int orderNumber, DateTime date);
    }
}
