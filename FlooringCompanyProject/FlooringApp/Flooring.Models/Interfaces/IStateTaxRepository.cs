﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.Models
{
    public interface IStateTaxRepository
    {
        List<StateTax> GetAll();
        StateTax GetByState(string inputState);
        void AddStateTax(StateTax stateTaxToAdd);
    }
}
