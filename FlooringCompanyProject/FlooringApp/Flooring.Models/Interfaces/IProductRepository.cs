﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.Models
{
    public interface IProductRepository
    {
        List<Product> GetAll();
        Product GetByType(string productType);
        void AddProduct(Product productToAdd);
    }
}
