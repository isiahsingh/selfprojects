﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.Models.Interfaces
{
    public interface IErrorRepository
    {
        List<string> GetAllErrors();
        void AddError(string message);
        void Overwrite(List<string> messageList);
    }
}
