﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.Models
{
    public class StateTax
    {
        public string ST { get; set; }
        public string State { get; set; }
        public decimal TaxRate { get; set; }

        public StateTax ConvertCSVToStateTax(string info)
        {
            string[] StateTaxInfo = info.Split(',');

            this.ST = StateTaxInfo[0];
            this.State = StateTaxInfo[1];
            this.TaxRate = decimal.Parse(StateTaxInfo[2]);

            return this;
        }

        public string ConvertToCSV()
        {
            string stateTax = $"{this.ST},{this.State},{this.TaxRate}";

            return stateTax;
        }
    }
}
