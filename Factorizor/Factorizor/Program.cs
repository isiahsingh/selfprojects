﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factorizor
{
    class Program
    {
        static void Main(string[] args)
        {
            bool playAgain = true;
            int number;
            Program p = new Program();
            while (playAgain == true)
            {
                Console.Clear();
                p.DisplayHeader();
                number = p.AskNumber();
                Console.WriteLine("\nThe factors of {0} are: \n", number);
                int[] factors = p.findFactors(number);
                Console.WriteLine("There are {0} factors for {1}", factors.Length, number);
                p.checkPerfect(factors, number);
                p.isPrime(factors, number);
                playAgain = p.playAgain();
            }
        }

        public void DisplayHeader()
        {
            Console.WriteLine("*******************************************************************************");
            Console.WriteLine("******************************** Factorizor ***********************************");
            Console.WriteLine("*******************************************************************************\n");
        }

        public bool playAgain()
        {
            ConsoleColor regularColor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Want to play again? (y/n)");
            Console.ForegroundColor = regularColor;
            string answer = Console.ReadLine();
            if (answer.ToUpper() == "Y")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void isPrime(int[] factors, int theNumber)
        {
            if (factors.Length == 1)
            {
                ConsoleColor regularColor = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("{0} is a prime number.", theNumber);
                Console.ForegroundColor = regularColor;
            }
            else
            {
                Console.WriteLine("{0} is NOT a prime number.", theNumber);
            }
        }

        public void checkPerfect(int[] factors, int theNumber)
        {
            int sumNumber = 0;
            foreach (int element in factors)
            {
                sumNumber += element;
            }
            if (sumNumber == theNumber)
            {
                ConsoleColor regularColor = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("{0} is a perfect number.", theNumber);
                Console.ForegroundColor = regularColor;
            }
            else
            {
                Console.WriteLine("{0} is NOT a perfect number.", theNumber);
            }
        }

        public int[] findFactors(int number)
        {
            List<int> factorList = new List<int>();

            for (int i = 1; i < number; i++)
            {
                if (number%i == 0)
                {
                    Console.WriteLine(i);
                    factorList.Add(i);
                }
            }
            return factorList.ToArray();
        }

        public int AskNumber()
        {
            int result;
            bool isValid;
            do
            {
                Console.Write("Please enter a number that you would like to factor : ");
                string input = Console.ReadLine();
                isValid = int.TryParse(input, out result);
                if (isValid == false)
                {
                    Console.WriteLine("What you entered was not a number!");
                }
            } while (isValid == false);
            return result;
        }
    }
}
