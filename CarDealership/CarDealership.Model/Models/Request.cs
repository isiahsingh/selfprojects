﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CarDealership.Model.Models
{
    public class Request
    {
        [Key]
        public int RequestId { get; set; }        
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime BestTimeToCall { get; set; }
        public string TimeFrameToPurchase { get; set; }
        public string AdditionalInfo { get; set; }
        public string ContactMethod { get; set; }
        public DateTime LastContactDate { get; set; }

        public int StatusId { get; set; }
        public virtual Status Status { get; set; }

        public int CarId { get; set; }
        public virtual Car Car { get; set; }
    }
}
