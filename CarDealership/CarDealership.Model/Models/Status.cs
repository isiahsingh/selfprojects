﻿using System.ComponentModel.DataAnnotations;

namespace CarDealership.Model.Models
{
    public class Status
    {
        [Key]
        public int StatusId { get; set; }
        public string StatusName { get; set; }
    }
}
