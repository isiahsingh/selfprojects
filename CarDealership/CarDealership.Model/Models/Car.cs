﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarDealership.Model.Models
{
    public class Car
    {
        [Key]
        public int Id { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public int Mileage { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }

        public int? ConditionId { get; set; }
        public virtual Condition CarCondition { get; set; }

        public int? ImageId { get; set; }
        public virtual FileUpload Image { get; set; }

        public virtual List<Request> Requests { get; set; } 

    }
}
