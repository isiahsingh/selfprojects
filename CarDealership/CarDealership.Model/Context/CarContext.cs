﻿using System.Data.Entity;
using System.Security.AccessControl;
using CarDealership.Model.Models;

namespace CarDealership.Model.Context
{
    public class CarContext : DbContext
    {
        public DbSet<Car> Cars { get; set; }
        public DbSet<Request> Requests { get; set; }
        public DbSet<Status> Statuses { get; set; }

        //public CarContext(): base("name=CarDealership") { }
        public DbSet<FileUpload> FileUploads { get; set; } 
        //public CarContext() : base("name=CarContext")
        //{
        //    this.Configuration.LazyLoadingEnabled = false;
        //}
    }
}
