﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CarDealership.Model.Context;
using CarDealership.Model.Models;

namespace CarDealership.UI.Controllers
{
    public class CarController : ApiController
    {
        public List<Car> Get()
        {
            List<Car> carList ;
            using (var db = new CarContext())
            {
                carList = db.Cars.Include("Requests").Include("CarCondition").Include("Image").Select(c => c).ToList();
                
            }
            return carList;
        }

        public Car Get(int id)
        {
            using (var db = new CarContext())
            {
                return db.Cars.FirstOrDefault(c => c.Id == id);
            }
        }

        public HttpResponseMessage Post(Car car)
        {
            HttpResponseMessage response;
            try
            {
                using (var db = new CarContext())
                {
                    db.Cars.Add(car);
                    db.SaveChanges();
                }
                response = Request.CreateResponse(HttpStatusCode.Created, car);
            }
            catch
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            return response;
        }

        public HttpResponseMessage Delete(int id)
        {
            HttpResponseMessage response;
            try
            {
                using (var db = new CarContext())
                {
                    Car car = db.Cars.FirstOrDefault(c => c.Id == id);
                    if (car != null)
                    {
                        db.Cars.Remove(car);
                        db.SaveChanges();
                    }
                }
                response = Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            return response;
        }

        public HttpResponseMessage Put(Car car)
        {
            HttpResponseMessage response;
            try
            {
                using (var db = new CarContext())
                {
                    db.Cars.AddOrUpdate(car);
                    db.SaveChanges();
                }
                response = Request.CreateResponse(HttpStatusCode.OK, car);
            }
            catch
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            return response;
        }
    }
}
