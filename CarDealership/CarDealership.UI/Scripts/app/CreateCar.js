﻿$(document).ready(function() {
    $('#btnAddCar').click(function() {
        $('#addCarModal').modal('show');

    });
});

function uploadImage() {
    //alert('test');
    //e.preventDefault();
    var form = $(this);
    var fileSelect = $("#fileUpload")[0];
    var uploadButton = $("#btnUploadFile");

    uploadButton.innerHTML = 'uploading...';

    var formData = new FormData();

    var files = fileSelect.files;

    // Loop through each of the selected files.
    for (var i = 0; i < files.length; i++) {
        var file = files[i];

        // Check the file type.
        if (!file.type.match('image.*') && file.length === 0) {
            continue;
        }

        // Add the file to the request.
        formData.append('photos[]', file, file.name);
    }

    var opts = {
        url: '/api/Image',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        success: function (data) {
            uploadButton.innerHTML = 'Upload';
        },
        error: function (data, status) {
            alert(status);
        }
    };
    $.ajax('/api/Image', opts);
}

function GetImageId() {
    
}

$('#btnSaveCar').click(function() {
    var car = {};

    car.Make = $('#make').val();
    car.Model = $('#model').val();
    car.Year = $('#year').val();
    car.ConditionId = $('#condition').val();
    car.Mileage = $('#mileage').val();
    car.Price = $('#price').val();
    car.Description = $('#description').val();
    uploadImage();
    GetImageId();
    //car.ImageId = 
    $.post(uri, car)
        .done(function () {
           
            GetCars();
            
            $('#addCarModal').modal('hide');
        })
        .fail(function(jqXhr, status, error) {
            $(errorMessage(error)).appendTo($('#errors'));
            $('#addCarModal').modal('hide');
        });

    
});

function errorMessage(error) {
    return '<div class="alert alert-danger" role="alert" id="danger-alert">' +
    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
    '<span aria-hidden="true">&times;</span></button>' + error + '</div>';
}