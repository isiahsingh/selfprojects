﻿var uri = '/api/car/';

$(document).ready(function() {
    GetCars();

    //$('#delete').click(function deleteRow() {
    //    DeleteCar();
    //});
});

function DeleteCar(id) {
    $.ajax({
        url: uri + id,
        type: 'DELETE',
        success: function() { GetCars() }
    });
}

function GetCars() {
    var counter = 0;
    $.getJSON(uri)
        .done(function (data) {
            $('#carsTable tbody tr').remove();
            $('#pictures .row .col-md-3').remove();
            $.each(data, function (index, car) {
                var rText = "";
                var eText = "";
                $(createRow(car)).appendTo($('#carsTable tbody'));
                if (car.ImageId != null) {
                    
                    if (counter % 4 === 0 || counter === 0) {
                        rText = '<div class="row">';
                        eText = '</div>';
                    }
                    
                    var hText = '<div class="col-md-3"><img src="Content/Upload/' + car.Image.ImageName + '"height="250" width="250" class="img-responsive img-rounded"></div>';
                    $(rText + hText + eText).appendTo($('#pictures .row'));
                    counter++;
                }    
            });
        });
}

function createRow(car) {
    return '<tr><td>' + car.Make + '</td>' +
        '<td>' + car.Model + '</td>' +
        '<td>' + car.Year + '</td>' +
        '<td>' + car.CarCondition.Type + '</td>' +
        '<td>' + car.Mileage + '</td>' +
        '<td>' + car.Price + '</td>' +
        '<td>' + car.Description + '</td>' +
        '<td><button type="button" class="btn btn-danger" id="delete" onclick="DeleteCar('+car.Id+')">Delete</button></tr>';

}

