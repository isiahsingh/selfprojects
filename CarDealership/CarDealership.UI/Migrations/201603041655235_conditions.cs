namespace CarDealership.UI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class conditions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Conditions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Cars", "ConditionId", c => c.Int());
            CreateIndex("dbo.Cars", "ConditionId");
            AddForeignKey("dbo.Cars", "ConditionId", "dbo.Conditions", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cars", "ConditionId", "dbo.Conditions");
            DropIndex("dbo.Cars", new[] { "ConditionId" });
            DropColumn("dbo.Cars", "ConditionId");
            DropTable("dbo.Conditions");
        }
    }
}
