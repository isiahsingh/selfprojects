namespace CarDealership.UI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nullid : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Cars", "ImageId", "dbo.FileUploads");
            DropIndex("dbo.Cars", new[] { "ImageId" });
            AlterColumn("dbo.Cars", "ImageId", c => c.Int());
            CreateIndex("dbo.Cars", "ImageId");
            AddForeignKey("dbo.Cars", "ImageId", "dbo.FileUploads", "ImageId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cars", "ImageId", "dbo.FileUploads");
            DropIndex("dbo.Cars", new[] { "ImageId" });
            AlterColumn("dbo.Cars", "ImageId", c => c.Int(nullable: false));
            CreateIndex("dbo.Cars", "ImageId");
            AddForeignKey("dbo.Cars", "ImageId", "dbo.FileUploads", "ImageId", cascadeDelete: true);
        }
    }
}
