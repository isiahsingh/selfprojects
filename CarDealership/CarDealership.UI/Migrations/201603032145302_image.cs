namespace CarDealership.UI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class image : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Cars", "ImageId");
            AddForeignKey("dbo.Cars", "ImageId", "dbo.FileUploads", "ImageId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cars", "ImageId", "dbo.FileUploads");
            DropIndex("dbo.Cars", new[] { "ImageId" });
        }
    }
}
