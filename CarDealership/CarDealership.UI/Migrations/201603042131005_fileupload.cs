namespace CarDealership.UI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fileupload : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.FileUploads", "ImageData");
        }
        
        public override void Down()
        {
            AddColumn("dbo.FileUploads", "ImageData", c => c.Binary());
        }
    }
}
