namespace CarDealership.UI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class imageID : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cars", "ImageId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cars", "ImageId");
        }
    }
}
