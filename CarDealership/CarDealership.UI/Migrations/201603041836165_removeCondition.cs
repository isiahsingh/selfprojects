namespace CarDealership.UI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeCondition : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Cars", "Condition");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Cars", "Condition", c => c.String());
        }
    }
}
